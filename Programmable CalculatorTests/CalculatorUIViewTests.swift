//
//  Programmers_CalculatorTests.swift
//  Programmers CalculatorTests
//
//  Created by Bradley Smith on 10/21/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import _RPN_

class CalculatorUIViewTests: XCTestCase {
	func test1() {
		XCTAssertEqual(1, CalculatorUIView.totalWeights([1]));
	}
	
	func test2() {
		XCTAssertEqual(15, CalculatorUIView.totalWeights([1, 2, 3, 4, 5]));
	}

	func test3()
	{
		XCTAssertEqual([1, 3], CalculatorUIView.distributeSegmentsToCells([1, 1, 1, 1], weights: [1, 3]));
	}

	func test4()
	{
		XCTAssertEqual([401, 100, 399], CalculatorUIView.distributeSegmentsToCells([100, 101, 99, 101, 100, 99, 100, 101, 99], weights: [4, 1, 4]));
	}
	
	func test5()
	{
		XCTAssertEqual([2000, 4001, 1999], CalculatorUIView.distributeSegmentsToCells([2000, 2001, 2000, 1999], weights: [1, 2, 1]));
	}
}