//
//  Programmable_CalculatorTests.swift
//  Programmable CalculatorTests
//
//  Created by Bradley Smith on 11/6/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import _RPN_

class DynamicUITests: XCTestCase
{
	func testOwnerRect()
	{
		let pers = TestLayout.load(id: "testPerspective");

		let ra = CalculatorUIView.buildRectArray(pers, layer: Layer.normal);
		
		// check all rows
		XCTAssertEqual(4, ra.count);

		// check columns of each row
		XCTAssertEqual(2, ra[0].count);
		XCTAssertEqual(2, ra[1].count);
		XCTAssertEqual(2, ra[2].count);
		XCTAssertEqual(2, ra[3].count);

		// check ids
		XCTAssertEqual(0, ra[0][0]);
		XCTAssertEqual(1, ra[0][1]);

		XCTAssertEqual(1000, ra[1][0]);
		XCTAssertEqual(1, ra[1][1]);
		
		XCTAssertEqual(2000, ra[2][0]);
		XCTAssertEqual(2001, ra[2][1]);
		
		XCTAssertEqual(3000, ra[3][0]);
		XCTAssertEqual(3000, ra[3][1]);
	}

	func testRectMap()
	{
		let pers = TestLayout.load(id: "testPerspective");
		
		let ra = CalculatorUIView.buildRectMap(pers, layer: Layer.normal, screenSize: CGSize(width: 100, height: 100));

		// Should be 6 real rects
		XCTAssertEqual(6, ra.count);

		let keySet = ra.keys;

		let keysOrdered = keySet.sorted() { $0 < $1 };
		
		for id in keysOrdered
		{
			let cell = ra[id];
			
			print("\(id) \(String(describing: cell))");
		}
	}
}
