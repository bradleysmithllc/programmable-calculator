//
//  TestProfile.swift
//  Programmable Calculator
//
//  Created by Bradley Smith on 11/24/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

open class TestProfile: JSONProfile
{
	init(id: String)
	{
		if let file = Bundle(for:TestProfile.self).path(forResource: id, ofType: "json") {
			let url = URL(fileURLWithPath: file, isDirectory: false);
			
			super.init(id: id, url: url);
		}
		else
		{
			super.init();
		}
	}
}
