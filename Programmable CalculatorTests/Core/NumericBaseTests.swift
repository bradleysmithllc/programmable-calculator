//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import _RPN_

class NumericBaseTests: TestSuperclass {
	func testDefaultBase10() {
		XCTAssertEqual(10, stack.getNumericBase());
	}

	func testHex() {
		do
		{
			try calculator.invoke("hex");
			XCTAssertEqual(16, stack.getNumericBase());
		}
		catch
		{
			XCTFail();
		}
	}
	
	func testOct() {
		do
		{
			try calculator.invoke("oct");
			XCTAssertEqual(8, stack.getNumericBase());
		}
		catch
		{
			XCTFail();
		}
	}

	func testHex2() {
		XCTAssertEqual("29D", CalculatorCellOperationUIView.displayNumber(669.13333333, numericBehavior: .Decimal, integerWordSize: nil, numericBase: 16, decimals: 5));
	}
	
	func testBug() {
		do
		{
			try calculator.invoke("dec");
			try calculator.invoke("1");
			try calculator.invoke("00");
			try calculator.invoke("3");
			try calculator.invoke(".");
			try calculator.invoke("00");
			try calculator.invoke("7");
			try calculator.invoke("ent");
			try calculator.invoke("7");
			try calculator.invoke("/");
			XCTAssertEqual(NSDecimalNumber(value: 143.28671 as Double).decimalNumberRoundedToDigits(5), stack.peek().decimalNumberRoundedToDigits(5));

			try calculator.invoke("hex");

			XCTAssertEqual("8F", CalculatorCellOperationUIView.displayNumber(stack.pop(), numericBehavior: .Decimal, integerWordSize: nil, numericBase: 16, decimals: 5));
		}
		catch
		{
			XCTFail();
		}
	}
	
	func testDec() {
		do
		{
			// change from base 10 to make sure it changes
			try calculator.invoke("hex");
			try calculator.invoke("dec");
			XCTAssertEqual(10, stack.getNumericBase());
		}
		catch
		{
			XCTFail();
		}
	}

	func testBin() {
		do
		{
			try calculator.invoke("bin");
			XCTAssertEqual(2, stack.getNumericBase());
		}
		catch
		{
			XCTFail();
		}
	}
	
	/*I typed in the literal CAFEBABE in hex mode, signed 2-byte.  The result was CAFE,BABE in Y and -4542 in X.*/
	func testEntryBug()
	{
		stack.setNumericBehavior(NumericBehavior.SignedInteger);
		stack.setIntegerWordSize(IntegerWordSize.word_2);

		do
		{
			try calculator.invoke("hex");

			try calculator.invoke("C");
			try calculator.invoke("A");
			try calculator.invoke("F");
			try calculator.invoke("E");
			try calculator.invoke("B");
			try calculator.invoke("A");
			try calculator.invoke("B");
			try calculator.invoke("E");

			try calculator.invoke("ent");

			XCTAssertEqual(-0x4542, stack.getY());
			XCTAssertEqual(-0x4542, stack.getX());
		}
		catch
		{
			XCTFail();
		}
	}
}
