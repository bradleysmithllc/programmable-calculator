//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import _RPN_

class EnterTests: TestSuperclass {
	func testEmpty() {
		do {
			try calculator.invoke("ent");
		} catch _ {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 0);
		XCTAssertEqual(calculator.stack().getX(), NSDecimalNumber.zero);
	}

	func testBasic() {
		calculator.stack().setX(makeNumber("1.0"));
		calculator.stack().push(makeNumber("0.5"));

		do {
			try calculator.invoke("ent");
		} catch _ {
			XCTFail();
		}

		XCTAssert(calculator.stack().size() == 3);

		XCTAssert(calculator.stack().pop() == makeNumber("0.5"));
		XCTAssert(calculator.stack().pop() == makeNumber("0.5"));
		XCTAssert(calculator.stack().pop() == makeNumber("1.0"));
	}

	func testLittleMoreComplex() {
		do {
			try calculator.invoke("1");
			try calculator.invoke("ent");
			try calculator.invoke("2");

			XCTAssertEqual(calculator.stack().size(), 2);

			XCTAssertEqual(calculator.stack().getX(), makeNumber(integer: 2));
			XCTAssertEqual(calculator.stack().getY(), makeNumber(integer: 1));
		} catch _ {
			XCTFail();
		}
	}
}
