//
//  CalculatorCoreTests.swift
//  CalculatorCoreTests
//
//  Created by Bradley Smith on 10/5/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import _RPN_

class CalculatorTests: TestSuperclass {
	func testPropertyNotExists() {
		let v: String? = stack.property("Hi");
		XCTAssertNil(v);
	}

	func testPropertyNotExistsDefaultNil() {
		let v: String? = stack.propertyWithDefault("Hi", defaultValue: nil);
		XCTAssertNil(v);
	}

	func testPropertyNotExistsWithDefault() {
		let v: String = stack.propertyWithDefault("Hi", defaultValue: "Nil");
		XCTAssertEqual(v, "Nil");
	}

	func testPropertyExistsWithDefault() {
		let _ = stack.setProperty(CalculatorProperties.NUMERIC_ENTRY_DECIMAL, propertyValue: "Boo");
		let v: String = stack.propertyWithDefault(CalculatorProperties.NUMERIC_ENTRY_DECIMAL, defaultValue: "Nilly");
		XCTAssertEqual(v, "Boo");
	}

	func testIntPropertyNotExistsDefaultNil() {
		let v: Int? = stack.propertyWithDefault("Hi", defaultValue: nil);
		XCTAssertNil(v);
	}

	func testIntPropertyNotExistsWithDefault() {
		let v: Int = stack.propertyWithDefault("Hi", defaultValue: 1);
		XCTAssertEqual(v, 1);
	}

	func testIntPropertyExistsWithDefault() {
		let _ = stack.setProperty(CalculatorProperties.NUMERIC_ENTRY_DECIMAL, propertyValue: 1);
		let v: Int = stack.propertyWithDefault(CalculatorProperties.NUMERIC_ENTRY_DECIMAL, defaultValue: 2);
		XCTAssertEqual(v, 1);
	}

	func testNSDecimalNumberPropertyNotExistsDefaultNil() {
		let v: NSDecimalNumber? = stack.propertyWithDefault("Hi", defaultValue: nil);
		XCTAssertNil(v);
	}

	func testNSDecimalNumberPropertyNotExistsWithDefault() {
		let v: NSDecimalNumber = stack.propertyWithDefault("Hi", defaultValue: NSDecimalNumber.one);
		XCTAssertEqual(v, NSDecimalNumber.one);
	}

	func testNSDecimalNumberPropertyExistsWithDefault() {
		let _ = stack.setProperty(CalculatorProperties.NUMERIC_ENTRY_DECIMAL, propertyValue: NSDecimalNumber.one);
		let v: NSDecimalNumber = stack.propertyWithDefault(CalculatorProperties.NUMERIC_ENTRY_DECIMAL, defaultValue: NSDecimalNumber.zero);
		XCTAssertEqual(v, NSDecimalNumber.one);
	}

	func testPropertiesReset() {
		let _ = stack.setProperty(CalculatorProperties.NUMERIC_ENTRY_DECIMAL, propertyValue: NSDecimalNumber.one);
		calculator.reset();

		XCTAssertNil(stack.property(CalculatorProperties.NUMERIC_ENTRY_DECIMAL));
	}
}
