//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import _RPN_

class ConstantTests: TestSuperclass {
	func testπ() {
		do {
			try calculator.invoke("pi_constant");
		} catch _ {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), NSDecimalNumber(value: Double.pi).decimalNumberRoundedToDigits(stack.getDecimals()));
	}

	func test2π() {
		do {
			try calculator.invoke("two_pi_constant");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), NSDecimalNumber(value: Double.pi).multiplying(by: NSDecimalNumber(value: 2 as Int)).decimalNumberRoundedToDigits(stack.getDecimals()));
	}

	func testE() {
		do {
			try calculator.invoke("e_constant");
		} catch _ {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), NSDecimalNumber(value: M_E as Double).decimalNumberRoundedToDigits(stack.getDecimals()));
	}

	func testEuler() {
		do {
			try calculator.invoke("euler_constant");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), NSDecimalNumber(string: "0.57721566490153286060651209008240243104").decimalNumberRoundedToDigits(stack.getDecimals()));
	}

	func testGoldenRatio() {
		do {
			try calculator.invoke("gr_constant");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), NSDecimalNumber(string: "1.61803398874989484820458683436563811772030917980576").decimalNumberRoundedToDigits(stack.getDecimals()));
	}
}
