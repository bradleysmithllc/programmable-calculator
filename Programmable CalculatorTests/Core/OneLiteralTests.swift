//
//  ModulusTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
//@testable import Programmable Calculator

class OneLiteralTests: TestSuperclass {
	func testEmpty() {
		do {
			try calculator.invoke("1");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), NSDecimalNumber.one);
	}

	func testMultipleZeros() {
		do {
			try calculator.invoke("1");
			try calculator.invoke("1");
			try calculator.invoke("1");
			try calculator.invoke("1");
			try calculator.invoke("1");
			try calculator.invoke("1");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber("111111"));
	}
}
