//
//  AppDelegate.swift
//  Programmable Calculator
//
//  Created by Bradley Smith on 11/6/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

open class Entrails2
{
	let _calculator: Calculator = CalculatorCore();
	let _configuration: Configuration;
	
	fileprivate init()
	{
		_configuration = Configuration(calculator: _calculator);
	}
	
	fileprivate static var entrails: Entrails2?;
	
	open static func initSingleton()
	{
		entrails = Entrails2();
	}
	
	open static var calculator: Calculator
	{
		get
		{
			return entrails!._calculator;
		}
	}
	
	open static var configuration: Configuration
	{
		get
		{
			return entrails!._configuration;
		}
	}
}
