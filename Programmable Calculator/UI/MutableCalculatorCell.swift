//
//  DefaultCell.swift
//  Programmers Calculator
//
//  Created by Bradley Smith on 10/26/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

open class Cell
{
	let operationId: String?;
	var operation: Operation?;
	let overrideOperationType: OperationType?;

	let uiFunction: UIFunction?;

	let cellFunction: function;

	init(
		operationId: String,
		overrideOperationType: OperationType?
	)
	{
		cellFunction = .operation;

		uiFunction = nil;

		self.operationId = operationId;
		self.overrideOperationType = overrideOperationType;
	}

	init(
		uiFunction: UIFunction
		)
	{
		cellFunction = .function;

		operationId = nil;
		overrideOperationType = nil;

		self.uiFunction = uiFunction;
	}
}

open class MutableCalculatorCell: CalculatorCell
{
	fileprivate var _layers: [Layer: Cell] = [Layer: Cell]();
	fileprivate var activeLayers: [Layer] = [Layer]();

	fileprivate let _primalRow: UInt8;
	fileprivate let _rowSpan: UInt8;
	
	fileprivate let _primalColumn: UInt8;
	fileprivate let _columnSpan: UInt8;

	fileprivate let _cellKey: String;
	fileprivate var layout: Layout?;

	fileprivate var extendsRight: Bool = false;
	fileprivate var extendsDown: Bool = false;

	convenience init(primalRow: UInt8, primalColumn: UInt8, rowSpan: UInt8, columnSpan: UInt8, cellKey: String)
	{
		self.init(layout: nil, primalRow: primalRow, primalColumn: primalColumn, rowSpan: rowSpan, columnSpan: columnSpan, cellKey: cellKey);
	}

	init(layout: Layout?, primalRow: UInt8, primalColumn: UInt8, rowSpan: UInt8, columnSpan: UInt8, cellKey: String)
	{
		self.layout = layout;
		
		self._primalRow = primalRow;
		self._rowSpan = rowSpan;
		self._primalColumn = primalColumn;
		self._columnSpan = columnSpan;
		_cellKey = cellKey;
	}

	convenience init(layout: Layout, primalRow: UInt8, primalColumn: UInt8)
	{
		self.init(layout: layout, primalRow: primalRow, primalColumn: primalColumn, rowSpan: 1, columnSpan: 1, cellKey: "\(primalRow).\(primalColumn).1.1");
	}

	func extends(right: Bool, down: Bool)
	{
		extendsRight = right;
		extendsDown = down;
	}
	
	open func cellKey() -> String
	{
		return _cellKey;
	}

	open func columnSpan() -> UInt8
	{
		if let _ = layout
		{
			return deprecatedColumnSpan();
		}

		return _columnSpan;
	}

	func deprecatedColumnSpan() -> UInt8
	{
		// scan for extends right and adjust the column span
		var columnSpan: UInt8 = 1;

		if extendsRight, let l = layout
		{
			for column in primalColumn()..<l.columns()
			{
				let mcc = l.cell(row: primalRow(), column: column) as! MutableCalculatorCell

				if mcc.extendsRight
				{
					columnSpan += 1;
				}
				else
				{
					break;
				}
			}
		}

		return columnSpan;
	}

	open func rowSpan() -> UInt8
	{
		if let _ = layout
		{
			return deprecatedRowSpan();
		}
		
		return _rowSpan;
	}
	
	func deprecatedRowSpan() -> UInt8
	{
		return 1;
	}

	open func primalRow() -> UInt8
	{
		return _primalRow;
	}

	open func primalColumn() -> UInt8
	{
		return _primalColumn;
	}

	fileprivate func addCell(_ cell: Cell, layerName: Layer)
	{
		if (!activeLayers.contains(layerName))
		{
			activeLayers.append(layerName);
		}
		
		_layers[layerName] = cell;
	}

	open func profileKey(_ layer: Layer) -> String
	{
		// determine if this is an operation or a function
		switch (cellFunction(layer))
		{
		case .function:
			return uiFunctionType(layer).rawValue;
		case .operation:
			// there is an override.  Return that
			if let oot = overrideOperationType(layer)
			{
				return oot.rawValue;
			}
			else
			{
				// no override - if the operation is valid return it's code
				if let op = operation(layer)
				{
					return op.operationType().rawValue;
				}
			}
		}

		return "placeholder";
	}

	open func addOperation(
		_ layerName: Layer,
		operationId: String,
		overrideOperationType: OperationType?
		)
	{
		addCell(Cell(operationId: operationId, overrideOperationType: overrideOperationType), layerName: layerName);
	}

	open func addUIFunction(
		_ layerName: Layer,
		uiFunction: UIFunction
		)
	{
		addCell(Cell(uiFunction: uiFunction), layerName: layerName);
	}

	open func layers() -> [Layer]
	{
		return activeLayers;
	}

	open func populateOperations(_ calculator: Calculator)
	{
		for (_, cell) in _layers
		{
			if let cid = cell.operationId
			{
				cell.operation = calculator.operation(cid);
			}
		}
	}

	open func operation(_ layer: Layer) -> Operation?
	{
		if let cell = _layers[layer], let oid = cell.operation
		{
			return oid;
		}

		if let cell = _layers[Layer.normal], let oid = cell.operation
		{
			return oid;
		}

		return nil;
	}

	open func cellFunction(_ layer: Layer) -> function {
		if let cell = _layers[layer]
		{
			return cell.cellFunction;
		}

		if let cell = _layers[Layer.normal]
		{
			return cell.cellFunction;
		}

		return .function;
	}

	open func overrideOperationType(_ layer: Layer) -> OperationType?
	{
		if let cell = _layers[layer]
		{
			return cell.overrideOperationType;
		}

		if let cell = _layers[Layer.normal]
		{
			return cell.overrideOperationType;
		}

		
		return nil;
	}

	open func uiFunctionType(_ layer: Layer) -> UIFunction
	{
		if let cell = _layers[layer], let uif = cell.uiFunction
		{
			return uif;
		}

		if let cell = _layers[Layer.normal], let uif = cell.uiFunction
		{
			return uif;
		}
		
		return .placeholder;
	}
}
