//
//  TextDrawing.swift
//  ViewTextTest
//
//  Created by Bradley Smith on 11/19/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


public enum AlignPosition
{
	case top;
	case bottom;
	case right;
	case left;
	case center;
	
	static let allValues =
	[
		top,
		bottom,
		right,
		left,
		center
	];
}

open class TextDrawing
{
	open static func makeAttributedStringForBounds(
		string string1: String,
		font: UIFont,
		viewRect: CGRect,
		textColor: UIColor
		) -> (attributedString: NSAttributedString, boundsRect: CGRect)
	{
		//let font: UIFont = UIFont(name: "Courier New", size: 1.0)!;

		let parse_script_result = parseScripts(string1);
		
		let options: NSStringDrawingOptions = [.usesLineFragmentOrigin, .usesFontLeading];
		var attributes: [String: AnyObject] = [NSForegroundColorAttributeName: textColor];
		
		//var attributedString: NSMutableAttributedString? = nil;
		var lastGoodAttributedString: NSMutableAttributedString? = nil;
		
		var currentSize: CGFloat = 1.0;
		
		var currentRect: CGRect? = nil;
		var lastGoodRect: CGRect? = nil;
		
		var currentSizeDelta: CGFloat = 10.0;
		
		repeat
		{
			let fnt = font.withSize(currentSize);
			
			attributes[NSFontAttributeName] = fnt;
			
			let attributedString = NSMutableAttributedString(
				string: parse_script_result.result,
				attributes: attributes
			);
			
			attributeScripts(attributedString, ranges: parse_script_result.ranges)
			
			currentRect = attributedString.boundingRect(
					with: CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude),
					options: options,
					context: nil
				).integral;
			
			if viewRect.size.width < currentRect!.size.width || viewRect.size.height < currentRect?.size.height
			{
				if currentSizeDelta == 10.0
				{
					currentSize -= 10.0;
					currentSizeDelta = 1.0;
				}
				else if currentSizeDelta == 1.0
				{
					currentSize -= 1.0;
					currentSizeDelta = 0.1;
				}
				else
				{
					break;
				}
			}
			else
			{
				// record last working size since current is smaller than bounds
				lastGoodAttributedString = attributedString;
				lastGoodRect = currentRect;
			}
			
			currentSize += currentSizeDelta;
		}
			while true
		
		if let lgs = lastGoodAttributedString
		{
			return (attributedString: lgs, boundsRect: lastGoodRect!);
		}
		else
		{
			return (attributedString: NSAttributedString(string: "~", attributes: attributes), boundsRect: CGRect(x: 0, y: 0, width: 10, height: 10));
		}
	}
	
	open static func drawTextCentered(
		context: CGContext, attributedString: NSAttributedString, viewRect: CGRect, textRect: CGRect, alignment: AlignPosition
		)
	{
		if (textRect.size.width > viewRect.size.width || textRect.size.height > textRect.size.height)
		{
			print("Too big");
		}
		
		// calculate x and y offset to center the rect
		let delta_x = viewRect.size.width - textRect.width;
		let delta_y = viewRect.size.height - textRect.height;
		
		var origin_x: CGFloat;
		var origin_y: CGFloat;
		
		switch (alignment)
		{
		case .top:
			// x is centered
			origin_x = (viewRect.origin.x) + delta_x / 2.0;
			// y is 0
			origin_y = viewRect.origin.y;
			break
		case .right:
			// x is viewRect.width - textRect.width
			origin_x = viewRect.origin.x + viewRect.size.width - textRect.size.width;
			// y is centered
			origin_y = (viewRect.origin.y) + delta_y / 2.0;
			break
		case .bottom:
			// x is centered
			origin_x = (viewRect.origin.x) + delta_x / 2.0;
			// y is viewRect.height - textRect.height
			origin_y = viewRect.origin.y + viewRect.size.height - textRect.size.height;
			break
		case .left:
			// x is 0
			origin_x = viewRect.origin.x;
			// y is centered
			origin_y = (viewRect.origin.y) + delta_y / 2.0;
			break
		case .center:
			// x is centered
			origin_x = (viewRect.origin.x) + delta_x / 2.0;
			// y is centered
			origin_y = (viewRect.origin.y) + delta_y / 2.0;
			break
		}
		
		let realRect = textRect.offsetBy(dx: origin_x, dy: origin_y).integral;
		
		context.saveGState();
		context.translateBy(x: realRect.origin.x, y: realRect.origin.y);
		
		let drawingRect = CGRect(x: 0.0, y: 0.0, width: realRect.size.width, height: realRect.size.height);
		
		//CGContextStrokeRect(context, drawingRect);
		
		attributedString.draw(in: drawingRect);
		context.restoreGState();
	}
	
	enum parse_state
	{
		case in_subscript;
		case in_superscript;
		case normal;
	}
	
	open static func parseScripts(_ string: String) -> (result: String, ranges: (sub: [(begin: Int, end: Int)], sup: [(begin: Int, end: Int)]))
	{
		var result = (result: "", ranges: (sub: [(begin: Int, end: Int)](), sup: [(begin: Int, end: Int)]()));
		
		//var state = parse_state.normal;
		
		// temporary - it's got to get better than this . . .
		
		// replace the meta tags to simplify parsing
		/*
		var newString = string.stringByReplacingOccurrencesOfString("<sath>", withString: "")
		newString = newString.stringByReplacingOccurrencesOfString("</ sath>", withString: "")
		newString = newString.stringByReplacingOccurrencesOfString("<ssup>", withString: "")
		newString = newString.stringByReplacingOccurrencesOfString("</ ssup>", withString: "")
		newString = newString.stringByReplacingOccurrencesOfString("<ssub>", withString: "")
		newString = newString.stringByReplacingOccurrencesOfString("</ ssub>", withString: "")
		newString = newString.stringByReplacingOccurrencesOfString("<si>", withString: "")
		newString = newString.stringByReplacingOccurrencesOfString("</ si>", withString: "")

		newString = newString.stringByReplacingOccurrencesOfString("<sn>", withString: replBegin)
		newString = newString.stringByReplacingOccurrencesOfString("</ sn>", withString: replEnd)
		*/
		
		var voidCharCount = 0;
		
		var currentBeginIndex = 0;
		
		for (index, character) in string.characters.enumerated()
		{
			switch(character)
			{
			case "{":
				//state = .in_superscript;
				currentBeginIndex = index - voidCharCount;
				voidCharCount += 1;
				break;
			case "}":
				//state = .normal;
				result.ranges.sup.append((begin: currentBeginIndex, end: index - voidCharCount))
				voidCharCount += 1;
				break;
			case "[":
				//state = .in_subscript;
				currentBeginIndex = index - voidCharCount;
				voidCharCount += 1;
				break;
			case "]":
				//state = .normal;
				result.ranges.sub.append((begin: currentBeginIndex, end: index - voidCharCount))
				voidCharCount += 1;
				break;
			default:
				result.result.append(character);
				break;
			}
		}

		return result;
	}
	
	fileprivate static func attributeScripts(
		_ attributedString: NSMutableAttributedString,
		ranges: (sub: [(begin: Int, end: Int)], sup: [(begin: Int, end: Int)])
		)
	{
		// grab the current font and color
		var attributes = attributedString.attributes(at: 0, longestEffectiveRange: nil, in: NSMakeRange(0, 1));
		
		let font = attributes[NSFontAttributeName]! as! UIFont;
		
		let scriptFont = font.withSize(font.pointSize * 0.6);
		
		attributes[NSFontAttributeName] = scriptFont;
		
		for range in ranges.sub
		{
			let range = NSMakeRange(range.begin, range.end - range.begin);
			
			attributes[NSBaselineOffsetAttributeName] = font.pointSize * -0.2;
			
			attributedString.setAttributes(attributes, range: range)
		}

		for range in ranges.sup
		{
			let range = NSMakeRange(range.begin, range.end - range.begin);
			
			attributes[NSBaselineOffsetAttributeName] = font.pointSize * 0.4;
			
			attributedString.setAttributes(attributes, range: range)
		}
	}
}
