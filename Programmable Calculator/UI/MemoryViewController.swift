//
//  ViewController.swift
//  Programmers Calculator
//
//  Created by Bradley Smith on 10/21/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import UIKit

class MemoryViewController: UIViewController
{
	@IBOutlet var calculatorViewController: CalculatorViewController!;
	@IBOutlet var memoryTableView: UITableView!;

	var memoryTableDataSource: MemoryTableDataSource?;

	override func viewDidLoad()
	{
		super.viewDidLoad()

		memoryTableDataSource = MemoryTableDataSource();
		
		memoryTableView.dataSource = memoryTableDataSource;
		memoryTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
}

class MemoryTableDataSource: NSObject, UITableViewDataSource
{
	var memory: [String: NSDecimalNumber]?;
	var keysOrdered: [String]?;
	
	@objc func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		memory = AppDelegate.calculator.stack().memory();
		
		let keySet = memory!.keys;
		keysOrdered = keySet.sorted() { $0 < $1 };

		return keysOrdered!.count;
	}
	
	@objc func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell")!;
		
		let row = indexPath.row;
		
		let rowText = keysOrdered![row] + ": " + CalculatorCellOperationUIView.displayNumber(memory![keysOrdered![row]]!, calculator: AppDelegate.calculator);

		cell.textLabel?.text = rowText;
		
		return cell;
	}
	
	private func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
	}
}
