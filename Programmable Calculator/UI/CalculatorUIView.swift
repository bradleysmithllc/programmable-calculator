//
//  CalculatorGridView.swift
//  Programmers Calculator
//
//  Created by Bradley Smith on 10/23/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import UIKit;

open class CalculatorUIView: UIView {
	private let _calculator: Calculator = CalculatorCore();
	private let _configuration: Configuration;

	private var _runningOperation: Bool = false;
	private var _touching: String? = nil;
	
	private var displays: [UIView] = [UIView]();
	private var animator: Animator?;
	
	var runningOperation: Bool
	{
		get
		{
			return _runningOperation;
		}
	}
	
	var animating: Bool
	{
		get
		{
			if let a = animator
			{
				if (!a.done)
				{
					return true;
				}
			}

			return false;
		}
	}

	var _displayOffset: CGFloat = 0.0;

	var displayOffset: CGFloat
	{
		get {
			return _displayOffset;
		}
		set {
			_displayOffset = newValue;
		}
	}

	open var configuration: Configuration
		{
		get
		{
			return _configuration;
		}
	}

	open var calculator: Calculator
		{
		get
		{
			return _calculator;
		}
	}

	open var stack: Stack
		{
		get
		{
			return calculator.stack();
		}
	}

	var viewController: CalculatorViewController?;
	
	fileprivate var cellRectMap: [Int: CalculatorCellRect] = [Int: CalculatorCellRect]();

	required public init?(coder aDecoder: NSCoder) {
		_configuration = Configuration(calculator: _calculator);
		
		super.init(coder: aDecoder)
		
		NotificationCenter.default.addObserver(self, selector: #selector(CalculatorUIView.rotated), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil);
		
		calculator.stack().setCheckPointDepth(100);
	}

	override init(frame: CGRect) {
		_configuration = Configuration(calculator: _calculator);
		
		super.init(frame: frame)
		
		NotificationCenter.default.addObserver(self, selector: #selector(CalculatorUIView.rotated), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil);
		
		calculator.stack().setCheckPointDepth(100);
	}
	
	func beginOperation()
	{
		_runningOperation = true;
	}
	
	func completeOperation()
	{
		_runningOperation = false;
	}

	func touch(cellUIView: String)
	{
		_touching = cellUIView;
	}
	
	func untouch()
	{
		// update visual state of what was being touched
		if let touching = _touching
		{
			for view in subviews
			{
				if view is CalculatorCellOperationUIView
				{
					if let opView = (view as? CalculatorCellOperationUIView)
					{
						if let op = opView.operation
						{
							if (op.id() == touching)
							{
								view.setNeedsDisplay();
							}
						}
					}
				}
			}
		}

		_touching = nil;
	}
	
	func touchingMe(cellUIView: String) -> Bool
	{
		return _touching == cellUIView;
	}
	
	open override func layoutSubviews()
	{
		displays.removeAll();

		for tView in subviews
		{
			tView.removeFromSuperview();
		}
		
		updateInternalView();
		
		for (i, ccRect) in cellRectMap
		{
			let sv: CalculatorCellUIView;

			switch(ccRect.cell.cellFunction(activeLayer))
			{
				case .function:
					let type = ccRect.cell.uiFunctionType(activeLayer);

					switch(type)
					{
						case .display_x:
							sv = CalculatorCellDisplayFunctionUIView();
							displays.append(sv)
						case .display_y:
							sv = CalculatorCellDisplayFunctionUIView();
							displays.append(sv)
						case .display_z:
							sv = CalculatorCellDisplayFunctionUIView();
							displays.append(sv)
						case .display_t:
							sv = CalculatorCellDisplayFunctionUIView();
							displays.append(sv)
						default:
							sv = CalculatorCellButtonFunctionUIView();
					}

				case .operation:
					sv = CalculatorCellOperationUIView();
				}

			sv.setUp(self, cell: ccRect.cell);
			sv.frame = ccRect.rect;
			
			sv.backgroundColor = UIColor(red: ((CGFloat(i) + sv.frame.origin.x * 10).truncatingRemainder(dividingBy: 255)) / 255, green: ((CGFloat(i) + sv.frame.origin.y * 10).truncatingRemainder(dividingBy: 255)) / 255, blue: ((CGFloat(i) + sv.frame.origin.x + CGFloat(i) + sv.frame.origin.y * 10).truncatingRemainder(dividingBy: 255)) / 255, alpha: 1);

			addSubview(sv);
		}

		// give the layout a chance to approve of the current state
		activeLayout.enforceConsistency(calculator);
	}

	func animate(_ startingPoint: Int, endPoint: Int)
	{
		animator = Animator(startingPoint: startingPoint, endPoint: endPoint, increment: (startingPoint > endPoint) ? -1 : 1);
	}

	func rotated()
	{
		// do not respond to portrait upside down, face up, face down, or unknown
		switch(UIDevice.current.orientation)
		{
		case .unknown:
			print("Unknown");
		case .portrait: // Device oriented vertically, home button on the bottom
			print("Portrait");
			_configuration.landscape(false);
		case .portraitUpsideDown: // Device oriented vertically, home button on the top
			print("PortraitUpsideDown");
			//_configuration.landscape(true);
		case .landscapeLeft: // Device oriented horizontally, home button on the right
			_configuration.landscape(true);
			print("LandscapeLeft");
		case .landscapeRight: // Device oriented horizontally, home button on the left
			_configuration.landscape(true);
			print("LandscapeRight");
		case .faceUp: // Device oriented flat, face up
			print("FaceUp");
			//_configuration.landscape(true);
		case .faceDown: // Device oriented flat, face down
			print("FaceDown");
			//_configuration.landscape(true);
		}

		// adjust to landscape mode
		/*
		if(
			UIDevice.currentDevice().orientation.isLandscape
		)
		{
			print("Landscape");
			_configuration.landscape(true);
		} else {
			print("!Landscape");
			_configuration.landscape(false);
		}
		*/
		
		setNeedsLayout();
	}
	
	var activeProfile: Profile
	{
		get
		{
			return _configuration.activeProfile;
		}
	}
	
	var activeLayout: Layout
	{
		get
		{
			return _configuration.activeLayout;
		}
	}
	
	var activeLayer: Layer
	{
		get
		{
			return _configuration.activeLayer;
		}
	}

	func redrawSubviews()
	{
		var viewsToRedraw = 0;

		for view in subviews
		{
			if let ov = view as? CalculatorCellOperationUIView
			{
				// check mnemonic against last version
				if ov.needsRefresh
				{
					ov.setNeedsDisplay();
					viewsToRedraw += 1;
				}
			}
			else
			{
				// always refresh non-operations
				view.setNeedsDisplay();
				viewsToRedraw += 1;
			}
		}
		
		//print("Redraw All \(viewsToRedraw)");
	}

	func updateInternalView()
	{
		// calculate the size of each cell
		cellRectMap = CalculatorUIView.buildRectMap(activeLayout, layer: activeLayer, screenSize: bounds.size);
	}
	
	class func calculateCellSize(viewRect: CGRect, rows: Int, columns: Int) -> CGRect
	{
		let cPoint: CGPoint = CGPoint(x: 0, y: 0);
		
		let width = viewRect.width.native;
		let height = viewRect.height.native;
		
		let cRect: CGRect =
		CGRect(
			origin: cPoint,
			size: CGSize(
				width: CGFloat((width / CGFloat.NativeType(columns))) - 0.9,
				height: CGFloat((height / CGFloat.NativeType(rows))) - 0.9
			)
		);
		
		return cRect.integral;
	}
	
	/**Given the top-left rect, calculate the size of the rects of all cells
	*/
	static func buildRectMap(_ layout: Layout, layer: Layer, screenSize: CGSize) -> [Int: CalculatorCellRect]
	{
		// Temporary maps for tracking cells
		var cellMap = [Int: CalculatorCell]();
		var cellRectMap = [Int: CGRect]();
		
		// determine the widths and heights of all cells
		let colWidths: [UInt16] = weightSpans(layout.columns(), width: UInt16(screenSize.width), weights: layout.columnWeights());
		let rowHeights: [UInt16] = weightSpans(layout.rows(), width: UInt16(screenSize.height), weights: layout.rowWeights());

		let rectOwnerArray: [[Int]] = buildRectArray(layout, layer: layer);
		
		var cumulativeColWidth: UInt16 = 0;
		var cumulativeRowHeight: UInt16 = 0;
		
		// two-dimensional array.  iterate over both dimensions
		for (row_index, row) in rectOwnerArray.enumerated()
		{
			for (col_index, ownerId) in row.enumerated()
			{
				// calculate cell bounds
				let origin_x = CGFloat(cumulativeColWidth);
				let origin_y = CGFloat(cumulativeRowHeight);
				
				// given this id, calculate the rect that goes with this cell.
				let width = colWidths[col_index];
				let height = rowHeights[row_index];
				
				let thisCellRect: CGRect = CGRect(
						x: origin_x, y: origin_y, width: CGFloat(width), height: CGFloat(height)
					).integral;
				
				let key = (row_index * 1000) + col_index;
				
				let cell = layout.cell(row: UInt8(row_index), column: UInt8(col_index));
				
				cellMap[key] = cell;
				
				// lookup a prior value for this rect
				if (cellRectMap[ownerId] == nil)
				{
					cellRectMap[ownerId] = thisCellRect;
				}
				else
				{
					// union this new rect with the old one
					cellRectMap[ownerId] = cellRectMap[ownerId]!.union(thisCellRect);
				}
				
				cumulativeColWidth += colWidths[col_index];
			}
			
			cumulativeColWidth = 0;
			cumulativeRowHeight += rowHeights[row_index];
		}
		
		var rectArr = [Int: CalculatorCellRect]();
		
		// run through and combine into the array
		for (cellId, rect) in cellRectMap
		{
			// look up the cell
			let cell = cellMap[cellId]!;
			
			rectArr[cellId] = CalculatorCellRect(pcell: cell, prect: rect);
		}
		
		return rectArr;
	}
	
	/**Creates a map of grid id's showing which ones extend across cell borders.
	*/
	static func buildRectArray(_ layout: Layout, layer: Layer) -> [[Int]]
	{
		// first pass - label all the cells with the cell owner
		var rectOwnerArray = [[Int]]();
		
		let rowCount = layout.rows();
		let colCount = layout.columns();
		
		for row in 0..<Int(rowCount)
		{
			rectOwnerArray.append([Int]());
			
			for _ in 0..<Int(colCount)
			{
				rectOwnerArray[row].append(-1);
			}
		}
		
		let cells = layout.cells();
		
		for cell in cells
		{
			let primalRow = cell.primalRow();
			let primalColumn = cell.primalColumn();
			let rowSpan = cell.rowSpan();
			let columnSpan = cell.columnSpan();
			
			for row in primalRow..<(primalRow + rowSpan)
			{
				for column in primalColumn..<(primalColumn + columnSpan)
				{
					if rectOwnerArray[Int(row)][Int(column)] == -1
					{
						rectOwnerArray[Int(row)][Int(column)] = Int(primalRow) * 1000 + Int(primalColumn);
					}
				}
			}
		}

		/*
		for row in 0..<Int(rowCount)
		{
			for column in 0..<Int(colCount)
			{
				let cell = layout.cell(row: UInt8(row), column: UInt8(column));
				
				if (rectOwnerArray[row][column] == -1)
				{
					rectOwnerArray[row][column] = row * 1000 + column;
				}
				
				let cellId = rectOwnerArray[row][column]

				/*
				if (cell.extendsDown(layer))
				{
					rectOwnerArray[row + 1][column] = cellId;
				}

				//if (cell.extendsUp(layer))
				//{
				//	rectOwnerArray[row - 1][column] = cellId;
				//}
				
				if (cell.extendsRight(layer))
				{
					rectOwnerArray[row][column + 1] = cellId;
				}
				
				//if (cell.extendsLeft(layer))
				//{
				//	rectOwnerArray[row][column - 1] = cellId;
				//}
				*/
			}
		}
		*/
		
		return rectOwnerArray;
	}

	/*Calculate segments based on weighting*/
	open static func weightSpans(_ numberOfSegments: UInt8, width: UInt16, weights: [UInt8]) -> [UInt16]
	{
		// first add total number of weight segments
		let weightSegments = totalWeights(weights);

		// divide span into total segments
		let gaps = CalculatorUIView.distributeGaps(weightSegments, width: width);

		return distributeSegmentsToCells(gaps, weights: weights);
	}

	/*Given the gaps, distribute segments to columns by weights supplied*/
	open static func distributeSegmentsToCells(_ gaps: [UInt16], weights: [UInt8]) -> [UInt16]
	{
		var cells = [UInt16]();

		var segmentNum = 0;
		
		for weight in weights
		{
			// for each weight column, grab the number of segments requested
			var cellTotal: UInt16 = 0;

			for _ in 0..<weight
			{
				cellTotal += gaps[segmentNum];
				segmentNum += 1;
			}

			cells.append(cellTotal);
		}

		return cells;
	}
	
	/*Sum all weights*/
	open static func totalWeights(_ weights: [UInt8]) -> UInt8
	{
		var total: UInt8 = 0;

		for weight in weights
		{
			total += weight;
		}

		return total;
	}

	open static func distributeGaps(_ numberOfSegments: UInt8, width: UInt16) -> [UInt16]
	{
		let nos16: UInt16 = UInt16(numberOfSegments);
		
		if (numberOfSegments == 0)
		{
			return [UInt16]();
		}
		
		// initialize with common width
		let commonWidth: UInt16 = UInt16(width / nos16);
		
		var arr = [UInt16](repeating: commonWidth, count: Int(numberOfSegments));
	
		// determine remainder
		let remainder = Int(width % nos16);
		
		// distribute around the grid
		for gap in 0..<remainder
		{
			arr[gap] += 1;
		}
		
		return arr;
	}

	func activateLayer(_ layer: Layer)
	{
		_configuration.activeLayer = layer;
		setNeedsLayout()
	}
	
	func settings()
	{
		viewController!.settings(self);
	}
}

class CalculatorCellRect
{
	var cell: CalculatorCell;
	let rect: CGRect;
	
	init(pcell: CalculatorCell, prect: CGRect)
	{
		cell = pcell;
		rect = prect;
	}
}
