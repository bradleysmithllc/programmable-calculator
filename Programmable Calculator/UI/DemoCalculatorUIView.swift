//
//  CalculatorCellView.swift
//  Calc
//
//  Created by Bradley Smith on 1/12/16.
//  Copyright © 2016 Bradley Smith, LLC. All rights reserved.
//

import UIKit

/*Use the calculator view to build this cell*/
class DemoCalculatorUIView : CalculatorUIView
{
	fileprivate var profile: Profile? = nil;
	fileprivate var layout: Layout? = nil;
	fileprivate var displayName: String = "Unassigned";

	fileprivate var _activeLayer = Layer.normal;
	
	func setDisplayName(_ name: String)
	{
		displayName = name;
	}
	
	func setLayout(_ _layout: Layout)
	{
		layout = _layout;
	}

	func setProfile(_ _profile: Profile)
	{
		profile = _profile;
	}
	
	override var activeProfile: Profile
	{
		get
		{
			return profile!;
		}
	}

	override var activeLayer: Layer
	{
		get
		{
			return _activeLayer;
		}
	}
	
	override func activateLayer(_ layer: Layer)
	{
		_activeLayer = layer;
		self.setNeedsDisplay();
	}
	
	override var activeLayout: Layout
	{
		get
		{
			return layout!;
		}
	}
	
	/*Prevent actual operations
	*/
	func invoke(_ id: String) throws
	{
	}

	func getDisplayXText() -> String
	{
		return displayName;
	}
	
	override func settings()
	{
	}
}
