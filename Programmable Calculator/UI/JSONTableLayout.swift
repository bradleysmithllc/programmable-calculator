//
//  DefaultPerspective.swift
//  Programmers Calculator
//
//  Created by Bradley Smith on 10/26/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

open class JSONTableLayout: BaseDescribed, Layout
{
	fileprivate let _rows: [[CalculatorCell]];
	fileprivate var _cells: [CalculatorCell] = [CalculatorCell]();

	fileprivate var _rowWeights: [UInt8] = [UInt8]();
	fileprivate var _columnWeights: [UInt8] = [UInt8]();

	fileprivate let _layers: [Layer];
	fileprivate let version: String;

	fileprivate let _supportedNumericalBases: [UInt8]?;
	fileprivate let _supportedNumericBehaviors: [NumericBehavior]?;
	fileprivate let _supportedIntegerWordSizes: [IntegerWordSize]?;

	fileprivate let _orientation: Orientation;
	fileprivate let _suggestedDecimals: UInt8?;

	public init(id: String)
	{
		_supportedNumericalBases = [10];
		_orientation = Orientation.landscape;
		_suggestedDecimals = nil;

		_supportedNumericBehaviors = nil;
		_supportedIntegerWordSizes = nil;

		_rows = [[CalculatorCell]]();
		_layers = [Layer]();

		version = "JSONTableLayout:1.0";

		super.init(id: id, description: id, name: id);
	}

	public init(id: String, jsonRoot: JSON)
	{
		version = jsonRoot["json-layout-version"].string!;

		let or = jsonRoot["orientation"].string;

		_orientation = Orientation.mapValues[or!]!;

		if let suggestedDecimals = jsonRoot["suggestedDecimals"].int
		{
			_suggestedDecimals = UInt8(suggestedDecimals);
		}
		else
		{
			_suggestedDecimals = nil;
		}

		let description = jsonRoot["description"].string!;
		let name = jsonRoot["name"].string!;

		var supportedNumericalBases: [UInt8] = [UInt8]();
		
		// read the default numeric base
		if let bases = jsonRoot["supportedNumericBases"].array
		{
			for base in bases
			{
				supportedNumericalBases.append(UInt8(base.intValue));
			}
		}

		if (supportedNumericalBases.count == 0)
		{
			_supportedNumericalBases = nil;
		}
		else
		{
			_supportedNumericalBases = supportedNumericalBases;
		}

		var supportedNumericBehaviors: [NumericBehavior] = [NumericBehavior]();
		
		if let behavs = jsonRoot["supportedNumericBehaviors"].array
		{
			for behav in behavs
			{
				supportedNumericBehaviors.append(NumericBehavior.mapValues[behav.string!]!);
			}
		}

		if (supportedNumericBehaviors.count == 0)
		{
			_supportedNumericBehaviors = nil;
		}
		else
		{
			_supportedNumericBehaviors = supportedNumericBehaviors;
		}

		var supportedIntegerWordSizes: [IntegerWordSize] = [IntegerWordSize]()

		if let wrdszs = jsonRoot["supportedIntegerWordSizes"].array
		{
			for wrdsz in wrdszs
			{
				supportedIntegerWordSizes.append(IntegerWordSize.mapValues[wrdsz.string!]!);
			}
		}		
		
		if (supportedIntegerWordSizes.count == 0)
		{
			_supportedIntegerWordSizes = nil;
		}
		else
		{
			_supportedIntegerWordSizes = supportedIntegerWordSizes;
		}

		let layouts: JSON = jsonRoot["layout"];

		if layouts != JSON.null
		{
			let results = JSONTableLayout.readLayers(layouts);

			_layers = results.layers;
			_rows = results.rows;
		}
		else
		{
			_rows = [[CalculatorCell]]();
			_layers = [Layer]();
		}

		for row: [CalculatorCell] in _rows
		{
			for cell1: CalculatorCell in row
			{
				if !_cells.contains( where: { $0.cellKey() == cell1.cellKey() })
				{
					_cells.append(cell1);
				}
			}
		}

		super.init(id: id, description: description, name: name);
		
		if let rowWeights = jsonRoot["row-weights"].array
		{
			for weight in rowWeights
			{
				_rowWeights.append(weight.uInt8!);
			}
		}
		else
		{
			// initialize all weights at 1
			for _ in 0..<rows()
			{
				_rowWeights.append(UInt8(1));
			}
		}
		
		if let columnWeights = jsonRoot["column-weights"].array
		{
			for weight in columnWeights
			{
				_columnWeights.append(weight.uInt8!);
			}
		}
		else
		{
			// initialize all weights at 1
			for _ in 0..<columns()
			{
				_columnWeights.append(UInt8(1));
			}
		}
		
		// assert row weights and column weights match row and column counts
		assert(_rowWeights.count == Int(rows()));
		assert(_columnWeights.count == Int(columns()));
	}

	static func readLayers(_ layerData: JSON) -> (layers: [Layer], rows: [[CalculatorCell]])
	{
		var layers: [Layer] = [Layer]();

		var rows = [[CalculatorCell]]();

		// read the normal layer - that is mandatory
		JSONTableLayout.readRows(layerData["rows"].array!, rows: &rows, layer: Layer.normal);

		// the read all other layers
		let layersData = layerData["layers"];
		
		// top-level is layer-names
		for layerName: Layer in Layer.allValues
		{
			// not each layer need be present
			let layer = layersData[layerName.rawValue];

			if (layer != JSON.null)
			{
				layers.append(layerName);

				// this is another dictionary with one entry: rows - array
				if let jrows = layer.array
				{
					JSONTableLayout.readLayer(jrows, rows: rows, layer: layerName);
				}
			}
		}
		
		return (layers: layers, rows: rows);
	}
	
	static func readRows(_ rowData: [JSON], rows: inout [[CalculatorCell]], layer: Layer)
	{
		var cellKey = 0;

		var rowNum: Int = 0;
		
		for row in rowData
		{
			// start each row with a clone of the previous one - unless this is the first
			if (rowNum == 0)
			{
				rows.append([MutableCalculatorCell]());
			}
			else
			{
				// structs are copied on reference
				rows.append(rows[rowNum - 1]);
			}
			
			// get the corresponding data row
			var columnNum: Int = 0;

			for column in row.array!
			{
				let colSpan: UInt8;

				if let c = column["column-span"].uInt8
				{
					colSpan = c;
				}
				else
				{
					colSpan = 1;
				}

				let rowSpan: UInt8;
				if let c = column["row-span"].uInt8
				{
					rowSpan = c;
				}
				else
				{
					rowSpan = 1;
				}

				// we may need to skip first - look for row-spanning cells.
				if rows[rowNum].count > columnNum
				{
						while (Int(rows[rowNum][columnNum].primalRow() + rows[rowNum][columnNum].rowSpan()) > rowNum)
						{
							columnNum += 1;
						}
				}

				let mccell = MutableCalculatorCell(primalRow: UInt8(rowNum), primalColumn: UInt8(columnNum), rowSpan: rowSpan, columnSpan: colSpan, cellKey: String(cellKey))
				cellKey += 1;

				// determine where this cell goes
				for _ in 0..<mccell.columnSpan()
				{
					// check for replace or append
					if (rows[rowNum].count <= columnNum)
					{
						// append
						rows[rowNum].append(mccell);
					}
					else
					{
						// replace
						rows[rowNum][columnNum] = mccell;
					}

					columnNum += 1;
				}

				// is this cell an operation?
				if let id = column["operation-id"].string
				{
					// in this case the operation-type might be overriden
					let ot: OperationType?;
					
					if let overrideOperationType = column["override-operation-type"].string
					{
						ot = OperationType.mapValues[overrideOperationType]!;
					}
					else
					{
						ot = nil;
					}

					// add it to the cell for this layer
					mccell.addOperation(
						layer,
						operationId: id,
						overrideOperationType: ot
					);
				}
				// then it must be a ui function
				else
				{
					let uif: UIFunction;
					
					if let uiFunction = column["ui-function"].string, let uift = UIFunction.mapValues[uiFunction]
					{
						uif = uift;
					}
					else
					{
						uif = .placeholder;
					}

					// add it to the cell for this layer
					mccell.addUIFunction(
						layer,
						uiFunction: uif
					);
				}
			}
			
			rowNum += 1;
		}
	}
	
	static func readLayer(_ layerData: [JSON], rows: [[CalculatorCell]], layer: Layer)
	{
		var rowNum: Int = 0;
		
		for row in layerData
		{
			var columnNum: Int = 0;

			for column in row.array!
			{
				// look for a row or column coordinate
				if let rowCoordinate = column["row-coordinate"].int
				{
					rowNum = rowCoordinate;
				}

				if let colCoordinate = column["column-coordinate"].int
				{
					columnNum = colCoordinate;
				}
				
				let mccell: MutableCalculatorCell = rows[rowNum][columnNum] as! MutableCalculatorCell;
				
				// is this cell an operation?
				if let id = column["operation-id"].string
				{
					// in this case the operation-type might be overriden
					let ot: OperationType?;
					
					if let overrideOperationType = column["override-operation-type"].string
					{
						ot = OperationType.mapValues[overrideOperationType]!;
					}
					else
					{
						ot = nil;
					}
					
					// add it to the cell for this layer
					mccell.addOperation(
						layer,
						operationId: id,
						overrideOperationType: ot
					);
				}
					// then it must be a ui function
				else
				{
					let uif: UIFunction;
					
					if let uiFunction = column["ui-function"].string
					{
						uif = UIFunction.mapValues[uiFunction]!;
					}
					else
					{
						uif = .placeholder;
					}

					// add it to the cell for this layer
					mccell.addUIFunction(
						layer,
						uiFunction: uif
					);
				}

				columnNum += 1;
			}
			
			rowNum += 1;
		}
	}

	open func populateOperations(_ calculator: Calculator)
	{
		for row in _rows
		{
			for cell in row
			{
				let mccell: MutableCalculatorCell = cell as! MutableCalculatorCell;
				
				mccell.populateOperations(calculator);
			}
		}
	}

	open func cells() -> [CalculatorCell]
	{
		return _cells;
	}

	open func suggestedOrientation() -> Orientation
	{
		return _orientation;
	}
	
	open func supportedIntegerWordSizes() -> [IntegerWordSize]?
	{
		return _supportedIntegerWordSizes;
	}
	
	open func supportedNumericBehaviors() -> [NumericBehavior]?
	{
		return _supportedNumericBehaviors;
	}

	open func supportedNumericBases() -> [UInt8]?
	{
		return _supportedNumericalBases;
	}

	open func suggestedDecimals() -> UInt8?
	{
		return _suggestedDecimals;
	}

	fileprivate static func getBoolean(_ any: String?) -> Bool
	{
		if let b: String = any
		{
			return b == "true"
		}
		
		return false;
	}
	
	open func layers() -> [Layer] {
		return _layers;
	}

	open func rows() -> UInt8 {
		return UInt8(_rows.count);
	}
	
	open func columns() -> UInt8 {
		if (_rows.count == 0)
		{
			return 0;
		}
		else
		{
			return UInt8(_rows[0].count);
		}
	}
	
	open func cell(row: UInt8, column: UInt8) -> CalculatorCell
	{
		return _rows[Int(row)][Int(column)];
	}

	open func enforceConsistency(_ calculator: Calculator)
	{
		let stack = calculator.stack();
		
		// check integer word sizes
		if let s = supportedIntegerWordSizes()
		{
			if !s.contains(stack.getIntegerWordSize())
			{
				stack.setIntegerWordSize(s[0]);
			}
		}
		
		// check numeric behaviors
		if let s = supportedNumericBehaviors()
		{
			if !s.contains(stack.getNumericBehavior())
			{
				stack.setNumericBehavior(s[0]);
			}
		}
		else
		{
			// nothing specified - always go to Decimal
			stack.setNumericBehavior(.Decimal);
		}

		// potentially change the numerical base
		if let s = supportedNumericBases()
		{
			if !s.contains(stack.getNumericBase())
			{
				// change to first supported
				stack.setNumericBase(base: s[0]);
				stack.resetInputBuffer();
			}
		}
		else
		{
			// nothing specified - always go to base 10
			stack.setNumericBase(base: 10)
			stack.resetInputBuffer();
		}
		
		// check for a suggested number of decimals
		if let decs = suggestedDecimals()
		{
			stack.setDecimals(UInt8(decs));
		}
	}

	open func generateHtmlTable(_ layer: Layer, formatter: HtmlFormatter? = nil) -> String
	{
		let hformatter: HtmlFormatter;

		if let f = formatter
		{
			hformatter = f;
		}
		else
		{
			hformatter = NullHtmlFormatter();
		}

		var html = hformatter.prefix() + "<table\(hformatter.tableAttributes())>\n\t<thead>\n\t</thead>\n\t<tbody>\n";

		// draw rows and columns
		for row in 0..<rows()
		{
			var rowHtml = "\t\t<tr>\n";

			var cells = 0;

			for column in 0..<columns()
			{
				let c = cell(row: row, column: column);
				
				// check if this is the primal row and column for this cell - else skip
				if c.primalRow() == row && c.primalColumn() == column
				{
					cells += 1;
					rowHtml += "\t\t\t<td rowspan = \"\(c.rowSpan())\" colspan = \"\(c.columnSpan())\"\(hformatter.tdAttributes(c, layer: layer))>";

					// 'hide' some debug stuff in here
					rowHtml += "<!--";

					rowHtml += " cell-key: '" + String(c.cellKey()) + "'";

					rowHtml += " primal-row: '" + String(c.primalRow()) + "'";
					rowHtml += " row-span: '" + String(c.rowSpan()) + "'";
					rowHtml += " primal-column: '" + String(c.primalColumn()) + "'";
					rowHtml += " column-span: '" + String(c.columnSpan()) + "'";

					rowHtml += " profile-key: '" + String(c.profileKey(layer)) + "'";

					let cf = c.cellFunction(layer);

					rowHtml += " cell-function: '" + cf.rawValue + "'";

					if (cf == .operation)
					{
						if let op = c.operation(layer)
						{
							rowHtml += " operation-id: '" + op.id() + "'"
						}

						if let over = c.overrideOperationType(layer)
						{
							rowHtml += " override-operation-type: '" + over.rawValue + "'"
						}
					}
					else
					{
						rowHtml += " ui-function-type: '" + c.uiFunctionType(layer).rawValue + "'";
					}

					rowHtml += " -->";

					rowHtml += "\(hformatter.cellText(c, layer: layer))</td>\n";
				}
			}

			rowHtml += "\t\t</tr>\n";

			//if (cells > 0)
			//{
				html += rowHtml;
			//}
		}

		html += "\t</tbody>\n</table>" + hformatter.postfix();
		
		return html;
	}
	
	open func rowWeights() -> [UInt8] {
		return _rowWeights;
	}
	
	open func columnWeights() -> [UInt8] {
		return _columnWeights;
	}
}
