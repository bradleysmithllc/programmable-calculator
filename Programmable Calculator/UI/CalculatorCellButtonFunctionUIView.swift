//
//  CalculatorGridView.swift
//  Programmers Calculator
//
//  Created by Bradley Smith on 10/23/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import UIKit;

open class CalculatorCellButtonFunctionUIView: CalculatorCellUIView
{
	var touching: Bool = false;
	var touchingStartX: CGFloat = 0.0;
	
	fileprivate var attributedStringCache = [String: (attributedString: NSAttributedString, boundsRect: CGRect)]();
	
	fileprivate static var checkCount: Int = 0;
	
	override var cellAllowsInteractionDuringRunningOperation: Bool
	{
		get
		{
			switch calculatorCell.uiFunctionType(activeLayer)
			{
			case .display_x:
				return false;
			case .display_y:
				return false;
			case .display_z:
				return false;
			case .display_t:
				return false;
			case .normal:
				return true;
			case .settings:
				return false;
			case .function_a:
				return true;
			case .function_b:
				return true;
			case .function_c:
				return true;
			case .function_d:
				return true;
			case .placeholder:
				return false;
			}
		}
	};

	open override func draw(_ rect: CGRect)
	{
		guard let con = UIGraphicsGetCurrentContext() else {
				return;
		}
		
		con.saveGState();
		let conf = configuration;
		
		con.saveGState();
		drawCellBackground(
			con,
			cellRect: rect,
			cell: calculatorCell,
			touching: touching,
			operation: calculatorCell.operation(conf.activeLayer)
		);
		con.restoreGState();
		
		con.saveGState();
		drawCellBorder(con, cellRect: rect, cell: calculatorCell, touching: touching, operation: calculatorCell.operation(conf.activeLayer));
		con.restoreGState();
		
		drawCellText(con, cellRect: rect, cell: calculatorCell, touching: touching, operation: calculatorCell.operation(activeLayer));
		
		if (debugGraphics)
		{
			// draw bounding box for reference
			// upper-left
			con.setLineWidth(1.0);
			
			con.setStrokeColor(UIColor.red.cgColor);
			con.move(to: CGPoint(x: 10, y: 0));
			con.addLine(to: CGPoint(x: 0, y: 0));
			con.addLine(to: CGPoint(x: 0, y: 10));
			
			con.strokePath();
			
			// upper-right
			con.setStrokeColor(UIColor.blue.cgColor);
			con.move(to: CGPoint(x: rect.width, y: 10));
			con.addLine(to: CGPoint(x: rect.width, y: 0));
			con.addLine(to: CGPoint(x: rect.width - 10, y: 0));
			
			con.strokePath();
			
			// lower-right
			con.setStrokeColor(UIColor.green.cgColor);
			con.move(to: CGPoint(x: rect.width, y: rect.height - 10));
			con.addLine(to: CGPoint(x: rect.width, y: rect.height));
			con.addLine(to: CGPoint(x: rect.width - 10, y: rect.height));
			
			con.strokePath();
			
			// lower-left
			con.setStrokeColor(UIColor.black.cgColor);
			con.move(to: CGPoint(x: 10, y: rect.height));
			con.addLine(to: CGPoint(x: 0, y: rect.height));
			con.addLine(to: CGPoint(x: 0, y: rect.height - 10));
			
			con.strokePath();
			
			// draw edgelines
			// left
			con.setStrokeColor(UIColor.purple.cgColor);
			con.move(to: CGPoint(x: 5, y: 1));
			con.addLine(to: CGPoint(x: 5, y: rect.height - 1));
			
			con.strokePath();
			
			// top
			con.setStrokeColor(UIColor.orange.cgColor);
			con.move(to: CGPoint(x: 1, y: 5));
			con.addLine(to: CGPoint(x: rect.width - 1, y: 5));
			
			con.strokePath();
			
			// right
			con.setStrokeColor(UIColor.magenta.cgColor);
			con.move(to: CGPoint(x: rect.width - 5, y: 1));
			con.addLine(to: CGPoint(x: rect.width - 5, y: rect.height - 1));
			
			con.strokePath();
			
			// bottom
			con.setStrokeColor(UIColor.darkGray.cgColor);
			con.move(to: CGPoint(x: rect.width - 1, y: rect.height - 5));
			con.addLine(to: CGPoint(x: 1, y: rect.height - 5));
			
			con.strokePath();
		}
		
		con.restoreGState();
	}
	
	fileprivate func drawCellBackground(
		_ context: CGContext,
		cellRect: CGRect,
		cell: CalculatorCell,
		touching: Bool,
		operation: Operation?
		)
	{
		let layer: Layer = configuration.activeLayer;
		
		// draw the outline around the box
		let clr: UIColor;
		
		if (touching)
		{
			// special case for displays
			if cell.cellFunction(layer) == function.function
			{
				if
					cell.uiFunctionType(layer) == .display_x ||
						cell.uiFunctionType(layer) == .display_y ||
						cell.uiFunctionType(layer) == .display_z ||
						cell.uiFunctionType(layer) == .display_t
				{
					clr = activeProfile.buttonColor(buttonSelector: cell.profileKey(layer), layer: layer);
				}
				else
				{
					clr = activeProfile.buttonPressedColor(buttonSelector: cell.profileKey(layer), layer: layer);
				}
			}
			else
			{
				clr = activeProfile.buttonPressedColor(buttonSelector: cell.profileKey(layer), layer: layer);
			}
		}
		else
		{
			clr = activeProfile.buttonColor(buttonSelector: cell.profileKey(layer), layer: layer);
		}
		
		context.setFillColor(clr.cgColor);
		context.fill(cellRect);
	}
	
	fileprivate func drawCellBorder(_ context: CGContext, cellRect: CGRect, cell: CalculatorCell, touching: Bool, operation: Operation?)
	{
		let layer: Layer = activeLayer;
		
		context.setLineWidth(1.0);
		
		context.setStrokeColor(activeProfile.lineColor(
				buttonSelector: cell.profileKey(layer),
				layer: layer
				).cgColor
		);
		
		// draw the box around the key
		if (true)//!cell.extendsUp(layer))
		{
			context.move(to: CGPoint(x: cellRect.origin.x, y: cellRect.origin.y));
			context.addLine(to: CGPoint(x: cellRect.origin.x + cellRect.size.width, y: cellRect.origin.y));
		}
		
		if (true)//!cell.extendsRight(layer))
		{
			context.move(to: CGPoint(x: cellRect.origin.x + cellRect.size.width, y: cellRect.origin.y));
			context.addLine(to: CGPoint(x: cellRect.origin.x + cellRect.size.width, y: cellRect.origin.y + cellRect.size.height));
		}
		
		if (true)//!cell.extendsDown(layer))
		{
			context.move(to: CGPoint(x: cellRect.origin.x + cellRect.size.width, y: cellRect.origin.y + cellRect.size.height));
			context.addLine(to: CGPoint(x: cellRect.origin.x, y: cellRect.origin.y + cellRect.size.height));
		}
		
		if (true)//!cell.extendsLeft(layer))
		{
			context.move(to: CGPoint(x: cellRect.origin.x, y: cellRect.origin.y + cellRect.size.height));
			context.addLine(to: CGPoint(x: cellRect.origin.x, y: cellRect.origin.y));
		}
		
		context.strokePath();
	}
	
	fileprivate func drawCellText(_ context: CGContext, cellRect: CGRect, cell: CalculatorCell, touching: Bool, operation: Operation?)
	{
		let layer: Layer = activeLayer;
		
		let clr: UIColor;
		
		var state = 0;
		
		if (touching)
		{
			state |= 1;
			clr = activeProfile.activeMnemonicColorPressed(buttonSelector: cell.profileKey(layer), layer: layer);
		}
		else
		{
			// check for inactive
			if let op = operation
			{
				state |= 2;
				
				if (op.validateStack())
				{
					state |= 8;
					clr = activeProfile.activeMnemonicColor(buttonSelector: cell.profileKey(layer), layer: layer);
				}
				else
				{
					state |= 16;
					clr = activeProfile.inactiveMnemonicColor(buttonSelector: cell.profileKey(layer), layer: layer);
				}
			}
			else
			{
				state |= 4;
				
				clr = activeProfile.activeMnemonicColor(buttonSelector: cell.profileKey(layer), layer: layer);
			}
		}
		
		context.setStrokeColor(clr.cgColor);
		
		// overlay the mnemonic
		let mne: String?;
		let font: UIFont;
		
		switch(cell.uiFunctionType(layer))
		{
		case .normal:
			mne = String("↩︎");
			font = activeProfile.activeMnemonicFont(buttonSelector: UIFunction.normal.rawValue, layer: layer)
			break;
		case .settings:
			mne = "i {!}";
			font = activeProfile.activeMnemonicFont(buttonSelector: UIFunction.settings.rawValue, layer: layer)
			break;
		case .function_a:
			mne = "2{nd}";
			font = activeProfile.activeMnemonicFont(buttonSelector: UIFunction.function_a.rawValue, layer: layer)
			break;
		case .function_b:
			mne = "3{rd}";
			font = activeProfile.activeMnemonicFont(buttonSelector: UIFunction.function_b.rawValue, layer: layer)
			break;
		case .function_c:
			mne = "4{th}";
			font = activeProfile.activeMnemonicFont(buttonSelector: UIFunction.function_c.rawValue, layer: layer)
			break;
		case .function_d:
			mne = "5{th}";
			font = activeProfile.activeMnemonicFont(buttonSelector: UIFunction.function_d.rawValue, layer: layer)
			break;
		case .placeholder:
			mne = "INV";
			font = activeProfile.activeMnemonicFont(buttonSelector: UIFunction.placeholder.rawValue, layer: layer)
			break;
		default:
			fatalError();
		}
		
		if let textToDraw = mne
		{
			var textRect = CalculatorCellOperationUIView.makeNormalFacetTextRect(cellRect);
			
			// reset the coordinates to 0 for each cell
			let tx = textRect.origin.x;
			let ty = textRect.origin.y;
			
			context.clip(to: textRect);
			context.translateBy(x: tx, y: ty);
			
			textRect.origin.x = 0;
			textRect.origin.y = 0;
			
			//CGContextStrokeRect(context, textRect);
			drawText(
				context, text: textToDraw, textColor: clr, targetRect: textRect, useCache: true, pressed: touching, alignment: .center, state: state, font: font
			);
		}
	}
	
	func drawText(
		_ context: CGContext, text: String, textColor: UIColor, targetRect: CGRect, useCache: Bool, pressed: Bool, alignment: AlignPosition, state: Int, font: UIFont
		)
	{
		if text.trimmingCharacters(in: CharacterSet.whitespaces) == ""
		{
			return;
		}
		
		let key: String = makeKey(text + "." + pressed.description + "." + String(state));
		
		var strContext: (attributedString: NSAttributedString, boundsRect: CGRect);
		
		let stringContext = attributedStringCache[key];
		
		if useCache && stringContext != nil
		{
			strContext = stringContext!;
		}
		else
		{
			strContext = TextDrawing.makeAttributedStringForBounds(string: text, font: font, viewRect: targetRect, textColor: textColor);
		}
		
		TextDrawing.drawTextCentered(context: context, attributedString: strContext.attributedString, viewRect: targetRect, textRect: strContext.boundsRect, alignment: alignment);
		
		if (useCache)
		{
			attributedStringCache[key] = strContext;
		}
	}
	
	func makeKey(_ text: String) -> String
	{
		return text + "." + activeProfile.id() + "." + activeLayout.id() + "." + (configuration.isLandscape() ? "Landscape" : "Portrait");
	}
	
	open override func moderatedTouchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
	{
		touching = true;
		
		if let x = touches.first?.location(in: self).x
		{
			touchingStartX = x;
		}
		
		setNeedsDisplay();
	}
	
	open override func moderatedTouchesMoved(_ touches: Set<UITouch>, with event: UIEvent?)
	{
		if let x = touches.first?.location(in: self).x
		{
			superUIView.displayOffset = max(touchingStartX - x, 0.0);
		}
		
		//superUIView.redrawDisplays();
		setNeedsDisplay();
	}
	
	open override func moderatedTouchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
	{
		touching = false;
		
		let layer = activeLayer;

		switch (calculatorCell.uiFunctionType(layer))
		{
		case .settings:
			// switch the active layer
			superUIView.settings();
			// check for a function key.  These do not get passed to the calculator engine
		case .normal:
			// switch the active layer
			configuration.activeLayer = Layer.normal;
			superUIView.redrawSubviews()
		case .function_a:
			// switch the active layer
			configuration.activeLayer = Layer.function_a;
			superUIView.redrawSubviews()
		case .function_b:
			// switch the active layer
			configuration.activeLayer = Layer.function_b;
			superUIView.redrawSubviews()
		case .function_c:
			// switch the active layer
			configuration.activeLayer = Layer.function_c;
			superUIView.redrawSubviews()
		case .function_d:
			// switch the active layer
			configuration.activeLayer = Layer.function_d;
			superUIView.redrawSubviews()
		case .placeholder:
			break;
			// ignore
		default:
			fatalError();
		}
		
		self.setNeedsDisplay();
	}
	
	open override func moderatedTouchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?)
	{
		touching = false;
		setNeedsDisplay();
	}
}
