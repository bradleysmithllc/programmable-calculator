//
//  LayoutConstants.swift
//  Programmers Calculator
//
//  Created by Bradley Smith on 11/3/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation
import UIKit

open class UILayoutConstants
{
	/**For the normal facet, I.E. the curent mnemonic, this is the
	   percentage height of the total cell height which defines the top line.
	  */
	open static let NORMAL_FACET_TOP_BAR_PERCENT = CGFloat(0.30);

	/**For the normal facet, I.E. the current mnemonic, this is the
	   Percentage height of the total cell height the label can occupy
	*/
	open static let NORMAL_FACET_BOTTOM_PERCENT = CGFloat(0.20);
	
	/**For the normal facet, I.E. the current mnemonic, this is the
		 percentage width that represents the minimum margin on the right side.
	*/
	open static let NORMAL_FACET_RIGHT_MARGIN = CGFloat(0.15);
	
	/**For the normal facet, I.E. the current mnemonic, this is the
		percentage width that represents the minimum margin on the left side.
	*/
	open static let NORMAL_FACET_LEFT_MARGIN = CGFloat(0.15);
}
