//
//  CalculatorCellUIView.swift
//  |RPN|
//
//  Created by Bradley Smith on 2/3/16.
//  Copyright © 2016 Bradley Smith, LLC. All rights reserved.
//

import Foundation
import UIKit

open class CalculatorCellUIView : UIView
{
	static let _debugGraphics = false;
	
	private var _superUIView: CalculatorUIView?;
	private var _cell: CalculatorCell?;

	private var touchingMe = false;
	
	var cellAllowsInteractionDuringRunningOperation: Bool
	{
		get
		{
			return false;
		}
	};
	
	var debugGraphics: Bool
		{
			return CalculatorCellUIView._debugGraphics;
	}
	
	var superUIView: CalculatorUIView
	{
		get
		{
			return _superUIView!;
		}
	}
	
	var calculatorCell: CalculatorCell
	{
		get
		{
			return _cell!;
		}
	}
	
	var activeLayer: Layer
		{
		get
		{
			return superUIView.configuration.activeLayer;
		}
	}
	
	var configuration: Configuration
		{
		get
		{
			return superUIView.configuration;
		}
	}
	
	var calculator: Calculator
		{
		get
		{
			return superUIView.calculator;
		}
	}
	
	var activeProfile: Profile
		{
		get
		{
			return superUIView.activeProfile;
		}
	}
	
	var activeLayout: Layout
		{
		get
		{
			return superUIView.configuration.activeLayout;
		}
	}
	
	final func setUp(_ superView: CalculatorUIView, cell: CalculatorCell)
	{
		self._superUIView = superView;
		self._cell = cell;
	}

	open static func splitDecimal(_ input: String) -> (integral: String, decimal: String)
	{
		let decimalPortion: String;
		let integralPortion: String;
		
		// remove decimal portion
		if let srange = input.range(of: ".")
		{
			// strip decimals
			decimalPortion = input.substring(from: srange.lowerBound);
			
			integralPortion = input.substring(to: srange.lowerBound);
		}
		else
		{
			decimalPortion = "";
			integralPortion = input;
		}
		
		return (integral: integralPortion, decimal: decimalPortion);
	}
	
	open static func separatize(_ input: String, digitsPerSeparator: Int) -> String
	{
		var result: String = "";
		
		for (index, character) in input.characters.reversed().enumerated()
		{
			if (character != "-" && index != 0 && index % digitsPerSeparator == 0)
			{
				result.append(Character(","));
			}
			
			result.append(character);
		}
		
		return String(result.characters.reversed());
	}
	
	static func makeNormalFacetTextRect(_ cellRect: CGRect) -> CGRect
	{
		// new origin
		let new_x = cellRect.origin.y + cellRect.size.width * UILayoutConstants.NORMAL_FACET_LEFT_MARGIN;
		let new_y = cellRect.origin.x + UILayoutConstants.NORMAL_FACET_TOP_BAR_PERCENT * cellRect.size.height;
		
		// new width.  subtract the two margins from the width
		let new_width = cellRect.size.width * (1.0 - (UILayoutConstants.NORMAL_FACET_LEFT_MARGIN + UILayoutConstants.NORMAL_FACET_RIGHT_MARGIN));
		
		// new height.  subtract the two margins from the height
		let new_height = cellRect.size.height * (1.0 - (UILayoutConstants.NORMAL_FACET_TOP_BAR_PERCENT + UILayoutConstants.NORMAL_FACET_BOTTOM_PERCENT));
		
		return CGRect(x: new_x, y: new_y, width: new_width, height: new_height).integral;
	}
	
	open static func displayNumber(_ number: NSDecimalNumber, calculator: Calculator) -> String
	{
		return CalculatorCellOperationUIView.displayNumber(number, numericBehavior: calculator.stack().getNumericBehavior(), integerWordSize: calculator.stack().getIntegerWordSize(), numericBase: calculator.stack().getNumericBase(), decimals: calculator.stack().getDecimals());
	}
	
	open static func displayNumber(_ number: NSDecimalNumber, numericBehavior: NumericBehavior, integerWordSize: IntegerWordSize?, numericBase: UInt8, decimals: UInt8) -> String
	{
		let disp: NSDecimalNumber;
		
		if (numericBase == 10)
		{
			// round to required decimals
			disp = number.decimalNumberRoundedToDigits(decimals);
		}
		else
		{
			disp = number;
		}

		return prepareNumber(disp.formattedNumberInBase(numericBase, unsigned: numericBehavior == .UnsignedInteger), numericBase: numericBase);
	}

	open static func prepareNumber(_ formatted: String, numericBase: UInt8) -> String
	{
		// add commas
		var digitsToSeparate: Int;

		if numericBase == 2
		{
			digitsToSeparate = 8;
		}
		else if numericBase == 8
		{
			digitsToSeparate = 4;
		}
		else if numericBase == 16
		{
			digitsToSeparate = 4;
		}
		else
		{
			digitsToSeparate = 3;
		}
		
		let splitString = CalculatorCellOperationUIView.splitDecimal(formatted);
		
		let sepString = CalculatorCellOperationUIView.separatize(splitString.integral, digitsPerSeparator: digitsToSeparate) + splitString.decimal;
		
		return sepString;
	}

	open func moderatedTouchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {}
	
	final override public func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
	{
		if (!superUIView.runningOperation || cellAllowsInteractionDuringRunningOperation)
		{
			touchingMe = true;
			moderatedTouchesBegan(touches, with: event);
		}
	}
	
	open func moderatedTouchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {}
	
	final override public func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?)
	{
		if (!superUIView.runningOperation || cellAllowsInteractionDuringRunningOperation)
		{
			moderatedTouchesMoved(touches, with: event);
		}
	}
	
	open func moderatedTouchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {}
	
	final override public func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
	{
		if (!superUIView.runningOperation || cellAllowsInteractionDuringRunningOperation)
		{
			touchingMe = false;
			moderatedTouchesEnded(touches, with: event);
		}
	}
	
	open func moderatedTouchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {}
	
	final override public func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?)
	{
		if (!superUIView.runningOperation || cellAllowsInteractionDuringRunningOperation)
		{
			touchingMe = false;
			moderatedTouchesCancelled(touches, with: event);
		}
	}
}
