//
//  ButtonBackgroundLayer.swift
//  Programmers Calculator
//
//  Created by Bradley Smith on 11/2/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation
import QuartzCore

open class ButtonBackgroundDelegate
{
	open func displayLayer(_ layer: CALayer)
	{
		print("yes");
	}
	
	open func drawLayer(_ layer: CALayer, inContext ctx: CGContext)
	{
		print("yes");
		// calculate the size of each cell
		/*
		let perspective: Perspective = configuration.activePerspective();
		
		let cellRect: CGRect = CalculatorGridView.calculateCellSize(viewRect: bounds, rows: perspective.rows(), columns: perspective.columns());
		
		// iterate over all rows and columns
		for row: Int in 0...perspective.rows() - 1
		{
			for column: Int in 0...perspective.columns() - 1
			{
				// move the cell origin to it's new location
				var thisCellRect: CGRect = CalculatorGridView.calculateCellRect(cellRect: cellRect, row: row, column: column);
				
				CGContextSaveGState(con);
				//CGContextClipToRect(con, thisCellRect);
				
				// reset the coordinates to 0 for each cell
				let tx = thisCellRect.origin.x;
				let ty = thisCellRect.origin.y;
				
				CGContextTranslateCTM(con, tx, ty);
				
				thisCellRect.origin.x = 0.0;
				thisCellRect.origin.y = 0.0;
				
				drawCell(con!, cellRect: thisCellRect, cell: perspective.cell(row: row, column: column), row: row, column: column);
				CGContextRestoreGState(con);
			}
		}
	}

	private func drawCell(context: CGContext, cellRect: CGRect, cell: CalculatorCell, row: Int, column: Int)
	{
		let layer: Layer = activeLayer();
		
		// draw the outline around the box
		let clr: UIColor;
		
		if (touchingRow == row && touchingCol == column)
		{
			clr = configuration.activeProfile.buttonPressedColor(buttonType: cell.buttonType(layer), layer: layer);
		}
		else
		{
			clr = configuration.activeProfile.buttonColor(buttonType: cell.buttonType(layer), layer: layer);
		}
		
		CGContextSetFillColorWithColor(context, clr.CGColor);
		CGContextFillRect(context, cellRect);
		CGContextSetStrokeColorWithColor(context, configuration.activeProfile.lineColor(buttonType: cell.buttonType(layer), layer: layer).CGColor);
		
		CGContextSetLineWidth(context, 0.25);
		
		CGContextSetStrokeColorWithColor(
			context,
			configuration.activeProfile.lineColor(
				buttonType: cell.buttonType(layer),
				layer: layer
				).CGColor
		);
		
		// draw the box around the key
		if (!cell.extendsUp(layer))
		{
			CGContextMoveToPoint(context, cellRect.origin.x, cellRect.origin.y);
			CGContextAddLineToPoint(context, cellRect.origin.x + cellRect.size.width, cellRect.origin.y);
		}
		
		if (!cell.extendsRight(layer))
		{
			CGContextMoveToPoint(context, cellRect.origin.x + cellRect.size.width, cellRect.origin.y);
			CGContextAddLineToPoint(context, cellRect.origin.x + cellRect.size.width, cellRect.origin.y + cellRect.size.height);
		}
		
		if (!cell.extendsDown(layer))
		{
			CGContextMoveToPoint(context, cellRect.origin.x + cellRect.size.width, cellRect.origin.y + cellRect.size.height);
			CGContextAddLineToPoint(context, cellRect.origin.x, cellRect.origin.y + cellRect.size.height);
		}
		
		if (!cell.extendsLeft(layer))
		{
			CGContextMoveToPoint(context, cellRect.origin.x, cellRect.origin.y + cellRect.size.height);
			CGContextAddLineToPoint(context, cellRect.origin.x, cellRect.origin.y);
		}
		
		CGContextStrokePath(context);
		
		// overlay the mnemonic
		let attributes: [String: AnyObject] = [
			NSForegroundColorAttributeName: configuration.activeProfile.mnemonicColor(buttonType: cell.buttonType(layer), layer: layer)
		]
		
		let mne: String = cell.mnemonic(layer);
		
		CGContextSetTextMatrix(context, CGAffineTransformIdentity);
		CGContextTranslateCTM(context, 0, cellRect.size.height);
		CGContextScaleCTM(context, 1.0, -1.0);
		
		let path: CGMutablePathRef = CGPathCreateMutable(); //1
		CGPathAddRect(path, nil, cellRect);
		
		let attString: NSAttributedString = NSAttributedString(string: mne);
		
		let framesetter: CTFramesetterRef =	CTFramesetterCreateWithAttributedString(attString); //3
		let frame: CTFrameRef = CTFramesetterCreateFrame(framesetter,
			
			CFRangeMake(0, attString.length), path, nil);
		
		CGContextSetStrokeColorWithColor(
			context,
			configuration.activeProfile.mnemonicColor(
				buttonType: cell.buttonType(layer),
				layer: layer
				).CGColor
		);
		
		let options: NSStringDrawingOptions = [.UsesLineFragmentOrigin, .UsesFontLeading];
		
		let ri: CGRect = CGRectIntegral(attString.boundingRectWithSize(
			CGSizeMake(300.0, CGFloat.max),
			options: options,
			context: nil
			));
		
		print("Boundong rect \(ri)");
		
		CTFrameDraw(frame, context); //4
		*/
	}
}
