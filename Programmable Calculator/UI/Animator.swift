//
//  Animator.swift
//  |RPN|
//
//  Created by Bradley Smith on 2/2/16.
//  Copyright © 2016 Bradley Smith, LLC. All rights reserved.
//

import Foundation
import UIKit;

open class Animator
{
	fileprivate var currentPoint: Int;
	fileprivate let endPoint: Int;
	fileprivate let increment: Int;

	var point: CGFloat
	{
		get
		{
			return CGFloat(currentPoint);
		}
	}
	
	var done: Bool
	{
		get
		{
			return currentPoint == endPoint;
		}
	}

	init(startingPoint: Int, endPoint: Int, increment: Int)
	{
		self.currentPoint = startingPoint;
		self.endPoint = endPoint;
		self.increment = increment;
	}

	func step()
	{
		currentPoint += increment;
	}
}
