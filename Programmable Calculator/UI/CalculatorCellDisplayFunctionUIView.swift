//
//  CalculatorGridView.swift
//  Programmers Calculator
//
//  Created by Bradley Smith on 10/23/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import UIKit;

open class CalculatorCellDisplayFunctionUIView: CalculatorCellUIView
{
	var touching: Bool = false;
	var touchingStartX: CGFloat = 0.0;
	var mathHub: MathHub = MathHub.mathHub();
	
	fileprivate var attributedStringCache = [String: (attributedString: NSAttributedString, boundsRect: CGRect)]();
	
	fileprivate static var checkCount: Int = 0;
	
	open override func draw(_ rect: CGRect)
	{
		guard let con = UIGraphicsGetCurrentContext() else
		{
			return;
		}
		
		con.saveGState();
		let conf = configuration;

		con.saveGState();
		drawCellBackground(
			con,
			cellRect: rect,
			cell: calculatorCell,
			touching: touching,
			operation: calculatorCell.operation(conf.activeLayer)
		);
		con.restoreGState();
		
		con.saveGState();
		drawCellBorder(con, cellRect: rect, cell: calculatorCell, touching: touching, operation: calculatorCell.operation(conf.activeLayer));
		con.restoreGState();
		
		if calculatorCell.uiFunctionType(activeLayer) == .display_x
		{
			drawDisplayX(con, cellRect: rect);
		}
		else if calculatorCell.uiFunctionType(activeLayer) == .display_y
		{
			// somehing to do?
			if (calculator.stack().size() > 1)
			{
				drawDisplayN(con, cellRect: rect, element: calculator.stack().getYElement());
			}
		}
		else if calculatorCell.uiFunctionType(activeLayer) == .display_z
		{
			// somehing to do?
			if (calculator.stack().size() > 2)
			{
				drawDisplayN(con, cellRect: rect, element: calculator.stack().getZElement());
			}
		}
		else if calculatorCell.uiFunctionType(activeLayer) == .display_t
		{
			// somehing to do?
			if (calculator.stack().size() > 3)
			{
				drawDisplayN(con, cellRect: rect, element: calculator.stack().getTElement());
			}
		}
		else
		{
			drawCellText(con, cellRect: rect, cell: calculatorCell, touching: touching, operation: calculatorCell.operation(activeLayer));
		}
		
		if (debugGraphics)
		{
			// draw bounding box for reference
			// upper-left
			con.setLineWidth(1.0);
			
			con.setStrokeColor(UIColor.red.cgColor);
			con.move(to: CGPoint(x: 10, y: 0));
			con.addLine(to: CGPoint(x: 0, y: 0));
			con.addLine(to: CGPoint(x: 0, y: 10));
			
			con.strokePath();
			
			// upper-right
			con.setStrokeColor(UIColor.blue.cgColor);
			con.move(to: CGPoint(x: rect.width, y: 10));
			con.addLine(to: CGPoint(x: rect.width, y: 0));
			con.addLine(to: CGPoint(x: rect.width - 10, y: 0));
			
			con.strokePath();
			
			// lower-right
			con.setStrokeColor(UIColor.green.cgColor);
			con.move(to: CGPoint(x: rect.width, y: rect.height - 10));
			con.addLine(to: CGPoint(x: rect.width, y: rect.height));
			con.addLine(to: CGPoint(x: rect.width - 10, y: rect.height));
			
			con.strokePath();
			
			// lower-left
			con.setStrokeColor(UIColor.black.cgColor);
			con.move(to: CGPoint(x: 10, y: rect.height));
			con.addLine(to: CGPoint(x: 0, y: rect.height));
			con.addLine(to: CGPoint(x: 0, y: rect.height - 10));
			
			con.strokePath();
			
			// draw edgelines
			// left
			con.setStrokeColor(UIColor.purple.cgColor);
			con.move(to: CGPoint(x: 5, y: 1));
			con.addLine(to: CGPoint(x: 5, y: rect.height - 1));
			
			con.strokePath();
			
			// top
			con.setStrokeColor(UIColor.orange.cgColor);
			con.move(to: CGPoint(x: 1, y: 5));
			con.addLine(to: CGPoint(x: rect.width - 1, y: 5));
			
			con.strokePath();
			
			// right
			con.setStrokeColor(UIColor.magenta.cgColor);
			con.move(to: CGPoint(x: rect.width - 5, y: 1));
			con.addLine(to: CGPoint(x: rect.width - 5, y: rect.height - 1));
			
			con.strokePath();
			
			// bottom
			con.setStrokeColor(UIColor.darkGray.cgColor);
			con.move(to: CGPoint(x: rect.width - 1, y: rect.height - 5));
			con.addLine(to: CGPoint(x: 1, y: rect.height - 5));
			
			con.strokePath();
		}
		
		con.restoreGState();
	}
	
	fileprivate func drawCellBackground(
		_ context: CGContext,
		cellRect: CGRect,
		cell: CalculatorCell,
		touching: Bool,
		operation: Operation?
		)
	{
		let layer: Layer = configuration.activeLayer;
		
		// draw the outline around the box
		let clr: UIColor;
		
		if (touching)
		{
			// special case for displays
			if cell.cellFunction(layer) == function.function
			{
				if
					cell.uiFunctionType(layer) == .display_x ||
						cell.uiFunctionType(layer) == .display_y ||
						cell.uiFunctionType(layer) == .display_z ||
						cell.uiFunctionType(layer) == .display_t
				{
					clr = activeProfile.buttonColor(buttonSelector: cell.profileKey(layer), layer: layer);
				}
				else
				{
					clr = activeProfile.buttonPressedColor(buttonSelector: cell.profileKey(layer), layer: layer);
				}
			}
			else
			{
				clr = activeProfile.buttonPressedColor(buttonSelector: cell.profileKey(layer), layer: layer);
			}
		}
		else
		{
			clr = activeProfile.buttonColor(buttonSelector: cell.profileKey(layer), layer: layer);
		}
		
		context.setFillColor(clr.cgColor);
		context.fill(cellRect);
	}
	
	fileprivate func drawCellBorder(_ context: CGContext, cellRect: CGRect, cell: CalculatorCell, touching: Bool, operation: Operation?)
	{
		let layer: Layer = activeLayer;
		
		context.setLineWidth(1.0);
		
		context.setStrokeColor(activeProfile.lineColor(
				buttonSelector: cell.profileKey(layer),
				layer: layer
				).cgColor
		);
		
		// draw the box around the key
		if (true)//!cell.extendsUp(layer))
		{
			context.move(to: CGPoint(x: cellRect.origin.x, y: cellRect.origin.y));
			context.addLine(to: CGPoint(x: cellRect.origin.x + cellRect.size.width, y: cellRect.origin.y));
		}
		
		if (true)//!cell.extendsRight(layer))
		{
			context.move(to: CGPoint(x: cellRect.origin.x + cellRect.size.width, y: cellRect.origin.y));
			context.addLine(to: CGPoint(x: cellRect.origin.x + cellRect.size.width, y: cellRect.origin.y + cellRect.size.height));
		}
		
		if (true)//!cell.extendsDown(layer))
		{
			context.move(to: CGPoint(x: cellRect.origin.x + cellRect.size.width, y: cellRect.origin.y + cellRect.size.height));
			context.addLine(to: CGPoint(x: cellRect.origin.x, y: cellRect.origin.y + cellRect.size.height));
		}
		
		if (true)//!cell.extendsLeft(layer))
		{
			context.move(to: CGPoint(x: cellRect.origin.x, y: cellRect.origin.y + cellRect.size.height));
			context.addLine(to: CGPoint(x: cellRect.origin.x, y: cellRect.origin.y));
		}
		
		context.strokePath();
	}
	
	fileprivate func drawDisplayX(_ context: CGContext, cellRect: CGRect)
	{
		var clr: UIColor = activeProfile.activeMnemonicColor(buttonSelector: UIFunction.display_x.rawValue, layer: activeLayer);
		
		let lastMessage: (msg: String, failure: Bool)! = mathHub.lastMessage();
		
		if (lastMessage != nil && lastMessage.failure)
		{
			clr = UIColor.brown;
		}
		
		context.setStrokeColor(clr.cgColor);
		
		// overlay the mnemonic
		let mne: String = CalculatorCellDisplayFunctionUIView.getDisplayXText(calculator);
		
		var textRect = cellRect;
			
		// reset the coordinates to 0 for each cell
		let tx = textRect.origin.x - superUIView.displayOffset;
		let ty = textRect.origin.y;
			
		context.clip(to: textRect);
		context.translateBy(x: tx, y: ty);
			
		textRect.origin.x = 0;
		textRect.origin.y = 0;
			
		drawText(context, text: mne, textColor: clr, targetRect: textRect, useCache: false, pressed: false, alignment: .right, state: 0,
			font: activeProfile.activeMnemonicFont(buttonSelector: UIFunction.display_x.rawValue, layer: activeLayer)
		);
	}
	
	fileprivate func drawDisplayN(_ context: CGContext, cellRect: CGRect, element: StackElement)
	{
		let clr: UIColor = activeProfile.activeMnemonicColor(buttonSelector: UIFunction.display_y.rawValue, layer: activeLayer);
		
		context.setStrokeColor(clr.cgColor);
		
		let textToDraw = element.isMessage ? element.message() : displayNumber(element.number());
		
		var textRect = cellRect;
		
		// reset the coordinates to 0 for each cell
		let tx = textRect.origin.x - superUIView.displayOffset;
		let ty = textRect.origin.y;
		
		context.clip(to: textRect);
		context.translateBy(x: tx, y: ty);
		
		textRect.origin.x = 0;
		textRect.origin.y = 0;
		
		drawText(context, text: textToDraw, textColor: clr, targetRect: textRect, useCache: false, pressed: false, alignment: .right, state: 0,
			font: activeProfile.activeMnemonicFont(buttonSelector: UIFunction.display_y.rawValue, layer: activeLayer)
		);
	}
	
	fileprivate func drawCellText(_ context: CGContext, cellRect: CGRect, cell: CalculatorCell, touching: Bool, operation: Operation?)
	{
		let layer: Layer = activeLayer;
		
		let clr: UIColor;
		
		var state = 0;
		
		if (touching)
		{
			state |= 1;
			clr = activeProfile.activeMnemonicColorPressed(buttonSelector: cell.profileKey(layer), layer: layer);
		}
		else
		{
			// check for inactive
			if let op = operation
			{
				state |= 2;
				
				if (op.validateStack())
				{
					state |= 8;
					clr = activeProfile.activeMnemonicColor(buttonSelector: cell.profileKey(layer), layer: layer);
				}
				else
				{
					state |= 16;
					clr = activeProfile.inactiveMnemonicColor(buttonSelector: cell.profileKey(layer), layer: layer);
				}
			}
			else
			{
				state |= 4;
				
				clr = activeProfile.activeMnemonicColor(buttonSelector: cell.profileKey(layer), layer: layer);
			}
		}
		
		context.setStrokeColor(clr.cgColor);
		
		// overlay the mnemonic
		let mne: String?;
		let font: UIFont;
		
		switch(cell.uiFunctionType(layer))
		{
		case .display_x:
			let inNumericEntry = calculator.stack().inNumericEntry();
			
			if inNumericEntry
			{
				mne =
					(calculator.stack().getNumericEntrySignBit())
					?
						("-" + calculator.stack().printableInputImage())
					:
					(calculator.stack().printableInputImage());
			}
			else
			{
				mne = calculator.stack().getX().formattedNumberInBase(calculator.stack().getNumericBase(), unsigned: false);
			}
			
			font = activeProfile.activeMnemonicFont(buttonSelector: UIFunction.display_x.rawValue, layer: layer)
			break;
		case .display_y:
			mne = calculator.stack().getY().formattedNumberInBase(calculator.stack().getNumericBase(), unsigned: false);
			font = activeProfile.activeMnemonicFont(buttonSelector: UIFunction.display_y.rawValue, layer: layer)
			break;
		case .display_z:
			mne = calculator.stack().getZ().formattedNumberInBase(calculator.stack().getNumericBase(), unsigned: false);
			font = activeProfile.activeMnemonicFont(buttonSelector: UIFunction.display_z.rawValue, layer: layer)
			break;
		case .display_t:
			mne = calculator.stack().getT().formattedNumberInBase(calculator.stack().getNumericBase(), unsigned: false);
			font = activeProfile.activeMnemonicFont(buttonSelector: UIFunction.display_t.rawValue, layer: layer)
			break;
		case .normal:
			mne = String("↩︎");
			font = activeProfile.activeMnemonicFont(buttonSelector: UIFunction.normal.rawValue, layer: layer)
			break;
		default:
			fatalError();
		}
		
		if let textToDraw = mne
		{
			var textRect = CalculatorCellOperationUIView.makeNormalFacetTextRect(cellRect);
			
			// reset the coordinates to 0 for each cell
			let tx = textRect.origin.x;
			let ty = textRect.origin.y;
			
			context.clip(to: textRect);
			context.translateBy(x: tx, y: ty);
			
			textRect.origin.x = 0;
			textRect.origin.y = 0;
			
			//CGContextStrokeRect(context, textRect);
			drawText(
				context, text: textToDraw, textColor: clr, targetRect: textRect, useCache: true, pressed: touching, alignment: .center, state: state, font: font
			);
		}
	}
	
	open static func getDisplayXText(_ calculator: Calculator) -> String
	{
		let stack = calculator.stack();

		if (stack.inNumericEntry())
		{
			let image = stack.currentInputImage();

			if (image == "")
			{
				return "0";
			}
			// check for only '.' selected
			else if (image == ".")
			{
				return "0.";
			}

			let prepared = prepareNumber(image, numericBase: calculator.stack().getNumericBase())
			
			if (stack.getNumericEntrySignBit())
			{
				return "-" + prepared;
			}

			return prepared;
		}
		else
		{
			// grab X-display
			let sxe = calculator.stack().getXElement();
			let mne = sxe.isMessage ? sxe.message() : displayNumber(sxe.number(), calculator: calculator);
			
			return mne;
		}
	}

	func displayNumber(_ number: NSDecimalNumber) -> String
	{
		return CalculatorCellDisplayFunctionUIView.displayNumber(number, calculator: calculator);
	}
	
	func drawText(
		_ context: CGContext, text: String, textColor: UIColor, targetRect: CGRect, useCache: Bool, pressed: Bool, alignment: AlignPosition, state: Int, font: UIFont
		)
	{
		if text.trimmingCharacters(in: CharacterSet.whitespaces) == ""
		{
			return;
		}
		
		let key: String = makeKey(text + "." + pressed.description + "." + String(state));
		
		var strContext: (attributedString: NSAttributedString, boundsRect: CGRect);
		
		let stringContext = attributedStringCache[key];
		
		if useCache && stringContext != nil
		{
			strContext = stringContext!;
		}
		else
		{
			strContext = TextDrawing.makeAttributedStringForBounds(string: text, font: font, viewRect: targetRect, textColor: textColor);
		}
		
		TextDrawing.drawTextCentered(context: context, attributedString: strContext.attributedString, viewRect: targetRect, textRect: strContext.boundsRect, alignment: alignment);
		
		if (useCache)
		{
			attributedStringCache[key] = strContext;
		}
	}
	
	func makeKey(_ text: String) -> String
	{
		return text + "." + activeProfile.id() + "." + activeLayout.id() + "." + (configuration.isLandscape() ? "Landscape" : "Portrait");
	}
	
	open override func moderatedTouchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
	{
		touching = true;
		
		if let x = touches.first?.location(in: self).x
		{
			touchingStartX = x;
		}
		
		setNeedsDisplay();
	}
	
	open override func moderatedTouchesMoved(_ touches: Set<UITouch>, with event: UIEvent?)
	{
		if let x = touches.first?.location(in: self).x
		{
			superUIView.displayOffset = max(touchingStartX - x, 0.0);
		}
		
		setNeedsDisplay();
		//superUIView.redrawDisplays();
	}
	
	open override func moderatedTouchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
	{
		let moved: Bool;
		
		if let x = touches.first?.location(in: self).x
		{
			if (touchingStartX - x) > (bounds.width / 2.0)
			{
				moved = true;
			}
			else
			{
				moved = false;
			}
		}
		else
		{
			moved = false;
		}
		
		superUIView.displayOffset = 0.0;
		touching = false;
		
		let layer = activeLayer;
		
		switch (calculatorCell.uiFunctionType(layer))
		{
			// if there are two touches on a display cell, then this is a history rewind
		case .display_x:
			if moved
			{
				// doesn't matter where . . .
				calculator.stack().revertToCheckPoint();
				// animate
				superUIView.animate(100, endPoint: Int(bounds.width));
				superUIView.redrawSubviews()
			}
			superUIView.redrawSubviews();
		case .display_y:
			if moved
			{
				// doesn't matter where . . .
				calculator.stack().revertToCheckPoint();
				superUIView.animate(100, endPoint: Int(bounds.width));
				superUIView.redrawSubviews()
			}
			superUIView.redrawSubviews();
		case .display_z:
			if moved
			{
				// doesn't matter where . . .
				calculator.stack().revertToCheckPoint();
				superUIView.animate(100, endPoint: Int(bounds.width));
				superUIView.redrawSubviews()
			}
			superUIView.redrawSubviews();
		case .display_t:
			if moved
			{
				// doesn't matter where . . .
				calculator.stack().revertToCheckPoint();
				superUIView.animate(100, endPoint: Int(bounds.width));
				superUIView.redrawSubviews()
			}
			superUIView.redrawSubviews();
		default:
			fatalError();
		}
		
		self.setNeedsDisplay();
	}
	
	open override func moderatedTouchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?)
	{
		touching = false;
		setNeedsDisplay();
	}
}
