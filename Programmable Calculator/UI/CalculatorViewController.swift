//
//  ViewController.swift
//  Programmers Calculator
//
//  Created by Bradley Smith on 10/21/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import UIKit

class CalculatorViewController: UIViewController {
	var calculatorGridView: CalculatorUIView
	{
		get
		{
			return view as! CalculatorUIView;
		}
	}

	fileprivate var landscapeMode = false;

	override func loadView() {
		super.loadView();

		calculatorGridView.viewController = self;
		calculatorGridView.backgroundColor = UIColor.black;
	}

	func settings(_ sender: UIView!)
	{
		performSegue(withIdentifier: "Settings", sender: sender);
	}

	/*
	internal override func viewWillLayoutSubviews() {
		super.viewWillLayoutSubviews();

		let viewBounds = view.bounds;

		// set grid view to use entire screen
		let calcRect = CGRectMake(
			viewBounds.origin.x,
			viewBounds.origin.y,
			viewBounds.width,
			viewBounds.height
		);

		calculatorGridView!.bounds = calcRect;
		calculatorGridView!.frame = calculatorGridView!.bounds;

		calculatorGridView!.backgroundColor = UIColor.grayColor();

		//calculatorGridView!.setNeedsDisplay();
		
		//calculatorGridView!.updateInternalView();
		//view.setNeedsDisplay();
	}
	*/

	@IBAction func unwindFromSettings(_ segue: UIStoryboardSegue) {
		calculatorGridView.setNeedsLayout();
	}
}
