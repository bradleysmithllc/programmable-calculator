//
//  DefaultPerspective.swift
//  Programmers Calculator
//
//  Created by Bradley Smith on 10/26/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

open class JSONLayout: BaseDescribed, Layout
{
	fileprivate var _rows: [[CalculatorCell]] = [[CalculatorCell]]();
	fileprivate var _cells: [CalculatorCell] = [CalculatorCell]();

	fileprivate let _supportedNumericalBases: [UInt8]?;
	fileprivate let _supportedNumericBehaviors: [NumericBehavior]?;
	fileprivate let _supportedIntegerWordSizes: [IntegerWordSize]?;

	fileprivate let _orientation: Orientation;
	fileprivate let _suggestedDecimals: UInt8?;

	fileprivate var _layers: [Layer] = [Layer]();

	public init(id: String)
	{
		_supportedNumericalBases = nil;
		_orientation = Orientation.landscape;
		_suggestedDecimals = nil;

		_supportedNumericBehaviors = nil;
		_supportedIntegerWordSizes = nil;

		super.init(id: id, description: id, name: id);
	}

	public init(id: String, jsonRoot: JSON)
	{
		let or = jsonRoot["orientation"].string;

		_orientation = Orientation.mapValues[or!]!;

		if let suggestedDecimals = jsonRoot["suggestedDecimals"].int
		{
			_suggestedDecimals = UInt8(suggestedDecimals);
		}
		else
		{
			_suggestedDecimals = nil;
		}

		let description = jsonRoot["description"].string!;
		let name = jsonRoot["name"].string!;
		var supportedNumericalBases: [UInt8] = [UInt8]();
		
		// read the default numeric base
		if let bases = jsonRoot["supportedNumericBases"].array
		{
			for base in bases
			{
				supportedNumericalBases.append(UInt8(base.intValue));
			}
		}
		
		if (supportedNumericalBases.count == 0)
		{
			_supportedNumericalBases = nil;
		}
		else
		{
			_supportedNumericalBases = supportedNumericalBases;
		}

		var supportedNumericBehaviors: [NumericBehavior] = [NumericBehavior]();
		
		if let behavs = jsonRoot["supportedNumericBehaviors"].array
		{
			for behav in behavs
			{
				supportedNumericBehaviors.append(NumericBehavior.mapValues[behav.string!]!);
			}
		}
		
		if (supportedNumericBehaviors.count == 0)
		{
			_supportedNumericBehaviors = nil;
		}
		else
		{
			_supportedNumericBehaviors = supportedNumericBehaviors;
		}
		
		var supportedIntegerWordSizes: [IntegerWordSize] = [IntegerWordSize]()
		
		if let wrdszs = jsonRoot["supportedIntegerWordSizes"].array
		{
			for wrdsz in wrdszs
			{
				supportedIntegerWordSizes.append(IntegerWordSize.mapValues[wrdsz.string!]!);
			}
		}
		
		if (supportedIntegerWordSizes.count == 0)
		{
			_supportedIntegerWordSizes = nil;
		}
		else
		{
			_supportedIntegerWordSizes = supportedIntegerWordSizes;
		}

		super.init(id: id, description: description, name: name);

		let layouts: JSON! = jsonRoot["layouts"];
		
		// the top-level is layer-names
		for layerName: Layer in Layer.allValues
		{
			// not each layer need be present
			let layer = layouts[layerName.rawValue];

			if (layer != JSON.null)
			{
				_layers.append(layerName);

				// this is another dictionary with one entry: rows - array
				let rows = layer["rows"];
				
				var rowNum: Int = 0;
				
				for row in rows.array!
				{
					if (_rows.count <= rowNum)
					{
						_rows.append([MutableCalculatorCell]());
					}
					
					// get the corresponding data row
					var columnNum: Int = 0;
					
					for column in row.array!
					{
						if (_rows[rowNum].count <= columnNum)
						{
							let mcell = MutableCalculatorCell(layout: self, primalRow: UInt8(rowNum), primalColumn: UInt8(columnNum));
							_rows[rowNum].append(
								mcell
							);
							_cells.append(mcell);
						}
						
						let extendsRight: Bool = column["extendsRight"].boolValue;
						let extendsDown: Bool = column["extendsDown"].boolValue;

						(_rows[rowNum][columnNum] as! MutableCalculatorCell).extends(right: extendsRight, down: extendsDown);

						// is this cell an operation?
						if let id = column["operation-id"].string
						{
							// in this case the operation-type might be overriden
							let ot: OperationType?;

							if let overrideOperationType = column["override-operation-type"].string
							{
								ot = OperationType.mapValues[overrideOperationType]!;
							}
							else
							{
								ot = nil;
							}

							(_rows[rowNum][columnNum] as! MutableCalculatorCell).addOperation(
								layerName,
								operationId: id,
								overrideOperationType: ot
							);
						}
						// then it must be a ui function
						else
						{
							let uif: UIFunction;

							if let uiFunction = column["ui-function"].string
							{
								uif = UIFunction.mapValues[uiFunction]!;
							}
							else
							{
								uif = .placeholder;
							}

							(_rows[rowNum][columnNum] as! MutableCalculatorCell).addUIFunction(
								layerName,
								uiFunction: uif
							);
						}
						
						columnNum += 1;
					}
					
					rowNum += 1;
				}
			}
		}
	}

	open func populateOperations(_ calculator: Calculator)
	{
		for row in _rows
		{
			for cell in row
			{
				let mccell = cell as! MutableCalculatorCell;
				mccell.populateOperations(calculator);
			}
		}
	}

	open func suggestedOrientation() -> Orientation
	{
		return _orientation;
	}
	
	open func supportedIntegerWordSizes() -> [IntegerWordSize]?
	{
		return _supportedIntegerWordSizes;
	}
	
	open func supportedNumericBehaviors() -> [NumericBehavior]?
	{
		return _supportedNumericBehaviors;
	}

	open func supportedNumericBases() -> [UInt8]?
	{
		return _supportedNumericalBases;
	}

	open func suggestedDecimals() -> UInt8?
	{
		return _suggestedDecimals;
	}

	fileprivate static func getBoolean(_ any: String?) -> Bool
	{
		if let b: String = any
		{
			return b == "true"
		}
		
		return false;
	}
	
	open func layers() -> [Layer] {
		return _layers;
	}

	open func rows() -> UInt8 {
		return UInt8(_rows.count);
	}
	
	open func columns() -> UInt8 {
		return UInt8(_rows[0].count);
	}
	
	open func cell(row: UInt8, column: UInt8) -> CalculatorCell
	{
		return _rows[Int(row)][Int(column)];
	}

	open func cells() -> [CalculatorCell]
	{
		return _cells;
	}

	open func enforceConsistency(_ calculator: Calculator)
	{
		let stack = calculator.stack();
		
		// check integer word sizes
		if let s = supportedIntegerWordSizes()
		{
			if !s.contains(stack.getIntegerWordSize())
			{
				stack.setIntegerWordSize(s[0]);
			}
		}
		
		// check numeric behaviors
		if let s = supportedNumericBehaviors()
		{
			if !s.contains(stack.getNumericBehavior())
			{
				stack.setNumericBehavior(s[0]);
			}
		}
		
		// potentially change the numerical base
		if let s = supportedNumericBases()
		{
			if !s.contains(stack.getNumericBase())
			{
				// change to first supported
				stack.setNumericBase(base: s[0]);
				stack.resetInputBuffer();
			}
		}
		
		// check for a suggested number of decimals
		if let decs = suggestedDecimals()
		{
			calculator.stack().setDecimals(UInt8(decs));
		}
	}
	
	open func generateHtmlTable(_ layer: Layer, formatter: HtmlFormatter? = nil) -> String
	{
		return "<table />";
	}
	
	open func rowWeights() -> [UInt8] {
		fatalError();
	}
	
	open func columnWeights() -> [UInt8] {
		fatalError();
	}
}
