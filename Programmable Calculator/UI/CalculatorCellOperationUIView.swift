//
//  CalculatorGridView.swift
//  Programmers Calculator
//
//  Created by Bradley Smith on 10/23/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import UIKit;


open class CalculatorCellOperationUIView: CalculatorCellUIView
{
	private static let operationQueue = DispatchQueue(label: "OperationQueue");
	
	open var operation: Operation?
	{
		get
		{
			return calculatorCell.operation(activeLayer);
		}
	}

	var touchingMe: Bool
	{
		if let op = operation
		{
			return superUIView.touchingMe(cellUIView: op.id())
		}
		
		return false;
	}
	
	open var needsRefresh: Bool
	{
		get
		{
			if let op = operation, let lm = lastMnemonic, let lvs = lastViewState
			{
				if op.mnemonic() == lm && lvs == op.validateStack()
				{
					return false;
				}
			}

			return true;
		}
	}

	var lastMnemonic: String? = nil;
	var lastViewState: Bool? = nil;

	fileprivate var attributedStringCache = [String: (attributedString: NSAttributedString, boundsRect: CGRect)]();
	
	fileprivate static var checkCount: Int = 0;
	
	open override func draw(_ rect: CGRect)
	{
		guard let con = UIGraphicsGetCurrentContext() else
		{
			return;
		}

		lastViewState = operation?.validateStack();

		con.saveGState();
		let conf = configuration;
		
		con.saveGState();
		drawCellBackground(
			con,
			cellRect: rect,
			cell: calculatorCell,
			touching: touchingMe,
			operation: calculatorCell.operation(conf.activeLayer)
		);
		con.restoreGState();
		
		con.saveGState();
		drawCellBorder(con, cellRect: rect, cell: calculatorCell, touching: touchingMe, operation: calculatorCell.operation(conf.activeLayer));
		con.restoreGState();
		
		drawCellText(con, cellRect: rect, cell: calculatorCell, touching: touchingMe, operation: calculatorCell.operation(activeLayer));
		
		if (debugGraphics)
		{
			// draw bounding box for reference
			// upper-left
			con.setLineWidth(1.0);
			
			con.setStrokeColor(UIColor.red.cgColor);
			con.move(to: CGPoint(x: 10, y: 0));
			con.addLine(to: CGPoint(x: 0, y: 0));
			con.addLine(to: CGPoint(x: 0, y: 10));
			
			con.strokePath();
			
			// upper-right
			con.setStrokeColor(UIColor.blue.cgColor);
			con.move(to: CGPoint(x: rect.width, y: 10));
			con.addLine(to: CGPoint(x: rect.width, y: 0));
			con.addLine(to: CGPoint(x: rect.width - 10, y: 0));
			
			con.strokePath();
			
			// lower-right
			con.setStrokeColor(UIColor.green.cgColor);
			con.move(to: CGPoint(x: rect.width, y: rect.height - 10));
			con.addLine(to: CGPoint(x: rect.width, y: rect.height));
			con.addLine(to: CGPoint(x: rect.width - 10, y: rect.height));
			
			con.strokePath();
			
			// lower-left
			con.setStrokeColor(UIColor.black.cgColor);
			con.move(to: CGPoint(x: 10, y: rect.height));
			con.addLine(to: CGPoint(x: 0, y: rect.height));
			con.addLine(to: CGPoint(x: 0, y: rect.height - 10));
			
			con.strokePath();
			
			// draw edgelines
			// left
			con.setStrokeColor(UIColor.purple.cgColor);
			con.move(to: CGPoint(x: 5, y: 1));
			con.addLine(to: CGPoint(x: 5, y: rect.height - 1));
			
			con.strokePath();
			
			// top
			con.setStrokeColor(UIColor.orange.cgColor);
			con.move(to: CGPoint(x: 1, y: 5));
			con.addLine(to: CGPoint(x: rect.width - 1, y: 5));
			
			con.strokePath();
			
			// right
			con.setStrokeColor(UIColor.magenta.cgColor);
			con.move(to: CGPoint(x: rect.width - 5, y: 1));
			con.addLine(to: CGPoint(x: rect.width - 5, y: rect.height - 1));
			
			con.strokePath();
			
			// bottom
			con.setStrokeColor(UIColor.darkGray.cgColor);
			con.move(to: CGPoint(x: rect.width - 1, y: rect.height - 5));
			con.addLine(to: CGPoint(x: 1, y: rect.height - 5));
			
			con.strokePath();
		}
		
		con.restoreGState();
	}
	
	fileprivate func drawCellBackground(
		_ context: CGContext,
		cellRect: CGRect,
		cell: CalculatorCell,
		touching: Bool,
		operation: Operation?
		)
	{
		let layer: Layer = configuration.activeLayer;
		
		// draw the outline around the box
		let clr: UIColor;
		
		if (touching)
		{
			clr = activeProfile.buttonPressedColor(buttonSelector: cell.profileKey(layer), layer: layer);
		}
		else
		{
			clr = activeProfile.buttonColor(buttonSelector: cell.profileKey(layer), layer: layer);
		}
		
		context.setFillColor(clr.cgColor);
		context.fill(cellRect);
	}
	
	fileprivate func drawCellBorder(_ context: CGContext, cellRect: CGRect, cell: CalculatorCell, touching: Bool, operation: Operation?)
	{
		let layer: Layer = activeLayer;
		
		context.setLineWidth(1.0);
		
		context.setStrokeColor(activeProfile.lineColor(
				buttonSelector: cell.profileKey(layer),
				layer: layer
				).cgColor
		);
		
		// draw the box around the key
		context.move(to: CGPoint(x: cellRect.origin.x, y: cellRect.origin.y));
		context.addLine(to: CGPoint(x: cellRect.origin.x + cellRect.size.width, y: cellRect.origin.y));

		context.move(to: CGPoint(x: cellRect.origin.x + cellRect.size.width, y: cellRect.origin.y));
		context.addLine(to: CGPoint(x: cellRect.origin.x + cellRect.size.width, y: cellRect.origin.y + cellRect.size.height));

		context.move(to: CGPoint(x: cellRect.origin.x + cellRect.size.width, y: cellRect.origin.y + cellRect.size.height));
		context.addLine(to: CGPoint(x: cellRect.origin.x, y: cellRect.origin.y + cellRect.size.height));

		context.move(to: CGPoint(x: cellRect.origin.x, y: cellRect.origin.y + cellRect.size.height));
		context.addLine(to: CGPoint(x: cellRect.origin.x, y: cellRect.origin.y));
		
		context.strokePath();
	}
	
	fileprivate func drawCellText(_ context: CGContext, cellRect: CGRect, cell: CalculatorCell, touching: Bool, operation: Operation?)
	{
		let layer: Layer = activeLayer;
		let buttonType = cell.profileKey(layer);
		
		let clr: UIColor;
		
		var state = 0;
		
		if (touching)
		{
			state |= 1;
			clr = activeProfile.activeMnemonicColorPressed(buttonSelector: cell.profileKey(layer), layer: layer);
		}
		else
		{
			// check for inactive
			if let op = operation
			{
				state |= 2;
				
				if (op.validateStack())
				{
					state |= 8;
					clr = activeProfile.activeMnemonicColor(buttonSelector: cell.profileKey(layer), layer: layer);
				}
				else
				{
					state |= 16;
					clr = activeProfile.inactiveMnemonicColor(buttonSelector: cell.profileKey(layer), layer: layer);
				}
			}
			else
			{
				state |= 4;
				
				clr = activeProfile.activeMnemonicColor(buttonSelector: cell.profileKey(layer), layer: layer);
			}
		}
		
		context.setStrokeColor(clr.cgColor);
		
		// overlay the mnemonic
		let mne: String?;
		let font: UIFont;
		
		if let operation = cell.operation(layer)
		{
			mne = operation.mnemonic();
			
			if operation.validateStack()
			{
				font = activeProfile.activeMnemonicFont(buttonSelector: buttonType, layer: layer)
			}
			else
			{
				font = activeProfile.inactiveMnemonicFont(buttonSelector: buttonType, layer: layer)
			}
		}
		else
		{
			font = activeProfile.inactiveMnemonicFont(buttonSelector: buttonType, layer: layer)
			mne = nil;
		}
		
		if let textToDraw = mne
		{
			lastMnemonic = textToDraw
			var textRect = CalculatorCellOperationUIView.makeNormalFacetTextRect(cellRect);
			
			// reset the coordinates to 0 for each cell
			let tx = textRect.origin.x;
			let ty = textRect.origin.y;
			
			context.clip(to: textRect);
			context.translateBy(x: tx, y: ty);
			
			textRect.origin.x = 0;
			textRect.origin.y = 0;
			
			//CGContextStrokeRect(context, textRect);
			drawText(
				context, text: textToDraw, textColor: clr, targetRect: textRect, useCache: true, pressed: touching, alignment: .center, state: state, font: font
			);
		}
	}
	
	func drawText(
		_ context: CGContext, text: String, textColor: UIColor, targetRect: CGRect, useCache: Bool, pressed: Bool, alignment: AlignPosition, state: Int, font: UIFont
		)
	{
		if text.trimmingCharacters(in: CharacterSet.whitespaces) == ""
		{
			return;
		}
		
		let key: String = makeKey(text + "." + pressed.description + "." + String(state));
		
		var strContext: (attributedString: NSAttributedString, boundsRect: CGRect);
		
		let stringContext = attributedStringCache[key];
		
		if useCache && stringContext != nil
		{
			strContext = stringContext!;
		}
		else
		{
			strContext = TextDrawing.makeAttributedStringForBounds(string: text, font: font, viewRect: targetRect, textColor: textColor);
		}
		
		TextDrawing.drawTextCentered(context: context, attributedString: strContext.attributedString, viewRect: targetRect, textRect: strContext.boundsRect, alignment: alignment);
		
		if (useCache)
		{
			attributedStringCache[key] = strContext;
		}
	}
	
	func makeKey(_ text: String) -> String
	{
		return text + "." + activeProfile.id() + "." + activeLayout.id() + "." + (configuration.isLandscape() ? "Landscape" : "Portrait");
	}
	
	open override func moderatedTouchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
	{
		if let op = calculatorCell.operation(activeLayer)
		{
			if (op.validateStack())
			{
				superUIView.touch(cellUIView: op.id());

				setNeedsDisplay();
			}
		}
	}
	
	open override func moderatedTouchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
	{
		superUIView.displayOffset = 0.0;
		
		if (touchingMe)
		{
			let layer = activeLayer;

			if let op = calculatorCell.operation(layer)
			{
				// perform this asynchronously
				CalculatorCellOperationUIView.operationQueue.async {
					self.superUIView.beginOperation();
					self.calculator.stack().checkPoint();
					self.activeLayout.enforceConsistency(self.calculator);
					
					do
					{
						try self.calculator.invoke(op.id());
					}
					catch
					{
						print("\(error)");
					}

					DispatchQueue.main.async(execute: {
						// make sure to update the UI to refelect possibly changed state
						self.superUIView.completeOperation();
						self.superUIView.untouch();
						self.superUIView.redrawSubviews();
					})
				}
			}
		}
	}
	
	open override func moderatedTouchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?)
	{
		if (touchingMe)
		{
			superUIView.untouch();
		}

		setNeedsDisplay();
	}
}
