//
//  ViewController.swift
//  Programmers Calculator
//
//  Created by Bradley Smith on 10/21/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController, UITableViewDelegate, OrientationListener {
	@IBOutlet var profileTableView: UITableView!;
	var profileTableDataSource: ProfileTableDataSource?;
	
	override func viewDidLoad() {
		super.viewDidLoad()

		profileTableView.backgroundColor = UIColor.black;

		profileTableDataSource = ProfileTableDataSource(viewController: self);
		
		profileTableView.rowHeight = CGFloat(Int(view.bounds.width / CGFloat(2.0)));
		
		profileTableView.dataSource = profileTableDataSource;
		profileTableView.delegate = self;
		
		AppDelegate.configuration.addOrientationListener(self);
	}
	
	func id() -> String
	{
		return "ProfileViewController";
	}
	
	func orientationChanged(_ o: Orientation)
	{
		profileTableView.reloadData();
		view.setNeedsLayout();
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

	@objc func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
	{
		// activate perspective.  Configuration will decide if we are in landscape or no.
		AppDelegate.configuration.activateProfile(AppDelegate.configuration.profiles()[indexPath.item]);
		profileTableView.setNeedsDisplay();

		AppDelegate.configuration.activeLayer = Layer.normal;
	}
}

class ProfileTableDataSource: NSObject, UITableViewDataSource
{
	fileprivate let profileLayout = AppDelegate.configuration.layout("_profile_cell")!;
	fileprivate let viewController: UIViewController;
	
	init(viewController: UIViewController)
	{
		self.viewController = viewController;
	}

	@objc func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		let rowCount = AppDelegate.configuration.profiles().count;
		
		return rowCount;
	}

	@objc func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let profile = AppDelegate.configuration.profile(AppDelegate.configuration.profiles()[indexPath.item]);
		let pname = profile.name();
		
		tableView.register(UINib(nibName: "CalculatorViewCell", bundle: nil), forCellReuseIdentifier: pname)

		let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: pname, for: indexPath);
		cell.layer.borderColor = UIColor.white.cgColor;
		cell.layer.borderWidth = 1.0

		//cell.textLabel?.text = rowText;

		let activeProfile = AppDelegate.configuration.activeProfile;

		//cell.backgroundColor = profile.buttonColor(buttonType: .display_x, layer: .normal);
		//cell.textLabel!.textColor = profile.activeMnemonicColor(buttonType: .display_x, layer: .normal);

		//tableView.separatorStyle = UITableViewCellSeparatorStyle.None;
	
		let pv = cell.viewWithTag(1) as! DemoCalculatorUIView;
		pv.setLayout(profileLayout);
		pv.setProfile(profile);
		pv.setDisplayName("3.14159");

		cell.setNeedsLayout();
		
		let ui = cell.viewWithTag(3);
		
		let bv = ui as! UIButton;
		
		if profile.id() != activeProfile.id()
		{
			bv.setTitle("Use " + profile.id(), for: UIControlState());
		}
		else
		{
			bv.setTitle("Using " + profile.name(), for: UIControlState());
			bv.isEnabled = false;
		}
		
		bv.addTarget(self, action: #selector(ProfileTableDataSource.useProfile(_:)), for: .touchUpInside)
		
		return cell;
	}
	
	func useProfile(_ sender: UIButton)
	{
		let label = sender.titleLabel!.text!;
		let layoutName = label.substring(from: label.characters.index(label.startIndex, offsetBy: 4));

		AppDelegate.configuration.activateProfile(layoutName)
		
		viewController.performSegue(withIdentifier: "unwind", sender: sender);
	}

	private func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
	}
}
