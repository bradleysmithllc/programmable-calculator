//
//  Described.swift
//  Programmable Calculator
//
//  Created by Bradley Smith on 11/24/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

public protocol Described
{
	func id() -> String;
	func name() -> String;
	func description() -> String;
}