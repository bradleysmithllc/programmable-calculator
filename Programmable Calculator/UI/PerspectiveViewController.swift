//
//  ViewController.swift
//  Programmers Calculator
//
//  Created by Bradley Smith on 10/21/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import UIKit

class PerspectiveViewController: UIViewController, UITableViewDelegate, OrientationListener {
	@IBOutlet var prespectiveTableView: UITableView!;
	var prespectiveTableDataSource: PerspectiveTableDataSource?;
	
	override func viewDidLoad() {
		super.viewDidLoad()

		prespectiveTableDataSource = PerspectiveTableDataSource(viewController: self);

		prespectiveTableView.rowHeight = view.bounds.size.height;

		prespectiveTableView.dataSource = prespectiveTableDataSource;
		prespectiveTableView.delegate = self;

		AppDelegate.configuration.addOrientationListener(self);
	}

	func id() -> String
	{
		return "PerspectiveViewController";
	}

	func orientationChanged(_ o: Orientation)
	{
		prespectiveTableView.reloadData();
		view.setNeedsLayout();
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

	@objc func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
	{
		// activate perspective.  Configuration will decide if we are in landscape or no.
		let layouts: [Layout] = prespectiveTableDataSource!.layouts();
		let layout = layouts[indexPath.item];

		AppDelegate.configuration.activateLayout(layout.id());
		prespectiveTableView.setNeedsDisplay();

		AppDelegate.configuration.activeLayer = Layer.normal;

		prespectiveTableView.reloadData();
	}
}

class PerspectiveTableDataSource: NSObject, UITableViewDataSource
{
	fileprivate let landscapeLayouts: [Layout];
	fileprivate let portraitLayouts: [Layout];
	fileprivate let viewController: UIViewController;

	init(viewController: UIViewController)
	{
		self.viewController = viewController;

		var _landscapeLayouts = [Layout]();
		var _portraitLayouts = [Layout]();

		for layout_id in AppDelegate.configuration.layouts()
		{
			let layout = AppDelegate.configuration.layout(layout_id)!;

			switch(layout.suggestedOrientation())
			{
				case .landscape:
					_landscapeLayouts.append(layout)
					break;
				case.portrait:
					_portraitLayouts.append(layout)
					break;
			}
		}

		landscapeLayouts = _landscapeLayouts;
		portraitLayouts = _portraitLayouts;

		super.init();
	}

	fileprivate func layouts() -> [Layout]
	{
		switch(AppDelegate.configuration.orientation())
		{
		case .landscape:
			return landscapeLayouts;
		case .portrait:
			return portraitLayouts;
		}
	}

	@objc func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return layouts().count;
	}
	
	@objc func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let perspective = layouts()[indexPath.item];

		//let rowText = perspective.name() + " (\(perspective.suggestedOrientation()))";
		let pname = perspective.name();

		tableView.register(UINib(nibName: "CalculatorViewCell", bundle: nil), forCellReuseIdentifier: pname)

		let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: pname)!;
		cell.layer.borderColor = UIColor.white.cgColor;
		cell.layer.borderWidth = 1.0

		/*
		cell.textLabel?.text = rowText;

		let activePortraitPerspective = Entrails.configuration.activePortraitLayout();
		let activeLandscapePerspective = Entrails.configuration.activeLandscapeLayout();

		// check for active
		else if perspective.id() == activeLandscapePerspective.id()
		{
			cell.textLabel?.text = rowText + " ↔️";
		}
		else
		{
			cell.backgroundColor = UIColor.whiteColor();
		}
		*/

		let pv = cell.viewWithTag(1) as! DemoCalculatorUIView;
		pv.setLayout(perspective);
		pv.setDisplayName("0");
		pv.setProfile(AppDelegate.configuration.activeProfile);
		
		cell.setNeedsLayout();

		let ui = cell.viewWithTag(3);

		let bv = ui as! UIButton;

		if perspective.id() != AppDelegate.configuration.activeLayout.id()
		{
			bv.setTitle("Use " + perspective.id(), for: UIControlState());
		}
		else
		{
			bv.setTitle("Using " + perspective.name(), for: UIControlState());
			bv.isEnabled = false;
		}

		bv.addTarget(self, action: #selector(PerspectiveTableDataSource.useLayout(_:)), for: .touchUpInside)

		return cell;
	}
	
	func useLayout(_ sender: UIButton)
	{
		let label = sender.titleLabel!.text!;
		let layoutName = label.substring(from: label.characters.index(label.startIndex, offsetBy: 4));

		AppDelegate.configuration.activateLayout(layoutName)

		viewController.performSegue(withIdentifier: "unwind", sender: sender);
	}

	private func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
	}
}
