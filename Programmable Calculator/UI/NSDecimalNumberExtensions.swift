//
//  NSDecimalNumberExtensions.swift
//  Calc
//
//  Created by Bradley Smith on 1/5/16.
//  Copyright © 2016 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/*
Extend NSDecimalNumber to include some functions
*/
extension NSDecimalNumber {
	/* Answers (aDecimal * -1) */
	func decimalNumberByNegating() -> NSDecimalNumber {
		if self == NSDecimalNumber.notANumber
		{
			return self;
		}

		return self.multiplying(by: NSDecimalNumber(mantissa: 1, exponent: 0, isNegative: true));
	}
	
	/* Answers |x| */
	func decimalNumberWithAbsoluteValue() -> NSDecimalNumber {
		if (doubleValue < 0)
		{
			return decimalNumberByNegating();
		}
		
		return self;
	}
	
	/**Returns a number that is appropriate for this base.
	*/
	func decimalNumberInBase(_ base: Int) -> NSDecimalNumber
	{
		if (base == 10)
		{
			return self;
		}
		
		return NSDecimalNumber(value: safeLongLongValue as Int64);
	}
	
	func decimalNumberRoundedToDigits(_ withDecimals: UInt8, roundingMode: NSDecimalNumber.RoundingMode = NSDecimalNumber.RoundingMode.plain) -> NSDecimalNumber
	{
		// round to required decimals
		let disp = rounding(accordingToBehavior: NSDecimalNumberHandler(roundingMode: roundingMode, scale: Int16(withDecimals), raiseOnExactness: false, raiseOnOverflow: false, raiseOnUnderflow: false, raiseOnDivideByZero: false));
		
		return disp;
	}
	
	/**Returns a formatted string in the base.
	*/
	func formattedNumberInBase(_ base: UInt8, unsigned: Bool) -> String
	{
		if (base == 10)
		{
			return String(describing: self);
		}
		else
		{
			var str: String;

			if (unsigned)
			{
				str = String(safeUnsignedLongLongValue, radix: Int(base));
			}
			else
			{
				str = String(safeLongLongValue, radix: Int(base));
			}

			if (base > 10)
			{
				str = str.uppercased()
			}
			
			return str;
		}
	}

	var safeByteValue: Int8 {
		get {
			let bits = UInt8(decimalNumberRoundedToDigits(0).int64Value & 0xFF);

			return Int8(bitPattern: bits);
		}
	};

	var safeUnsignedByteValue: UInt8 {
		get {
			let charValue = UInt8(decimalNumberRoundedToDigits(0).uint64Value & 0xFF);
			return charValue;
		}
	};

	var safeShortValue: Int16 {
		get {
			let bits = UInt16(decimalNumberRoundedToDigits(0).int64Value & 0xFFFF);
			
			return Int16(bitPattern: bits);
		}
	};
	
	var safeUnsignedShortValue: UInt16 {
		get {
			return UInt16(decimalNumberRoundedToDigits(0).uint64Value & 0xFFFF);
		}
	};

	var safeIntValue: Int32 {
		get {
			let bits = UInt32(decimalNumberRoundedToDigits(0).int64Value & 0xFFFFFFFF);
			
			return Int32(bitPattern: bits);
		}
	};

	var safeUnsignedIntValue: UInt32 {
		get {
			return UInt32(decimalNumberRoundedToDigits(0).uint64Value & 0xFFFFFFFF);
		}
	};
	
	var safeLongLongValue: Int64 {
		get {
			return decimalNumberRoundedToDigits(0).int64Value;
		}
	};
	
	var safeUnsignedLongLongValue: UInt64 {
		get {
			return decimalNumberRoundedToDigits(0).uint64Value;
		}
	};
}
