//
//  CalculatorCore.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

open class CalculatorCore: Calculator {
	fileprivate var _stack: OperandStack!;
	fileprivate var _operations: [String: Operation]! = [String: Operation]();

	fileprivate var observers: [OperationObserver] = [];
	fileprivate var mathHub = MathHub.mathHub();
	
	public init() {
		_stack = OperandStack(calculator: self);
		
		// There is no way there isn't a better way to do this.
		addOperation(AdditionOperation(calculator: self));
		addOperation(SubtractionOperation(calculator: self));
		addOperation(DivisionOperation(calculator: self));
		addOperation(MultiplicationOperation(calculator: self));
		addOperation(EnterOperation(calculator: self));
		addOperation(XYTranspositionOperation(calculator: self));
		addOperation(StackRotateUpOperation(calculator: self));
		addOperation(StackRotateDownOperation(calculator: self));
		addOperation(ModulusOperation(calculator: self));
		addOperation(YExpXOperation(calculator: self));
		addOperation(XExpYOperation(calculator: self));
		addOperation(TenExpXOperation(calculator: self));
		addOperation(TwoExpXOperation(calculator: self));
		addOperation(XSquaredOperation(calculator: self));
		addOperation(XCubedOperation(calculator: self));
		addOperation(ZeroLiteralOperation(calculator: self));
		addOperation(OneLiteralOperation(calculator: self));
		addOperation(TwoLiteralOperation(calculator: self));
		addOperation(ThreeLiteralOperation(calculator: self));
		addOperation(FourLiteralOperation(calculator: self));
		addOperation(FiveLiteralOperation(calculator: self));
		addOperation(SixLiteralOperation(calculator: self));
		addOperation(SevenLiteralOperation(calculator: self));
		addOperation(EightLiteralOperation(calculator: self));
		addOperation(NineLiteralOperation(calculator: self));
		addOperation(TenLiteralOperation(calculator: self));
		addOperation(ElevenLiteralOperation(calculator: self));
		addOperation(TwelveLiteralOperation(calculator: self));
		addOperation(ThirteenLiteralOperation(calculator: self));
		addOperation(FourteenLiteralOperation(calculator: self));
		addOperation(FifteenLiteralOperation(calculator: self));
		addOperation(DecimalLiteralOperation(calculator: self));
		addOperation(DropXOperation(calculator: self));
		addOperation(SineXOperation(calculator: self));
		addOperation(CosineXOperation(calculator: self));
		addOperation(SecantXOperation(calculator: self));
		addOperation(CosecantXOperation(calculator: self));
		addOperation(TangentXOperation(calculator: self));
		addOperation(CotangentXOperation(calculator: self));
		addOperation(ArcSineXOperation(calculator: self));
		addOperation(ArcCosineXOperation(calculator: self));
		addOperation(ArcSecantXOperation(calculator: self));
		addOperation(ArcCosecantXOperation(calculator: self));
		addOperation(ArcTangentXOperation(calculator: self));
		addOperation(ArcCotangentXOperation(calculator: self));
		addOperation(HyperbolicSineXOperation(calculator: self));
		addOperation(HyperbolicCosineXOperation(calculator: self));
		addOperation(HyperbolicSecantXOperation(calculator: self));
		addOperation(HyperbolicCosecantXOperation(calculator: self));
		addOperation(HyperbolicTangentXOperation(calculator: self));
		addOperation(HyperbolicCotangentXOperation(calculator: self));
		addOperation(HyperbolicArcSineXOperation(calculator: self));
		addOperation(HyperbolicArcCosineXOperation(calculator: self));
		addOperation(HyperbolicArcSecantXOperation(calculator: self));
		addOperation(HyperbolicArcCosecantXOperation(calculator: self));
		addOperation(HyperbolicArcTangentXOperation(calculator: self));
		addOperation(HyperbolicArcCotangentXOperation(calculator: self));
		addOperation(ChangeSignXOperation(calculator: self));
		addOperation(ResetOperation(calculator: self));
		addOperation(ClearXOperation(calculator: self));
		addOperation(UseRadiansOperation(calculator: self));
		addOperation(UseDegreesOperation(calculator: self));
		addOperation(UseGradiansOperation(calculator: self));
		addOperation(ToggleDegreesOperation(calculator: self));
		addOperation(PiOperation(calculator: self));
		addOperation(TwoPiOperation(calculator: self));
		addOperation(EOperation(calculator: self));
		addOperation(EulerOperation(calculator: self));
		addOperation(GoldenRatioOperation(calculator: self));
		addOperation(ReciprocalXOperation(calculator: self));
		addOperation(SquareRootXOperation(calculator: self));
		addOperation(CubeRootXOperation(calculator: self));
		addOperation(NthRootXOperation(calculator: self));
		addOperation(CheckpointOperation(calculator: self));
		addOperation(RestoreCheckpointOperation(calculator: self));
		addOperation(EExpXOperation(calculator: self));
		addOperation(BackupXOperation(calculator: self));
		addOperation(PercentOperation(calculator: self));
		addOperation(ToPercentOperation(calculator: self));
		addOperation(DeltaPercentOperation(calculator: self));
		addOperation(StoreXOperation(calculator: self));
		addOperation(StorePlusXOperation(calculator: self));
		addOperation(StoreMinusXOperation(calculator: self));
		addOperation(RecallXOperation(calculator: self));
		addOperation(LogBase10Operation(calculator: self));
		addOperation(LnOperation(calculator: self));
		addOperation(LogBase2Operation(calculator: self));
		addOperation(TruncateFractionalOperation(calculator: self));
		addOperation(TruncateIntegerOperation(calculator: self));
		addOperation(Base2Operation(calculator: self));
		addOperation(Base8Operation(calculator: self));
		addOperation(Base10Operation(calculator: self));
		addOperation(Base16Operation(calculator: self));
		addOperation(SumOperation(calculator: self));
		addOperation(SumProductOperation(calculator: self));
		addOperation(MemoryStoreOperation(calculator: self));
		addOperation(MemoryPlusOperation(calculator: self));
		addOperation(MemoryMinusOperation(calculator: self));
		addOperation(RecallMemoryOperation(calculator: self));
		addOperation(ClearMemoryOperation(calculator: self));
		addOperation(DoubleZeroLiteralOperation(calculator: self));
		addOperation(DoubleFLiteralOperation(calculator: self));
		addOperation(AndOperation(calculator: self));
		addOperation(OrOperation(calculator: self));
		addOperation(UnaryNotOperation(calculator: self));
		addOperation(NorOperation(calculator: self));
		addOperation(XorOperation(calculator: self));
		addOperation(LeftShiftOperation(calculator: self));
		addOperation(RightShiftOperation(calculator: self));
		addOperation(YLeftShiftXOperation(calculator: self));
		addOperation(YRightShiftXOperation(calculator: self));
		addOperation(RandOperation(calculator: self));
		addOperation(FactorialOperation(calculator: self));
		addOperation(ReduceFractionOperation(calculator: self));
		addOperation(FactorQuadraticOperation(calculator: self));
		addOperation(AbsoluteValueXOperation(calculator: self));
		addOperation(ConvertToRadiansOperation(calculator: self));
		addOperation(ConvertToDegreesOperation(calculator: self));
		addOperation(ConvertToGradiansOperation(calculator: self));
		addOperation(DecimalOperation(calculator: self));
		addOperation(SupplementaryAngleOperation(calculator: self));
		addOperation(ComplementaryAngleOperation(calculator: self));
		addOperation(ToggleWordSizeOperation(calculator: self));
		addOperation(ToggleSignedMathOperation(calculator: self));
		addOperation(DisabledOperation(calculator: self));
		addOperation(RoundOperation(calculator: self));
		addOperation(ConvertDateToJulianDayOperation(calculator: self));
		addOperation(ConvertJulianTODateOperation(calculator: self));
		addOperation(PercentXYOperation(calculator: self));
		addOperation(LeastCommonMultipleOperation(calculator: self));
	}

	open func addOperation(_ operation: Operation)
	{
		let id = operation.id();
		
		if let _ = _operations[id]
		{
			//fail.  I don't know how to handle this yet.
			fatalError("Duplicated operation id: " + id);
		}
		
		_operations[id] = operation;
		
		if (operation is OperationObserver) {
			observers.append(operation as! OperationObserver);
		}
	}

	open func stack() -> Stack {
		return _stack;
	}
	
	open func reset() {
		_stack.reset();
	}
	
	open func invoke(_ id: String) throws {
		mathHub.resetMessages();

		let op: Operation? = operation(id);
		
		if let ops: Operation = op {
			// broadcast change
			for var observer: OperationObserver in observers {
				observer.operationInvoked(self, operation: ops);
			}
			
			do {
				let currentNumericEntryState = _stack.numericEntryState()

				try ops.perform();
				
				let registerResultState = ops.registerResultState();
				
				if (registerResultState == NumericEntryState.in_NUMERIC_ENTRY)
				{
					// first, calculate current entry value
					let vcurrentInput: NSDecimalNumber = _stack.currentInput();
					
					if (
						currentNumericEntryState == NumericEntryState.in_NUMERIC_ENTRY
							||
							currentNumericEntryState == NumericEntryState.waiting_FOR_NUMERIC_ENTRY
						)
					{
						// replace the x register with this value
						stack().setX(vcurrentInput);
					}
					else
					{
						// push current value onto the stack
						stack().push(vcurrentInput);
					}
				}
				else
				{
					_stack.resetInputBuffer();
				}
				
				_stack.setNumericEntryState(registerResultState);
			} catch {
				print("\(error)");
			}
			
		} else {
			throw BadOperationState();
		}
	}
	
	open func operation(_ id: String) -> Operation? {
		return _operations[id];
	}
	
	open 	func operationIds() -> [String]
	{
		return Array(_operations.keys).sorted(){ $1 > $0 };
	}

	open func operations() -> [String:Operation] {
		return _operations;
	}
}
