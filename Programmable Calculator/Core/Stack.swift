//
//  Stack.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**A single element on the stack.  Currently can hold a number or a string.
*/
public struct StackElement
{
	fileprivate let _message: String?;
	fileprivate var _number: NSDecimalNumber?;

	public init(message: String)
	{
		_message = message;
		_number = nil;
	}

	public init(number: NSDecimalNumber)
	{
		_message = nil;
		_number = number;
	}
	
	init()
	{
		_message = nil;
		_number = nil;
	}

	/**Call isMessage before calling this!  It will force-unwrap the optional
	*/
	public func message() -> String
	{
		return _message!;
	}

	/**In keeping with the contract that the stack is infinite and every element not
	  *explicitly assigned a value contains 0, this method will return 0 if it
	  *represents nil, notANumber if it represents a message, or the number itself.
	  */
	public func number() -> NSDecimalNumber
	{
		if isNil
		{
			return NSDecimalNumber.zero;
		}
		else if isMessage
		{
			return NSDecimalNumber.notANumber;
		}
		else
		{
			return _number!;
		}
	}
	
	mutating func swapNumber(_ number: NSDecimalNumber)
	{
		_number = number;
	}

	public var isNil: Bool {
		get
		{
			return !isMessage && !isNumber;
		}
	}

	public var isMessage: Bool {
		get
		{
			return _message != nil;
		}
	}
	
	public var isNumber: Bool {
		get
		{
			return _number != nil;
		}
	}
}

let _nilElement: StackElement = StackElement();

/*A stack of operations constituting application state.  The stack maintains a real size and data values,
 *but at read time every operation will return at least the number 0.  The size function reflects
 *the actual size of operands.
 */
public protocol Stack {
	var _nil: StackElement { get };

	/**Push an element onto the stack.
    */
	func push(_ decNum: NSDecimalNumber);

	/**Push a message onto the stack.
	*/
	func push(_ message: String);
	
	/**Push a message onto the stack.
	*/
	func push(_ element: StackElement);
	
	/**Push an element onto the stack, but at the end, not at the head.
	*/
	func pushAtEnd(_ decNum: NSDecimalNumber);
	
	/**Push a message onto the stack, but at the end, not at the head.
	*/
	func pushAtEnd(_ message: String);
	
	/**Push an element onto the stack, but at the end, not at the head.
	*/
	func pushAtEnd(_ element: StackElement);
	
	/* Pops the most recently pushed element from the stack.
	*/
	func popAll() -> [StackElement];

	/* Pops the most recently pushed element from the stack.
	*/
	func pop() -> NSDecimalNumber;
	
	/* Pops the most recently pushed element from the stack.
	*/
	func popElement() -> StackElement;

	/* Removes the oldest element on the stack, I.E., the last one that would normally be popped.
	*/
	func popOldest() -> StackElement;

	/*The actual stack size.
	*/
	func size() -> Int;

	/** True if size == 0
	*/
	func empty() -> Bool;

	/*Peek at a specified location.  Will return 0 if there is nothing there.
	*/
  func peek(_ index: Int) -> NSDecimalNumber;
	
	/*Peek at a specified location.  Will return _nil if there is nothing there.
	*/
	func peekElement(_ index: Int) -> StackElement;

	/*Peek at the last operand.  Will return 0 if there is nothing there.
	*/
  func peek() -> NSDecimalNumber;

	/**Native peek function.
	*/
	func peekElement() -> StackElement;

	/*Clear actual stack state.
	 */
	func clear();

	/*Sets the value of the X register.  The X register is always the last objet on the stack, I.E.,
	 *the last operand pushed or the next one to be popped.
	 */
	func setX(_ number: NSDecimalNumber);
	
	/*Returns the contents of the X register, or 0.
	*/
	func getX() -> NSDecimalNumber;
	
	/*Returns the contents of the X register, or 0.
	*/
	func getXElement() -> StackElement;
	
	/*Gets the contents of the Y register.
	*/
	func getY() -> NSDecimalNumber;
	
	/*Gets the contents of the Y register.
	*/
	func getYElement() -> StackElement;
	
	/*Returns the contents of the Z register, or 0.
	*/
	func getZ() -> NSDecimalNumber;
	
	/*Returns the contents of the Z register, or 0.
	*/
	func getZElement() -> StackElement;
	
	/*Returns the contents of the T register, or 0.
	*/
	func getT() -> NSDecimalNumber;
	
	/*Returns the contents of the T register, or 0.
	*/
	func getTElement() -> StackElement;

	/**Gets the index register - a register that is not stack based and is used
	  *for indirection.
	  */
	func getIndex() -> NSDecimalNumber;
	
	/**Sets the index register.
	*/
	func setIndex(index: NSDecimalNumber);
	
	/**
	* A map of properties which represent calculator state, E.G., numeric base, etc.
	*/
	func properties() -> [String:Any];
	
	/** Read a property using the inferred type.  It is the callers responsibility to know
	* the type beforehand.
	*/
	func property<T>(_ propertyName: String) -> T?;
	
	/** Read a property using the inferred type.  It is the callers responsibility to know
	* the type beforehand.  If not set, use the default value as an in initial
	*/
	func propertyWithDefault<T>(_ propertyName: String, defaultValue: T) -> T;
	
	/** Write a property.  The newly written property value is returned and is guaranteed to be the same object as the
	*  supplied.
	*/
	func setProperty<T>(_ propertyName: String, propertyValue: T) -> T;
	
	/** Clear a property.  Returns the existing value, if any.
	*/
	func clearProperty<T>(_ propertyName: String) -> T?;
	
	/**
	* A map of properties which represent calculator state, E.G., numeric base, etc.
	*/
	func stateProperties() -> [String:Any];
	
	/** Read a property using the inferred type.  It is the callers responsibility to know
	* the type beforehand.
	*/
	func stateProperty<T>(_ propertyName: String) -> T?;
	
	/** Read a property using the inferred type.  It is the callers responsibility to know
	* the type beforehand.  If not set, use the default value as an in initial
	*/
 func statePropertyWithDefault<T>(_ propertyName: String, defaultValue: T) -> T;
	
	/** Write a property.  The newly written property value is returned and is guaranteed to be the same object as the
	*  supplied.
	*/
	func setStateProperty<T>(_ propertyName: String, propertyValue: T) -> T;
	
	/** Clear a property.  Returns the existing value, if any.
	*/
	func clearStateProperty<T>(_ propertyName: String) -> T?;

	/**
	* These are named memory areas.  Data is stored by a label for later use.
	*/
	func memory() -> [String:NSDecimalNumber]!;
	
	/** Read memory by name.
	*/
	func memory(_ slotName: String) -> NSDecimalNumber;
	
	/** Read memory by name.  Assign the default value if it is not set.
	*/
	mutating func memoryWithDefault(_ slotName: String, initialValue: NSDecimalNumber) -> NSDecimalNumber;
	
	/** Write to a named memory slot.
	*/
	mutating func assignMemory(_ slotName: String, value: NSDecimalNumber) -> NSDecimalNumber;
	
	/** Clear a memory slot.
	*/
	mutating func clearMemory(_ slotName: String) -> NSDecimalNumber?;

	/**Create a checkpoint which saves all state.
    */
	func checkPoint();
	
	/**Reverts to a previous checkpoint.  If there is none, this operation does nothing.
	*/
	func revertToCheckPoint();

	/**Tells checkpoints not to store more than depth states.
	*/
	func setCheckPointDepth(_ depth: Int);
	
	/**Size of the current integer mode.
	*/
	func getIntegerWordSize() -> IntegerWordSize;
	
	/**Size of the current integer mode.
	*/
	func setIntegerWordSize(_ size: IntegerWordSize);
	
	/**Decimal, or integer sign mode.
	*/
	func getNumericBehavior() -> NumericBehavior;
	
	/**Decimal, or integer sign mode.
	*/
	func setNumericBehavior(_ behavior: NumericBehavior);
	
	/**Gets the number of decimals to round to.
	*/
	func getDecimals() -> UInt8;
	
	/**Set the number of significant decimal places
	*/
	func setDecimals(_ num: UInt8);
	
	/**Gets the numeric base for data input and display.
	*/
	func getNumericBase() -> UInt8;
	
	/**Set the numeric base.
	*/
	func setNumericBase(base: UInt8);
	
	/**Reverses the sign of the current numeric entry
	*/
	func flipNumericEntrySignBit();
	
	/**Appends a digit to the numeric entry.  Use Calculator.decimalPoint for the decimal point.
	*/
	func addDigitToNumericEntry(_ digit: UInt8) throws;
	
	/**Are we currently in numeric entry?
	*/
	func inNumericEntry() -> Bool;
	func numericEntryState() -> NumericEntryState;
	
	/**Clears the last added digit, or decimal point.  Does nothing once the end of input is reached.
	*/
	func clearLastDigitInNumericEntry();
	
	func getNumericEntrySignBit() -> Bool;
	
	/**Resets the data input buffer to default.
	*/
	func resetInputBuffer();
	
	/**Gets the current value of the numeric entry
	*/
	func currentInput() -> NSDecimalNumber;
	
	/**Gets the current image of the numeric input.
	*/
	func currentInputImage() -> String;
	
	/**Gets a printable version of the current image.  I.E., no blank strings, ".0", etc.
	*/
	func printableInputImage() -> String;
	
	/**Determine if the current image has a decimal applied.
	*/
	func currentInputImageIncludesDecimal() -> Bool;
	
	func reset();
}

extension Stack
{
 public var _nil: StackElement {
		get
		{
			return _nilElement;
		}
	}
}
