//
//  Stack.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/12/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

open class OperandStack: Stack {
	fileprivate var mathHub: MathHub = MathHub.mathHub();

	fileprivate var elements: [StackElement] = [];
	fileprivate var _namedMemory: [String: NSDecimalNumber]! = [String: NSDecimalNumber]();
	fileprivate var _properties: [String: Any] = [String: Any]();
	fileprivate var _stateProperties: [String: Any] = [String: Any]();
	
	fileprivate var checkpoints = Array<(stack: Array<StackElement>, memory: [String: NSDecimalNumber], properties: [String: Any], stateProperties: [String: Any])>();
	
	fileprivate var calculator: Calculator;
	
	public init(calculator: Calculator) {
		self.calculator = calculator;
		clear();
	}
	
	open func pushAtEnd(_ decNum: NSDecimalNumber)
	{
		pushAtEnd(StackElement(number: decNum));
	}
	
	open func pushAtEnd(_ message: String)
	{
		pushAtEnd(StackElement(message: message));
	}

	/**Push an element onto the stack, but at the end, not at the head.
	*/
	open func pushAtEnd(_ element: StackElement)
	{
		push(element, atIndex: 0);
	}

	open func push(_ message: String)
	{
		push(StackElement(message: message));
	}

	/**Push a message onto the stack.
	*/
	open func push(_ element: StackElement)
	{
		push(element, atIndex: elements.count);
	}

	open func push(_ decNum: NSDecimalNumber) {
		push(StackElement(number: decNum));
	}
	
	fileprivate func push(_ _element: StackElement, atIndex: Int)
	{
		var mutElement = _element;

		// force the number to fit into the current base
		if mutElement.isNumber
		{
			switch(getNumericBehavior())
			{
			case .Decimal:
				// Constrain to decimal count
				mutElement.swapNumber(mutElement.number().decimalNumberRoundedToDigits(getDecimals()));
				break;
			case .UnsignedInteger:
				switch(getIntegerWordSize())
				{
				case .byte:
					mutElement.swapNumber(NSDecimalNumber(value: mutElement.number().safeUnsignedByteValue as UInt8));
				case .word_2:
					mutElement.swapNumber(NSDecimalNumber(value: mutElement.number().safeUnsignedShortValue as UInt16));
				case .word_4:
					mutElement.swapNumber(NSDecimalNumber(value: mutElement.number().safeUnsignedIntValue as UInt32));
				case .word_8:
					mutElement.swapNumber(NSDecimalNumber(value: mutElement.number().safeUnsignedLongLongValue as UInt64));
				case .none:
				break;
				}
			case .SignedInteger:
				switch(getIntegerWordSize())
				{
				case .byte:
					mutElement.swapNumber(NSDecimalNumber(value: mutElement.number().safeByteValue as Int8));
				case .word_2:
					mutElement.swapNumber(NSDecimalNumber(value: mutElement.number().safeShortValue as Int16));
				case .word_4:
					mutElement.swapNumber(NSDecimalNumber(value: mutElement.number().safeIntValue as Int32));
				case .word_8:
					mutElement.swapNumber(NSDecimalNumber(value: mutElement.number().safeLongLongValue as Int64));
				case .none:
					break;
				}
			}
		}

		elements.insert(mutElement, at: atIndex);
	}
	
	open func pop() -> NSDecimalNumber {
		return popElement().number();
	}

	open func popElement() -> StackElement {
		if (size() > 0) {
			return elements.removeLast();
		}
		else
		{
			return _nil;
		}
	}
	
	open func setX(_ number: NSDecimalNumber) {
		// implement as pop and push to make sure that push is the only place data can enter the stack
		let _ = pop();
		push(number);
		/*
		if (size() > 0) {
			elements[size() - 1] = StackElement(number: number);
		} else {
			elements.append(StackElement(number: number));
		}*/
	}
	
	open func getX() -> NSDecimalNumber {
		return getXElement().number();
	}

	open func getXElement() -> StackElement
	{
		return peekElement();
	}
	
	open func getYElement() -> StackElement
	{
		return peekElement(size() - 2);
	}
	
	open func getY() -> NSDecimalNumber {
		return getYElement().number();
	}
	
	open func getZElement() -> StackElement
	{
		return peekElement(size() - 3);
	}
	
	open func getZ() -> NSDecimalNumber {
		return getZElement().number();
	}
	
	open func getTElement() -> StackElement
	{
		return peekElement(size() - 4);
	}
	
	open func getT() -> NSDecimalNumber {
		return getTElement().number();
	}

	open func popOldest() -> StackElement {
		if (size() > 0) {
			return elements.removeFirst();
		}
		else
		{
			return _nil;
		}
	}
	
	open func size() -> Int {
		return elements.count;
	}
	
	open func empty() -> Bool
	{
		return size() == 0;
	}
	
	open func peek() -> NSDecimalNumber {
		return peek(size() - 1);
	}
	
	open func popAll() -> [StackElement] {
		let ele = elements;

		elements.removeAll();

		return ele;
	}

	open func peekElement() -> StackElement {
		return peekElement(size() - 1);
	}
	
	open func peek(_ index: Int) -> NSDecimalNumber {
		return peekElement(index).number();
	}
	
	open func peekElement(_ index: Int) -> StackElement {
		var offset: Int = size() - 1;
		
		if (index != -1) {
			offset = index;
		}
		
		if (index < 0 || index >= size())
		{
			return _nil;
		}
		
		return elements[offset];
	}

	open func clear() {
		_namedMemory.removeAll();
		elements.removeAll();
		indexRegister = NSDecimalNumber.zero;
	}
	
	fileprivate var indexRegister: NSDecimalNumber = NSDecimalNumber.zero;
	
	open func getIndex() -> NSDecimalNumber
	{
		return indexRegister;
	}
	
	open func setIndex(index: NSDecimalNumber)
	{
		indexRegister = index;
	}
	
	open func checkPoint()
	{
		checkpoints.insert((stack: elements, memory: _namedMemory, properties: _properties, stateProperties: _stateProperties), at: 0)

		while checkpoints.count > checkPointDepth
		{
			checkpoints.removeLast();
		}
	}
	
	var checkPointDepth: Int = 10;

	open func setCheckPointDepth(_ depth: Int)
	{
		checkPointDepth = depth;
	}

	open func revertToCheckPoint()
	{
		if (checkpoints.count == 0)
		{
			return;
		}

		let lastCheck = checkpoints.removeFirst();

		elements = lastCheck.stack;
		_namedMemory = lastCheck.memory;
		_properties = lastCheck.properties;
		_stateProperties = lastCheck.stateProperties;
	}

	open func memory(_ slotName: String) -> NSDecimalNumber {
		if let mem = _namedMemory[slotName]
		{
			return mem;
		}
		
		return NSDecimalNumber.zero;
	}
	
	open func memoryWithDefault(_ slotName: String, initialValue: NSDecimalNumber) -> NSDecimalNumber {
		if let mem = _namedMemory[slotName] {
			return mem;
		}
		
		return assignMemory(slotName, value: initialValue);
	}
	
	open func assignMemory(_ slotName: String, value: NSDecimalNumber) -> NSDecimalNumber {
		_namedMemory[slotName] = value;
		
		return value;
	}
	
	open func clearMemory(_ slotName: String) -> NSDecimalNumber? {
		return _namedMemory.removeValue(forKey: slotName);
	}
	
	open func memory() -> [String:NSDecimalNumber]! {
		return _namedMemory;
	}
	
	open func properties() -> [String:Any] {
		return _properties;
	}
	
	func property0(_ properties: [String:Any], propertyName: String) -> Any? {
		let v = properties[propertyName];
		
		return v;
	}
	
	fileprivate func _property<T>(_ properties: [String:Any], propertyName: String) -> T? {
		let v: Any? = property0(properties, propertyName: propertyName);
		
		if let tt: T = (v as? T) {
			return tt;
		}
		
		return nil;
	}
	
	fileprivate func _propertyWithDefault<T>(_ properties: inout [String:Any], propertyName: String, defaultValue: T) -> T {
		let t: T? = _property(properties, propertyName: propertyName)
		
		if let tt: T = t {
			return tt;
		}
		
		return _setProperty(&properties, propertyName: propertyName, propertyValue: defaultValue);
	}
	
	fileprivate func _setProperty<T>(_ properties: inout [String:Any], propertyName: String, propertyValue: T) -> T {
		properties[propertyName] = propertyValue;
		
		return propertyValue;
	}
	
	fileprivate func _clearProperty<T>(_ properties: inout [String:Any], propertyName: String) -> T? {
		return properties.removeValue(forKey: propertyName) as? T;
	}
	
	open func property<T>(_ propertyName: String) -> T? {return _property(_properties, propertyName: propertyName);}
	open func propertyWithDefault<T>(_ propertyName: String, defaultValue: T) -> T {return _propertyWithDefault(&_properties, propertyName: propertyName, defaultValue: defaultValue);}
	open func setProperty<T>(_ propertyName: String, propertyValue: T) -> T {return _setProperty(&_properties, propertyName: propertyName, propertyValue: propertyValue);}
	open func clearProperty<T>(_ propertyName: String) -> T? {return _clearProperty(&_properties, propertyName: propertyName);
	}
	
	open func reset() {
		clear();
		_properties.removeAll();
	}
	
	open func stateProperties() -> [String:Any] {
		return _stateProperties;
	}

	open func stateProperty<T>(_ propertyName: String) -> T? {return _property(_stateProperties, propertyName: propertyName);}
	
	open func statePropertyWithDefault<T>(_ propertyName: String, defaultValue: T) -> T {return _propertyWithDefault(&_stateProperties, propertyName: propertyName, defaultValue: defaultValue);}
	
	open func setStateProperty<T>(_ propertyName: String, propertyValue: T) -> T {return _setProperty(&_stateProperties, propertyName: propertyName, propertyValue: propertyValue);}
	
	open func clearStateProperty<T>(_ propertyName: String) -> T? {return _clearProperty(&_stateProperties, propertyName: propertyName);}
	
	/**Gets the number of decimals to round to.
	*/
	open func getDecimals() -> UInt8
	{
		return statePropertyWithDefault(CalculatorProperties.SIGNIFICANT_DECIMALS, defaultValue: 5);
	}
	
	/**Set the number of significant decimal places
	*/
	open func setDecimals(_ num: UInt8)
	{
		let _ = setStateProperty(CalculatorProperties.SIGNIFICANT_DECIMALS, propertyValue: num);
	}
	
	open func getIntegerWordSize() -> IntegerWordSize
	{
		return statePropertyWithDefault(CalculatorProperties.INTEGER_WORD_SIZE, defaultValue: IntegerWordSize.none);
	}
	
	open func setIntegerWordSize(_ size: IntegerWordSize)
	{
		let _ = setStateProperty(CalculatorProperties.INTEGER_WORD_SIZE, propertyValue: size);
		
		// run through the stack and convert all to new size.  This may have to be done to memory as well
		let ssize = self.size()
		
		for _ in 0..<ssize
		{
			// rely on the push function to properly adjust the stack
			pushAtEnd(popElement());
		}
	}
	
	open func getNumericBehavior() -> NumericBehavior
	{
		return statePropertyWithDefault(CalculatorProperties.NUMERIC_BEHAVIOR, defaultValue: NumericBehavior.Decimal);
	}
	
	open func setNumericBehavior(_ behavior: NumericBehavior)
	{
		let _ = setStateProperty(CalculatorProperties.NUMERIC_BEHAVIOR, propertyValue: behavior);
		
		// sanity checks.  If changing to decimal, no word size is selected.
		if behavior == .Decimal
		{
			setIntegerWordSize(.none);
		}
		else
		{
			// select a default word size if None is current
			if getIntegerWordSize() == .none
			{
				setIntegerWordSize(.word_8);
			}
			else
			{
				// run through the stack and convert all to new size.  This may have to be done to memory as well
				let ssize = self.size()
				
				for _ in 0..<ssize
				{
					// rely on the push function to properly adjust the stack
					pushAtEnd(popElement());
				}
			}
		}
	}
	
	/**Gets the numeric base for data input and display.
	*/
	open func getNumericBase() -> UInt8
	{
		return statePropertyWithDefault(CalculatorProperties.NUMERIC_BASE, defaultValue: 10);
	}
	
	/**Set the numeric base.
	*/
	open func setNumericBase(base: UInt8)
	{
		let _ = setStateProperty(CalculatorProperties.NUMERIC_BASE, propertyValue: base);
	}
	
	fileprivate static let BASE_DIGITS = [
		"0",
		"1",
		"2",
		"3",
		"4",
		"5",
		"6",
		"7",
		"8",
		"9",
		"A",
		"B",
		"C",
		"D",
		"E",
		"F"
	];
	
	fileprivate static let BASE_DIGIT_MAP: [String: NSDecimalNumber] = [
		"0": NSDecimalNumber.zero,
		"1": NSDecimalNumber.one,
		"2": NSDecimalNumber(value: 2 as Int),
		"3": NSDecimalNumber(value: 3 as Int),
		"4": NSDecimalNumber(value: 4 as Int),
		"5": NSDecimalNumber(value: 5 as Int),
		"6": NSDecimalNumber(value: 6 as Int),
		"7": NSDecimalNumber(value: 7 as Int),
		"8": NSDecimalNumber(value: 8 as Int),
		"9": NSDecimalNumber(value: 9 as Int),
		"A": NSDecimalNumber(value: 10 as Int),
		"B": NSDecimalNumber(value: 11 as Int),
		"C": NSDecimalNumber(value: 12 as Int),
		"D": NSDecimalNumber(value: 13 as Int),
		"E": NSDecimalNumber(value: 14 as Int),
		"F": NSDecimalNumber(value: 15 as Int)
	];
	
	open func addDigitToNumericEntry(_ digit: UInt8) throws
	{
		var currentText: String = currentInputImage();
		
		// special case.  Don't add a bunch of 0's as the most significant digit
		guard (!(digit == 0 && currentText.isEmpty)) else
		{
			return;
		}
		
		if (digit == 0xFF)
		{
			// decimal point
			if (currentInputImageIncludesDecimal())
			{
				throw BadOperationState() as Error;
			}
			else
			{
				// if this is the first input, prepend '0'
				if (currentText == "")
				{
					currentText = "0.";
				}
				else
				{
					currentText += ".";
				}
			}
		}
		else
		{
			currentText += OperandStack.BASE_DIGITS[Int(digit)];
		}
		
		updateCurrentInputImage(currentText);
	}
	
	open func resetInputBuffer() {
		let _ : String? = clearProperty(CalculatorProperties.NUMERIC_ENTRY_BUFFER);
		let _ : Bool? = clearProperty(CalculatorProperties.NUMERIC_ENTRY_NEGATIVE_FLAG);
	}
	
	open func currentInput() -> NSDecimalNumber {
		// iterate over the digits of the string and create the numeric representation
		let image: String = currentInputImage();
		
		var decimal: NSDecimalNumber = NSDecimalNumber.zero;
		var integral: NSDecimalNumber = NSDecimalNumber.zero;
		
		if image.contains(".")
		{
			// grab the fractional portion
			decimal = createFractionalPart(image, index: image.index(after: image.characters.index(of: ".")!));
			integral = createIntegralPart(image, index: image.characters.index(of: ".")!);
		}
		else
		{
			integral = createIntegralPart(image, index: image.endIndex);
		}
		
		var result: NSDecimalNumber = mathHub.add(left: integral, right: decimal);
		
		if (getNumericEntrySignBit())
		{
			result = result.decimalNumberByNegating();
		}
		
		return result;
	}
	
	open func flipNumericEntrySignBit()
	{
		let neg : Bool = propertyWithDefault(CalculatorProperties.NUMERIC_ENTRY_NEGATIVE_FLAG, defaultValue: false);
		
		let _ = setProperty(CalculatorProperties.NUMERIC_ENTRY_NEGATIVE_FLAG, propertyValue: !neg);
	}
	
	open func getNumericEntrySignBit() -> Bool
	{
		return propertyWithDefault(CalculatorProperties.NUMERIC_ENTRY_NEGATIVE_FLAG, defaultValue: false);
	}
	
	fileprivate func createIntegralPart(_ input: String, index: String.CharacterView.Index) -> NSDecimalNumber
	{
		var pred = index;
		
		let base: NSDecimalNumber = NSDecimalNumber(value: Int(getNumericBase()) as Int);
		var power: NSDecimalNumber = NSDecimalNumber.one;
		var result: NSDecimalNumber = NSDecimalNumber.zero;
		
		while(pred != input.startIndex)
		{
			pred = input.index(before: pred);
			
			let ch = input[pred];
			
			let digit: NSDecimalNumber = OperandStack.BASE_DIGIT_MAP[String(ch)]!;
			
			// multiply this digit by the current power and add to the in-progress result
			let digitScaled = mathHub.multiply(left: power, right: digit);
			
			result = mathHub.add(left: result, right: digitScaled);
			
			power = mathHub.multiply(left: power, right: base);
		}
		
		return result;
	}
	
	fileprivate func createFractionalPart(_ input: String, index: String.CharacterView.Index) -> NSDecimalNumber
	{
		var pred = index;
		
		let base: NSDecimalNumber = NSDecimalNumber(value: Int(getNumericBase()) as Int);
		var power: NSDecimalNumber = NSDecimalNumber.one;
		var result: NSDecimalNumber = NSDecimalNumber.zero;
		
		while(pred != input.endIndex)
		{
			// move the decimal point one to the right
			power = mathHub.divide(dividend: power, divisor: base);
			
			let ch = input[pred];
			
			let digit: NSDecimalNumber = OperandStack.BASE_DIGIT_MAP[String(ch)]!;
			
			// multiply this digit by the current power and add to the in-progress result
			let digitScaled = mathHub.multiply(left: power, right: digit);
			
			// add 1/scaledDigit to the result
			result = mathHub.add(left: result, right: digitScaled);
			
		 	pred = input.index(after: pred);
		}
		
		return result;
	}
	
	open func numericEntryState() -> NumericEntryState
	{
		return propertyWithDefault(CalculatorProperties.NUMERIC_ENTRY_STATE, defaultValue: NumericEntryState.waiting_FOR_OPERATION);
	}
	
	open func setNumericEntryState(_ state: NumericEntryState)
	{
		let _ = setProperty(CalculatorProperties.NUMERIC_ENTRY_STATE, propertyValue: state);
	}
	
	open func inNumericEntry() -> Bool
	{
		return numericEntryState() == NumericEntryState.in_NUMERIC_ENTRY;
	}
	
	open func beginNumericEntry()
	{
		let _ = setProperty(CalculatorProperties.NUMERIC_ENTRY_STATE, propertyValue: NumericEntryState.in_NUMERIC_ENTRY);
	}
	
	open func currentInputImage() -> String
	{
		return propertyWithDefault(CalculatorProperties.NUMERIC_ENTRY_BUFFER, defaultValue: "");
	}
	
	open func printableInputImage() -> String
	{
		let img: String = currentInputImage();
		
		if (img == "")
		{
			return "0";
		}
		else if img[img.startIndex] == "."
		{
			return "0" + img;
		}
		
		return img;
	}

	fileprivate func updateCurrentInputImage(_ new: String)
	{
		let _ = setProperty(CalculatorProperties.NUMERIC_ENTRY_BUFFER, propertyValue: new);
	}

	open func currentInputImageIncludesDecimal() -> Bool
	{
		return currentInputImage().contains(".");
	}
	
	open func clearLastDigitInNumericEntry()
	{
		let str = currentInputImage();
		
		if (str.isEmpty)
		{
			return;
		}
		
		updateCurrentInputImage(str.substring(to: str.characters.index(str.endIndex, offsetBy: -1)));
	}
}
