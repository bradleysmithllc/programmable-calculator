//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Calculates what percentage X is of Y and returns the result as a percentage.
**/

open class PercentXYOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		let x: NSDecimalNumber! = stack.pop();
		let y: NSDecimalNumber! = stack.pop();

		let percentDecimal: NSDecimalNumber = mathHub.divide(dividend: x, divisor: y);

		let result: NSDecimalNumber = mathHub.multiply(left: ToPercentOperation._100, right: percentDecimal);

		stack.push(result);
	}

	open override func id() -> String {
		return "xpercenty";
	}

	open override func mnemonic() -> String {
		return "x/y%";
	}

	open override func stackAffect() -> [StackAffect]?
	{
		return [.Pop, .Pop, .Push];
	}
	
	open override func formula() -> String?
	{
		return
			"<math xmlns=\"http://www.w3.org/1998/Math/MathML\">" +
				"<mrow>" +
					"<mn>100</mn>" +
					"<mo>&sdot;</mo>" +
					"<mfrac>" +
						"<mi><stack-x /></mi>" +
						"<mi><stack-y /></mi>" +
					"</mfrac>" +
				"</mrow>" +
			"</math>";
	}
	
	open override func description() -> String
	{
		return "Calculates what percentage x is of y.";
	}
}
