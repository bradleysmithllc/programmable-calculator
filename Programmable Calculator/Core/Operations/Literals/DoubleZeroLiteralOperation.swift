//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
 * Pushes a literal to the right side of the number in the x register.
 **/

open class DoubleZeroLiteralOperation: BasicOperation {
	open override func perform() throws {
		try stack.addDigitToNumericEntry(0);
		try stack.addDigitToNumericEntry(0);
	}

	public final override func registerResultState() -> NumericEntryState
	{
		return NumericEntryState.in_NUMERIC_ENTRY;
	}

	open override func validateStack() -> Bool {
		return BaseLiteralOperation.validateStack(calculator.stack(), doubleDigit: true, digitToCheck: 0);
	}

	open override func id() -> String {
		return "00";
	}
	
	open override func mnemonic() -> String {
		return "00";
	}
	
	open override func operationType() -> OperationType
	{
		return .literal;
	}
}
