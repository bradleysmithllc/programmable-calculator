//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation;

/**
 * Pushes a literal to the right side of the number in the x register.
 **/

open class BaseLiteralOperation: BasicOperation {
	fileprivate static let negativeOne: NSDecimalNumber = NSDecimalNumber(value: -1 as Int);

	fileprivate let value: UInt8;
	fileprivate let _mnemonic: String;

	public required init(calculator: Calculator) {
		fatalError("Class is abstract.")
	}

	init(calculator: Calculator, value v: UInt8) {
		value = v;
		_mnemonic = String(value);

		super.init(calculator: calculator);
	}

	init(calculator: Calculator, value v: UInt8, mnemonic: String) {
		value = v;
		_mnemonic = mnemonic;

		super.init(calculator: calculator);
	}

	public final override func registerResultState() -> NumericEntryState
	{
		return NumericEntryState.in_NUMERIC_ENTRY;
	}

	open override func validateStack() -> Bool {
		return BaseLiteralOperation.validateStack(calculator.stack(), doubleDigit: false, digitToCheck: value);
	}

	open static func validateStack(_ stack: Stack, doubleDigit: Bool, digitToCheck: UInt8) -> Bool {
		var digitsToAllow: UInt8;
		
		// reasonable allowances for different bases
		if (stack.getNumericBase() == 16)
		{
			// constrain to the word size
			switch(stack.getIntegerWordSize())
			{
			case .none:
				fatalError();
			case .byte:
				digitsToAllow = 2;
			case .word_2:
				digitsToAllow = 4;
			case .word_4:
				digitsToAllow = 8;
			case .word_8:
				digitsToAllow = 16;
			}
		}
		else if (stack.getNumericBase() == 10)
		{
			// constrain to the word size.  These are approximate - still possible to enter greater than max
			switch(stack.getIntegerWordSize())
			{
			case .none:
				digitsToAllow = 38;
			case .byte:
				digitsToAllow = 3;
			case .word_2:
				digitsToAllow = 5;
			case .word_4:
				digitsToAllow = 10;
			case .word_8:
				digitsToAllow = 20;
			}
		}
		else if (stack.getNumericBase() == 8)
		{
			// constrain to the word size.  These are approximate - still possible to enter greater than max
			switch(stack.getIntegerWordSize())
			{
			case .none:
				fatalError();
			case .byte:
				digitsToAllow = 3;
			case .word_2:
				digitsToAllow = 6;
			case .word_4:
				digitsToAllow = 11;
			case .word_8:
				digitsToAllow = 22;
			}
		}
		else //if (stack.getNumericBase() == 2)
		{
			// constrain to the word size
			switch(stack.getIntegerWordSize())
			{
			case .none:
				fatalError();
			case .byte:
				digitsToAllow = 8;
			case .word_2:
				digitsToAllow = 16;
			case .word_4:
				digitsToAllow = 32;
			case .word_8:
				digitsToAllow = 64;
			}
		}

		if doubleDigit
		{
			digitsToAllow -= 1;
		}

		return BaseLiteralOperation.validateStack(stack, digitsToAllow: digitsToAllow, digitToCheck: digitToCheck);
	}

	open static func validateStack(_ stack: Stack, digitsToAllow: UInt8, digitToCheck: UInt8) -> Bool {
		let currentInput = stack.currentInputImage();

		// not so much fun, but required I think . . . ?
		var maxDigits = digitsToAllow;

		if (stack.currentInputImageIncludesDecimal())
		{
			// allow one more 'digit' for the decimal character
			maxDigits += 1;

			if (currentInput.characters[currentInput.startIndex] == "0")
			{
				// since this is a decimal number, the leading '0' doesn't exist and therefore does not count
				// against the input tally
				maxDigits += 1;
			}
		}

		let validInputState: Bool;
		let charCount = currentInput.characters.count;

		if charCount >= Int(maxDigits)
		{
			validInputState = false;
		}
		else
		{
			validInputState = true;
		}

		let numericBase = stack.getNumericBase();

		let numericBaseTest = numericBase > digitToCheck;

		let test = validInputState && numericBaseTest;

		//print("currentInput \(currentInput)\ncharCount \(charCount)\nmaxDigits \(maxDigits)\ndigitsToAllow \(digitsToAllow)\ndigitToCheck \(digitToCheck)\nnumericBaseTest \(numericBaseTest)\nvalidInputState \(validInputState)\ntest: \(test)");

		return test;
	}

	open override func perform() throws {
		// just append to the numeric entry
		try stack.addDigitToNumericEntry(value);
	}
	
	open override func id() -> String {
		return _mnemonic;
	}
	
	open override func mnemonic() -> String {
		return _mnemonic;
	}

	open override func operationType() -> OperationType
	{
		return .literal;
	}
}
