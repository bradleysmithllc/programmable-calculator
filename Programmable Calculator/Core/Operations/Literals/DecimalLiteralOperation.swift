//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
 * Inserts the decimal point in the numeric input.
 **/
open class DecimalLiteralOperation: BaseLiteralOperation {
	public required init(calculator: Calculator) {
		super.init(calculator: calculator, value: calculator.decimalPoint);
	}

	open override func validateStack() -> Bool {
		return stack.getNumericBehavior() == .Decimal && stack.getNumericBase() == 10 && !stack.currentInputImageIncludesDecimal() && stack.getDecimals() > 0;
	}
	
	open override func id() -> String {
		return ".";
	}
	
	open override func mnemonic() -> String {
		return ".";
	}
}
