//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
  * Clears the X register.
  **/

open class ClearXOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();
		
		// there are two modes here
		let inNumericEntry = stack.inNumericEntry();
		
		// 1 - If in numeric entry, simply reset the input buffer
		if (inNumericEntry)
		{
			stack.resetInputBuffer();
		}

		stack.setX(NSDecimalNumber.zero);
	}
	
	open override func id() -> String {
		return "clx";
	}

	open override func mnemonic() -> String {
		return "clx";
	}
	
	open override func description() -> String
	{
		return "Clears the value in the X register and replaces with 0.  The stack in unaffected other than the change to the X register.";
	}
	
	public final override func registerResultState() -> NumericEntryState
	{
		return NumericEntryState.waiting_FOR_NUMERIC_ENTRY;
	}
	
	open override func operationType() -> OperationType
	{
		return .state;
	}
}
