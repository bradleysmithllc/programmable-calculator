//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
  * Adds the value in the Y register in the memory slot identified by X.  The X register
  * is cleared but the Y register remains in X.
  **/

open class StorePlusXOperation: BasicOperation {
	open override func perform() throws {
		var stack = calculator.stack();

		let slotId: NSDecimalNumber = stack.pop();
		let valueToStore = stack.getX();

		let slotKey = String(slotId.intValue);
		
		let _ = stack.assignMemory(slotKey,
			value: mathHub.add(left: valueToStore, right: stack.memory(slotKey))
		);
	}
	
	open override func id() -> String {
		return "store_plus";
	}
	
	open override func description() -> String
	{
		return "Add the contents of the X register to the stored memory register.";
	}

	open override func mnemonic() -> String {
		return "sto+";
	}
	
	open override func operationType() -> OperationType
	{
		return .state;
	}
}
