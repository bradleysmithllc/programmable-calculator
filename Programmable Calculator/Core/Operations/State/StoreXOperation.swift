//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
  * Stores the value in the Y register in the memory slot identified by X.  The X register
  * is cleared but the Y register remains in X.
  **/

open class StoreXOperation: BasicOperation {
	open override func perform() throws {
		var stack = calculator.stack();

		let slotId: NSDecimalNumber = stack.pop();
		let valueToStore = stack.getX();

		let _ = stack.assignMemory(String(slotId.intValue), value: valueToStore);
	}
	
	open override func id() -> String {
		return "store";
	}
	
	open override func description() -> String
	{
		return "Store the contents of the X register in the stored memory register.";
	}

	open override func mnemonic() -> String {
		return "sto";
	}
	
	open override func operationType() -> OperationType
	{
		return .state;
	}
}
