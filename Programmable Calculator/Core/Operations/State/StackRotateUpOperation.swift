//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
 * Pops the element off the top of the stack (oldest element), and pushes it onto the bottom.
 **/

open class StackRotateUpOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		if (stack.size() > 1) {
			stack.push(stack.popOldest());
		}
	}
	
	open override func id() -> String {
		return "rotup";
	}
	
	open override func description() -> String
	{
		return "Rotate the stack upwards.  This will push X into Y, and eventually the last number on the stack will wrap around into the X register.";
	}

	open override func mnemonic() -> String {
		return "R↑";
	}
	
	open override func operationType() -> OperationType
	{
		return .state;
	}
}
