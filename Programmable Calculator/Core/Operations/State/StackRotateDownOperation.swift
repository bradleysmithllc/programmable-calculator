//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
 * Pops the element off the top of the stack (oldest element), and pushes it onto the bottom.
 **/

open class StackRotateDownOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		if (stack.size() > 1) {
			stack.pushAtEnd(stack.popElement());
		}
	}
	
	open override func id() -> String {
		return "rotdown";
	}
	
	open override func description() -> String
	{
		return "Rotate the stack downwards.  This will push X to the end of the stack, Y into X, and every other stack element will move down.";
	}

	open override func mnemonic() -> String {
		return "R↓";
	}
	
	open override func operationType() -> OperationType
	{
		return .state;
	}
}
