//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
  * Subtratcs the value in the Y register from the memory slot identified by X.  The X register
  * is cleared but the Y register remains in X.
  **/

open class StoreMinusXOperation: BasicOperation {
	open override func perform() throws {
		var stack = calculator.stack();

		let slotId: NSDecimalNumber = stack.pop();
		let valueToStore = stack.getX();

		let slotKey = String(slotId.intValue);
		
		let mem = stack.memory(slotKey);
		let result = mathHub.subtract(left: mem, right: valueToStore);

		let _ = stack.assignMemory(slotKey,
			value: result
		);
	}
	
	open override func id() -> String {
		return "store_minus";
	}
	
	open override func description() -> String
	{
		return "Subtract the contents of the X register from the stored memory register.";
	}

	open override func mnemonic() -> String {
		return "sto-";
	}
	
	open override func operationType() -> OperationType
	{
		return .state;
	}
}
