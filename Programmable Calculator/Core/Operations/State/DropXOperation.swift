//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Pops the bottom stack, the X-register, off.
**/

open class DropXOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		let _ = stack.popElement();
	}
	
	open override func id() -> String {
		return "dropx";
	}
	
	open override func description() -> String
	{
		return "Drops the value in the X register, moving the rest of the stack downwards.";
	}

	open override func mnemonic() -> String {
		return "drop";
	}
	
	open override func operationType() -> OperationType
	{
		return .state;
	}
}
