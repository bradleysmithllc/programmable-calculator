//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Calculates the X percentage of Y, and pushes the result.  X (the percentage) is popped, but not Y.
* The percentage is expressed E.G., as 4.0 for 4%.
*  15.76
*  ent
*  3
*  percent
*  Y: 15.76
*  X: 0.4728
**/

open class PercentOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		let percent: NSDecimalNumber! = stack.pop();
		let base: NSDecimalNumber! = stack.getX();

		let percentDecimal: NSDecimalNumber = mathHub.divide(dividend: percent, divisor: 100);

		let result: NSDecimalNumber = mathHub.multiply(left: base, right: percentDecimal);

		stack.push(result);
	}

	open override func id() -> String {
		return "percent";
	}

	open override func mnemonic() -> String {
		return "%";
	}

	open override func stackAffect() -> [StackAffect]?
	{
		return [.Pop, .Peek, .Push];
	}
	
	open override func formula() -> String?
	{
		return
			"<math xmlns=\"http://www.w3.org/1998/Math/MathML\">" +
				"<mrow>" +
					"<mi><stack-y /></mi>" +
					"<mo>&sdot;</mo>" +
					"<mfrac>" +
						"<mi><stack-x /></mi>" +
						"<mn>100</mn>" +
					"</mfrac>" +
				"</mrow>" +
			"</math>";
	}
	
	open override func description() -> String
	{
		return "Calculates X% of Y";
	}
}
