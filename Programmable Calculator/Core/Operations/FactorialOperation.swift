//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Factorial x.
**/

open class FactorialOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		let x = stack.pop();
		let d_x = x.doubleValue;

		// trap arg < 0 since this is NaN
		if (d_x < 0)
		{
			stack.push(NSDecimalNumber.notANumber);
		}
		// not stricly required, but makes things easier
		else if (d_x == 0)
		{
			stack.push(NSDecimalNumber.one);
		}
		else
		{
			let i = x.decimalNumberInBase(16);
			let frac = mathHub.subtract(left: x, right: i);

			// if this is an integer, use a loop
			if (frac.doubleValue == 0)
			{
				let iv = i.intValue;

				// prevent lockup.  Do not process more than 100!
				if (iv >= 100)
				{
					stack.push(NSDecimalNumber.notANumber);
				}
				else if (iv < 0)
				{
					stack.push(NSDecimalNumber.notANumber);
				}
				else
				{
					// running sum
					var sum = NSDecimalNumber.one;

					if (iv != 0)
					{
						for j in 1...iv
						{
							sum = mathHub.multiply(left: sum, right: NSDecimalNumber(value: j as Int))
						}
					}
					
					stack.push(sum);
				}
			}
			else
			{
				let arg = x.doubleValue + 1.0;
					
				// use the tgamma function.  Requires adding one to the argument
				stack.push(NSDecimalNumber(value: tgamma(arg) as Double));
			}
		}
	}

	open override func id() -> String {
		return "x!";
	}

	open 	override func formula() -> String?
	{
		return "<math><mi>x</mi><mo>&sdot;</mo><mo>(</mo><mi>x</mi><mo>-</mo><mn>1</mn><mo>)</mo><mo>&sdot;</mo><mo>(</mo><mi>x</mi><mo>-</mo><mn>2</mn><mo>)</mo><mo>&ctdot;</mo><mo>(</mo><mi>x</mi><mo>-</mo><mi>x</mi><mo>+</mo><mi>1</mi><mo>)</mo></math>";
	}

	open override func mnemonic() -> String {
		return "x!";
	}
	
	open override func description() -> String
	{
		return "Factorial of x.";
	}
}
