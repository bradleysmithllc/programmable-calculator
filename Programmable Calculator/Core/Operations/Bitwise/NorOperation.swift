//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
  * Binary y nor x.  Uses the longest possible bit field
  **/

open class NorOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		let x = stack.pop().safeLongLongValue;
		let y = stack.pop().safeLongLongValue;
		
		stack.push(NSDecimalNumber(value: ~(y | x) as Int64));
	}

	open override func id() -> String {
		return "nor";
	}
	
	open override func mnemonic() -> String {
		return "nor";
	}
	
	open override func formula() -> String?
	{
		return "~(<stack-y /> | <stack-x />)"
	}
 
	open override func stackAffect() -> [StackAffect]?
	{
		return [.Pop, .Pop, .Push];
	}
	
	open override func description() -> String
	{
		return "Bitwise y nor x.  Bitwise y or x, then flip the bits in the resulting integer.";
	}
}
