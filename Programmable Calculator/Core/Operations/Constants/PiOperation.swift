//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Push PI onto the stack
**/

open class PiOperation: BasicOperation {
	open static let PI = NSDecimalNumber(value: Double.pi);
	
	open override func perform() throws {
		let stack = calculator.stack();
		
		stack.push(PiOperation.PI);
	}
	
	open override func id() -> String {
		return "pi_constant";
	}
	
	open override func mnemonic() -> String {
		return "π";
	}
}
