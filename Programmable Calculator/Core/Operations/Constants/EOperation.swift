//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Push E onto the stack
**/

open class EOperation: BasicOperation {
	open static let E = NSDecimalNumber(value: M_E as Double);
	
	open override func perform() throws {
		let stack = calculator.stack();
		
		stack.push(EOperation.E);
	}
	
	open override func id() -> String {
		return "e_constant";
	}

	open override func mnemonic() -> String {
		return "e";
	}
}
