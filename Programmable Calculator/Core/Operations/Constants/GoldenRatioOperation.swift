//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Push the golden ratio onto the stack
**/

open class GoldenRatioOperation: BasicOperation {
	fileprivate static let G = NSDecimalNumber(string: "1.61803398874989484820458683436563811772030917980576");
	
	open override func perform() throws {
		let stack = calculator.stack();
		
		stack.push(GoldenRatioOperation.G);
	}
	
	open override func id() -> String {
		return "gr_constant";
	}
	
	open override func mnemonic() -> String {
		return "gr";
	}
}
