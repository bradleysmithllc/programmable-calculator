//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Push Euler's constant onto the stack
**/

open class EulerOperation: BasicOperation {
	fileprivate static let GAMMA = NSDecimalNumber(string: "0.5772156649015328606065120900824024310421593359399235988057672348848677267776646709369470632917467495");
	
	open override func perform() throws {
		let stack = calculator.stack();
		
		stack.push(EulerOperation.GAMMA);
	}
	
	open override func id() -> String {
		return "euler_constant";
	}
	
	open override func mnemonic() -> String {
		return "euler";
	}
}
