//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Push 2*PI onto the stack
**/

open class TwoPiOperation: BasicOperation {
	fileprivate static let TWO_PI = NSDecimalNumber(value: Double.pi).multiplying(by: NSDecimalNumber(value: 2 as Int));
	
	open override func perform() throws {
		let stack = calculator.stack();
		
		stack.push(TwoPiOperation.TWO_PI);
	}
	
	open override func id() -> String {
		return "two_pi_constant";
	}
	
	open override func mnemonic() -> String {
		return "2*π";
	}
}
