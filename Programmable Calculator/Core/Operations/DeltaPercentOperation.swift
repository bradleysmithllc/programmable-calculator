//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Calculates the percentage difference between Y and X, and pushes the result.  X and Y are popped.
* The percentage is expressed E.G., as 4.0 for 4%, and is positive for X > Y or negative for X < Y
*  15.76
*  ent
*  14.12
*  delta-percent
*  X: -10.4061
**/

open class DeltaPercentOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		let x: NSDecimalNumber! = stack.pop();
		let y: NSDecimalNumber! = stack.pop();

		let x_minus_y: NSDecimalNumber = mathHub.subtract(left: x, right: y);
		let quotient = mathHub.divide(dividend: x_minus_y, divisor: y);
		
		// scale to a printable percentage
		let result: NSDecimalNumber = mathHub.multiply(left: quotient, right: NSDecimalNumber(value: 100 as Int));

		stack.push(result);
	}

	open override func id() -> String {
		return "delta-percent";
	}

	open override func mnemonic() -> String {
		return "%[Δ]";
	}
	
	open override func stackAffect() -> [StackAffect]?
	{
		return [.Pop, .Pop, .Push];
	}
	
	open override func formula() -> String?
	{
		return
			"<math xmlns=\"http://www.w3.org/1998/Math/MathML\">" +
				"<mrow>" +
					"<mfrac><mrow><mi><stack-x /></mi><mo>-</mo><mi><stack-y /></mi></mrow>" +
					"<mrow><mi><stack-y /></mi></mrow>" +
					"</mfrac>" +
					"<mo>&sdot;</mo>" +
					"<mi>100</mi>" +
				"</mrow>" +
		"</math>";
	}
	
	open override func description() -> String
	{
		return "Calculates % Change from Y to X";
	}
}
