//
//  BasicOperation.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/19/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

open class BasicOperation: Operation {
	let mathHub: MathHub = MathHub.mathHub();
	var calculator: Calculator;
	var stack: Stack;

	public required init(calculator: Calculator) {
		self.calculator = calculator;
		stack = calculator.stack();
	}

	open func operationType() -> OperationType
	{
		return .unary;
	}

	open func registerResultState() -> NumericEntryState
	{
		return .waiting_FOR_OPERATION;
	}

	open func validateStack() -> Bool {
		return true;
	}

	open func perform() throws {
		throw BadOperationState();
	}

	open func mnemonic() -> String {
		return id();
	}
	
	open func decoratedMnemonic() -> String {
		return mnemonic();
	}
	
	open func id() -> String {
		return "";
	}
	
	open func stackAffect() -> [StackAffect]?
	{
		return nil;
	}

	open func formula() -> String?
	{
		return nil;
	}

	open func description() -> String
	{
		return "";
	}
}
