//
//  TrigBaseOperation.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/20/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

open class TrigBaseOperation: BasicOperation {
	public enum TrigMode
	{
		case radians;
		case degrees;
		case gradians;
	}

	func getTrigInput(_ numeric: NSDecimalNumber, calculator: Calculator) -> NSDecimalNumber {
		// check for radians or degrees for input
		switch(TrigBaseOperation.operationsMode(calculator))
		{
		case .degrees:
			return mathHub.radiansFromDegrees(degrees: numeric);
		case .radians:
			return numeric;
		case .gradians:
			return mathHub.radiansFromGradians(gradians: numeric);
		}
	}

	func getTrigOutput(_ numeric: NSDecimalNumber, calculator: Calculator) -> NSDecimalNumber {
		// check for radians or degrees for output
		switch(TrigBaseOperation.operationsMode(calculator))
		{
		case .degrees:
			return mathHub.degreesFromRadians(radians: numeric);
		case .radians:
			return numeric;
		case .gradians:
			return mathHub.gradiansFromRadians(radians: numeric);
		}
	}

	open static func operationsMode(_ calculator: Calculator, setTo: TrigMode)
	{
		// check for radians or degrees for output
		let _ = calculator.stack().setProperty(CalculatorProperties.TRIGONOMERTIC_FUNCTIONS_MODE, propertyValue: setTo);
	}

	open static func operationsMode(_ calculator: Calculator) -> TrigMode
	{
		// check for radians or degrees for output
		return calculator.stack().propertyWithDefault(CalculatorProperties.TRIGONOMERTIC_FUNCTIONS_MODE, defaultValue: TrigMode.degrees)
	}

	open static func operationsInRadians(_ calculator: Calculator) -> Bool
	{
		// check for radians or degrees for output
		return operationsMode(calculator) == TrigMode.radians;
	}
	
	open override func operationType() -> OperationType
	{
		return .unary;
	}
}
