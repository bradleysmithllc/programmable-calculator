//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
*
**/

open class ToggleWordSizeOperation: BasicOperation {
	open override func perform() throws {
		switch (stack.getIntegerWordSize())
		{
			case .byte:
				stack.setIntegerWordSize(IntegerWordSize.word_2);
			case .word_2:
				stack.setIntegerWordSize(IntegerWordSize.word_4);
			case .word_4:
				stack.setIntegerWordSize(IntegerWordSize.word_8);
			case .word_8:
				stack.setIntegerWordSize(IntegerWordSize.byte);
			case .none:
				break;
		}
	}
	
	open override func description() -> String
	{
		return "Toggles the word size when in signed or unsigned integer mode.  Has no affect on decimal mode.<br />Available sizes:  <table><thead><th>Word Size</th><th>Signed</th><th>Unsigned</th></thead><tbody><tr><td>1 Byte</td><td>Int8</td><td>UInt8</td></tr><tr><td>2 Byte</td><td>Int16</td><td>UInt16</td></tr><tr><td>4 Byte</td><td>Int32</td><td>UInt32</td></tr><tr><td>8 Byte</td><td>Int64</td><td>UInt64</td></tr></tbody></table>";
	}

	open override func validateStack() -> Bool
	{
		return stack.getNumericBehavior() != .Decimal;
	}

	open override func id() -> String {
		return "wordsz";
	}

	open override func mnemonic() -> String {
		let unsigned = stack.getNumericBehavior() == .UnsignedInteger;
		let pre = (unsigned ? "U" : "") + "Int";

		switch (stack.getIntegerWordSize())
		{
		case .byte:
			return pre + "8";
		case .word_2:
			return pre + "16";
		case .word_4:
			return pre + "32";
		case .word_8:
			return pre + "64";
		case .none:
			return "-";
		}
	}
	
	open override func operationType() -> OperationType
	{
		return .state;
	}
}
