//
//  Programmers_CalculatorTests.swift
//  Programmers CalculatorTests
//
//  Created by Bradley Smith on 10/21/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import _RPN_

class CellSizeTests: TestSuperclass {
	func testOneCell() {
		let resultRect = CalculatorUIView.calculateCellSize(
			viewRect: makeRect(originX: 0, originY: 0, width: 12, height: 10),
			rows: 1,
			columns: 1
		);
		
		XCTAssertEqual(0, resultRect.origin.x);
		XCTAssertEqual(0, resultRect.origin.y);
		XCTAssertEqual(10, resultRect.height);
		XCTAssertEqual(12, resultRect.width);
	}
	
	func testOneCellFractional() {
		let resultRect = CalculatorUIView.calculateCellSize(
			viewRect: makeRect(originX: 0, originY: 0, width: 12.5, height: 10.5),
			rows: 1,
			columns: 1
		);

		XCTAssertEqual(0, resultRect.origin.x);
		XCTAssertEqual(0, resultRect.origin.y);
		XCTAssertEqual(10, resultRect.height);
		XCTAssertEqual(12, resultRect.width);
	}

	func testTwoByTwo() {
		let sourceRect = CalculatorUIView.calculateCellSize(
			viewRect: makeRect(originX: 0, originY: 0, width: 12, height: 10),
			rows: 1,
			columns: 1
		);
		
		let resultRect = CalculatorUIView.calculateCellSize(viewRect: sourceRect, rows: 2, columns: 2);
		
		XCTAssertEqual(0, resultRect.origin.x);
		XCTAssertEqual(0, resultRect.origin.y);
		XCTAssertEqual(5, resultRect.height);
		XCTAssertEqual(6, resultRect.width);
	}
}