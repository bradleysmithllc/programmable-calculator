//
//  Programmers_CalculatorTests.swift
//  Programmers CalculatorTests
//
//  Created by Bradley Smith on 10/21/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import _RPN_

/** Testing the gap distributing process.
*/
class GapDistributionTests: TestSuperclass {
	func testZero()
	{
		XCTAssertEqual(0, CalculatorUIView.distributeGaps(0, width: 0).count);
	}

	func testEven()
	{
		let gaps = CalculatorUIView.distributeGaps(7, width: 7)
		XCTAssertEqual(7, gaps.count);

		for gap in gaps
		{
			XCTAssertEqual(1, gap);
		}
	}
	
	func testUneven()
	{
		let gaps = CalculatorUIView.distributeGaps(7, width: 9)
		XCTAssertEqual(7, gaps.count);
		
		XCTAssertEqual(2, gaps[0]);
		XCTAssertEqual(2, gaps[1]);
		XCTAssertEqual(1, gaps[2]);
		XCTAssertEqual(1, gaps[3]);
		XCTAssertEqual(1, gaps[4]);
		XCTAssertEqual(1, gaps[5]);
		XCTAssertEqual(1, gaps[6]);
	}
}