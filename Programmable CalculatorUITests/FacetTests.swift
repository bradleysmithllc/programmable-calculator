//
//  Programmers_CalculatorTests.swift
//  Programmers CalculatorTests
//
//  Created by Bradley Smith on 10/21/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import _RPN_

class FacetTests: XCTestCase {
	func testEmpty()
	{
		let rect = CalculatorCellOperationUIView.makeNormalFacetTextRect(CGRect(x: 0, y: 0, width: 0, height: 0));

		XCTAssertEqual(0, rect.origin.x);
		XCTAssertEqual(0, rect.origin.y);
		XCTAssertEqual(0, rect.size.width);
		XCTAssertEqual(0, rect.size.height);
	}

	func testSingleCell()
	{
		let rect = CalculatorCellOperationUIView.makeNormalFacetTextRect(CGRect(x: 0, y: 0, width: 100, height: 100));
		
		XCTAssertEqual(15, rect.origin.x);
		XCTAssertEqual(30, rect.origin.y);
		XCTAssertEqual(70, rect.size.width);
		XCTAssertEqual(50, rect.size.height);
	}
}
