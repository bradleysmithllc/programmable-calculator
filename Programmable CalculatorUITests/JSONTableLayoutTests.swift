//
//  Programmers_CalculatorTests.swift
//  Programmers CalculatorTests
//
//  Created by Bradley Smith on 10/21/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import _RPN_

class JSONTableLayoutTests: TestSuperclass {
	func tesDefaultProfile() {
		let _: Layout = TestLayout.load(id: "");
	}

	func testEmpty() {
		let layout: Layout = TestLayout.load(id: "emptyTableLayout");

		XCTAssertEqual(0, layout.rows());
		XCTAssertEqual(0, layout.columns());
		XCTAssertNil(layout.suggestedDecimals());
		XCTAssertNil(layout.supportedNumericBases());

		XCTAssertNil(layout.supportedNumericBehaviors());
	}
	
	func testPortrait() {
		let layout: Layout = TestLayout.load(id: "portraitTableLayout");

		XCTAssertEqual(Orientation.portrait, layout.suggestedOrientation());
	}

	func testLandscape() {
		let layout: Layout = TestLayout.load(id: "landscapeTableLayout");
		
		XCTAssertEqual(Orientation.landscape, layout.suggestedOrientation());
	}

	func testNumericBases() {
		let layout: Layout = TestLayout.load(id: "numericBasesTableLayout");
		
		XCTAssertEqual(2, layout.supportedNumericBases()!.count);
		XCTAssertEqual(2, layout.supportedNumericBases()![0]);
		XCTAssertEqual(8, layout.supportedNumericBases()![1]);
	}

	func testLayoutEmpty() {
		let _ = layoutsTest("LayoutEmpty", rows: 0, columns: 0);
	}
	
	func testLayoutColumnsAll() {
		let _ = layoutsTest("LayoutColumnsAll", rows: 1, columns: 10);
	}

	func testLayoutColumnsSpanUIFunction() {
		let _ = layoutsTest("LayoutColumnsSpanUIFunction", rows: 1, columns: 10);
	}

	func testLayoutColumnsSpan() {
		let _ = layoutsTest("LayoutColumnsSpan", rows: 1, columns: 10);
	}

	func testLayoutColumnsRight() {
		let _ = layoutsTest("LayoutColumnsRight", rows: 1, columns: 10);
	}
	
	func testLayoutColumnsLeft() {
		let _ = layoutsTest("LayoutColumnsLeft", rows: 1, columns: 10);
	}
	
	func testLayoutColumnsJustified() {
		let _ = layoutsTest("LayoutColumnsJustified", rows: 1, columns: 10);
	}
	
	func testLayoutRowsAll() {
		let _ = layoutsTest("LayoutRowsAll", rows: 10, columns: 1);
	}

	func testLayoutRowsSpan() {
		let _ = layoutsTest("LayoutRowsSpan", rows: 10, columns: 1);
	}

	func testLayoutRowsRight() {
		let _ = layoutsTest("LayoutRowsRight", rows: 10, columns: 1);
	}

	func testLayoutRowsLeft() {
		let _ = layoutsTest("LayoutRowsLeft", rows: 10, columns: 1);
	}

	func testLayoutRowsJustified() {
		let _ = layoutsTest("LayoutRowsJustified", rows: 10, columns: 1);
	}
	
	func testLayoutBoxLeft() {
		let _ = layoutsTest("LayoutBoxLeft", rows: 10, columns: 4);
	}

	func testLayoutBoxRight() {
		let _ = layoutsTest("LayoutBoxRight", rows: 10, columns: 4);
	}

	func testLayoutBoxMiddle() {
		let _ = layoutsTest("LayoutBoxMiddle", rows: 10, columns: 4);
	}
	
	func testLayoutBoxTop() {
		let _ = layoutsTest("LayoutBoxTop", rows: 4, columns: 4);
	}

	func testLayoutBoxBottom() {
		let _ = layoutsTest("LayoutBoxBottom", rows: 4, columns: 4);
	}
	
	func testLayoutBoxHMiddle() {
		let _ = layoutsTest("LayoutBoxHMiddle", rows: 4, columns: 4);
	}
	
	func testLayoutBoxTrapped() {
		let _ = layoutsTest("LayoutBoxTrapped", rows: 4, columns: 4);
	}

	func testLayoutBoxNW() {
		let _ = layoutsTest("LayoutBoxNW", rows: 3, columns: 4);
	}

	func testLayoutLayoutCellFunctions() {
		let _ = layoutsTest("LayoutCellFunctions", rows: 1, columns: 3);
	}

	func testLayoutBox1() {
		let _ = layoutsTest("LayoutBox1", rows: 3, columns: 10);
	}

	func testLayoutBox2() {
		let _ = layoutsTest("LayoutBox2", rows: 5, columns: 10);
	}

	func testLayoutXScientific() {
		let _ = layoutsTest("LayoutXScientific", rows: 9, columns: 10, layer: Layer.normal, format: true);
	}

	func testLayoutXScientificFa() {
		let _ = layoutsTest("LayoutXScientific", rows: 9, columns: 10, layer: Layer.function_a, format: true);
	}
	
	func testLayoutInheritFromNormalNormal() {
		let _ = layoutsTest("LayoutInheritFromNormal", rows: 3, columns: 10, layer: Layer.normal);
	}
	
	func testLayoutInheritFromNormalFa() {
		let _ = layoutsTest("LayoutInheritFromNormal", rows: 3, columns: 10, layer: Layer.function_a);
	}
	
	func testLayoutInheritFromNormalFb() {
		let _ = layoutsTest("LayoutInheritFromNormal", rows: 3, columns: 10, layer: Layer.function_b);
	}
	
	func testLayoutInheritFromNormalFC() {
		let _ = layoutsTest("LayoutInheritFromNormal", rows: 3, columns: 10, layer: Layer.function_c);
	}

	func testLayoutInheritFromNormalFD() {
		let _ = layoutsTest("LayoutInheritFromNormal", rows: 3, columns: 10, layer: Layer.function_d);
	}

	func layoutsTest(_ id: String, rows: UInt8, columns: UInt8, layer: Layer = Layer.normal, format: Bool = false) -> Layout
	{
		let jsonfile = "tableLayoutTest_\(id)";
		let htmlfile = "\(jsonfile)_\(layer.rawValue)";
		
		let layout: Layout = TestLayout.load(id: jsonfile);
		layout.populateOperations(calculator);

		// compare html table to reference
		let htmlActual = layout.generateHtmlTable(layer, formatter: format ? TestFormatter() : nil);
		let htmlExpected = loadExpected(htmlfile);

		let result = matches(htmlActual, right: htmlExpected);

		print("Actual:\n\(htmlActual)");
		print("Expected:\n\(htmlExpected)");

		if !result.matches
		{
			XCTFail("JSONTableLayoutTests.\(layer) - \(id) contextLeft: '\(result.contextLeft)' contextRight: '\(result.contextRight)'");
		}
		
		XCTAssertEqual(columns, layout.columns(), "\(id) - columns")
		XCTAssertEqual(rows, layout.rows(), "\(id) - rows")

		return layout;
	}

	func matches(_ left: String, right: String) -> (matches: Bool, contextLeft: String, contextRight: String)
	{
		var indexActual = left.startIndex;
		var indexExpected = right.startIndex;
	
		var matches: Bool = true;

		while(matches && indexActual != left.endIndex && indexExpected != right.endIndex)
		{
			if left.characters[indexActual] != right.characters[indexExpected]
			{
				matches = false;
			}
			
			indexActual = left.index(indexActual, offsetBy: 1);
			indexExpected = right.index(indexExpected, offsetBy: 1);
		}

		// check for bad situation
		if (!matches || indexActual != left.endIndex || indexExpected != right.endIndex)
		{
			// character does not match in mid string.
			// grab either side of the spot and fail
			let results = getContext(left, indexLeft: indexActual, right: right, indexRight: indexExpected);

			return (matches: false, contextLeft: results.contextLeft, contextRight: results.contextRight);
		}

		return (matches: true, contextLeft: "", contextRight: "");
	}
	
	func getContext(_ left: String, indexLeft: String.Index, right: String, indexRight: String.Index) -> (contextLeft: String, contextRight: String)
	{
		// show context 20 chars before and 20 chars after
		let sact = makeSub(left, index: indexLeft, size: 10);
		let sexp = makeSub(right, index: indexRight, size: 10);

		return (contextLeft: sact, contextRight: sexp);
	}

	func makeSub(_ string: String, index: String.Index, size: UInt8) -> String
	{
		var indexBegin = index;

		for _ in 0..<size
		{
			if (indexBegin == string.startIndex)
			{
				break;
			}

			indexBegin = string.index(before: indexBegin);
		}

		var indexEnd = index;
		
		for _ in 0..<size
		{
			if (indexEnd == string.endIndex)
			{
				break;
			}
			
			indexEnd = string.index(after: indexEnd);
		}

		return string.substring(with: indexBegin..<indexEnd);
	}

	func loadExpected(_ filename: String) -> String
	{
		let file = Bundle(for:TestLayout.self).path(forResource: filename, ofType: "html")
		let url = URL(fileURLWithPath: file!, isDirectory: false);
		let data = try! Data(contentsOf: url)
		
		return String(data: data, encoding: String.Encoding.macOSRoman)!;
	}

	func testMatchesEmpty()
	{
		XCTAssertTrue(matches("", right: "").matches);
	}

	func testMatchesBlah()
	{
		XCTAssertTrue(matches("BlahBlahBlahBlahBlahBlahBlahBlahBlahBlahBlahBlahBlahBlah", right: "BlahBlahBlahBlahBlahBlahBlahBlahBlahBlahBlahBlahBlahBlah").matches);
	}

	func testMatchesLeftLonger()
	{
		matchesTest("A", right: "", expectedMatch: false, contextLeft: "A", contextRight: "");
	}
	
	func testMatchesRightLonger()
	{
		matchesTest("", right: "A", expectedMatch: false, contextLeft: "", contextRight: "A");
	}

	func testDifferInMiddle()
	{
		matchesTest("BBABB", right: "BBCBB", expectedMatch: false, contextLeft: "BBABB", contextRight: "BBCBB");
	}
	
	func testShowMeSomeContext()
	{
		matchesTest("bbbnnnnnnnnnngnnnnnnnnnnbbb", right: "bbbnnnnnnnnnnhnnnnnnnnnnbbb", expectedMatch: false, contextLeft: "nnnnnnnnngnnnnnnnnnn", contextRight: "nnnnnnnnnhnnnnnnnnnn");
	}

	func matchesTest(_ left: String, right: String, expectedMatch: Bool, contextLeft: String, contextRight: String)
	{
		let result = matches(left, right: right);

		XCTAssertEqual(expectedMatch, result.matches);

		if (!expectedMatch)
		{
			XCTAssertEqual(contextLeft, result.contextLeft);
			XCTAssertEqual(contextRight, result.contextRight);
		}
		else
		{
			XCTAssertEqual("", result.contextLeft);
			XCTAssertEqual("", result.contextRight);
		}
	}
}

class TestFormatter : NullHtmlFormatter
{
	override func prefix() -> String
	{
		return "<html><body>"
	}

	override func postfix() -> String
	{
		return "</body></html>"
	}

	override func tableAttributes() -> String
	{
		return " border=\"1\"";
	}
	
	override func cellText(_ cell: CalculatorCell, layer: Layer) -> String
	{
		let fun = cell.cellFunction(layer);
		
		var text = cell.cellKey() + ", " + fun.rawValue;

		if fun == function.function
		{
			text = cell.uiFunctionType(layer).rawValue
		}
		else
		{
			text = cell.operation(layer)!.id()
		}
		return text;
	}
}
