//
//  Programmers_CalculatorTests.swift
//  Programmers CalculatorTests
//
//  Created by Bradley Smith on 10/21/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import _RPN_

class JSONLayoutTests: XCTestCase {
	func testDefaultProfile() {
		let tp: Layout = TestLayout.load(id: "testPerspective");

		XCTAssertEqual([2, 8, 10], tp.supportedNumericBases()!);
	}
}