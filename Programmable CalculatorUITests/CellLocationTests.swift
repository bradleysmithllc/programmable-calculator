//
//  Programmers_CalculatorTests.swift
//  Programmers CalculatorTests
//
//  Created by Bradley Smith on 10/21/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import _RPN_

/** Testing the rect-alloctaion process to ensure the entire screen
  * is utilized and there is no overlap
  */
class CellLocationTests: TestSuperclass {
	func test1Layout()
	{
		let per: Layout = TestLayout.load(id: "testMonoPerspective");

		let rectMap = CalculatorUIView.buildRectMap(per, layer: Layer.normal, screenSize: CGSize(width: 100, height: 100));

		XCTAssertEqual(1, rectMap.count);

		XCTAssertEqual(0, rectMap[0]!.rect.origin.x);
		XCTAssertEqual(0, rectMap[0]!.rect.origin.y);
		XCTAssertEqual(100.0, rectMap[0]!.rect.size.width);
		XCTAssertEqual(100.0, rectMap[0]!.rect.size.height);
	}

	func testOrigin()
	{
		let per: Layout = TestLayout.load(id: "testMonoPerspective");
		
		let rectMap = CalculatorUIView.buildRectMap(per, layer: Layer.normal, screenSize: CGSize(width: 100, height: 100));
		
		XCTAssertEqual(1, rectMap.count);
		
		XCTAssertEqual(0, rectMap[0]!.rect.origin.x);
		XCTAssertEqual(0, rectMap[0]!.rect.origin.y);
		XCTAssertEqual(100.0, rectMap[0]!.rect.size.width);
		XCTAssertEqual(100.0, rectMap[0]!.rect.size.height);
	}
	
	func tesLayoutMore()
	{
		let per: Layout = TestLayout.load(id: "testPerspective");
		
		let rectMap = CalculatorUIView.buildRectMap(per, layer: Layer.normal, screenSize: CGSize(width: 20, height: 40));

		XCTAssertEqual(6, rectMap.count);

		// first row
		// top left cell
		XCTAssertEqual(0, rectMap[0]!.rect.origin.x);
		XCTAssertEqual(0, rectMap[0]!.rect.origin.y);
		XCTAssertEqual(10.0, rectMap[0]!.rect.size.width);
		XCTAssertEqual(10.0, rectMap[0]!.rect.size.height);
		
		// top right cell
		XCTAssertEqual(10.0, rectMap[1]!.rect.origin.x);
		XCTAssertEqual(0.0, rectMap[1]!.rect.origin.y);
		XCTAssertEqual(10.0, rectMap[1]!.rect.size.width);
		XCTAssertEqual(20.0, rectMap[1]!.rect.size.height);

		// second row
		// middle left cell (middle-right extends to row 1)
		XCTAssertEqual(0.0, rectMap[1000]!.rect.origin.x);
		XCTAssertEqual(10.0, rectMap[1000]!.rect.origin.y);
		XCTAssertEqual(10.0, rectMap[1000]!.rect.size.width);
		XCTAssertEqual(10.0, rectMap[1000]!.rect.size.height);

		// third row
		// middle left cell
		XCTAssertEqual(0.0, rectMap[2000]!.rect.origin.x);
		XCTAssertEqual(20.0, rectMap[2000]!.rect.origin.y);
		XCTAssertEqual(10.0, rectMap[2000]!.rect.size.width);
		XCTAssertEqual(10.0, rectMap[2000]!.rect.size.height);

		// middle right cell
		XCTAssertEqual(10.0, rectMap[2001]!.rect.origin.x);
		XCTAssertEqual(20.0, rectMap[2001]!.rect.origin.y);
		XCTAssertEqual(10.0, rectMap[2001]!.rect.size.width);
		XCTAssertEqual(10.0, rectMap[2001]!.rect.size.height);
		
		// fourth row
		// single merged cell
		XCTAssertEqual(0.0, rectMap[3000]!.rect.origin.x);
		XCTAssertEqual(30.0, rectMap[3000]!.rect.origin.y);
		XCTAssertEqual(20.0, rectMap[3000]!.rect.size.width);
		XCTAssertEqual(10.0, rectMap[3000]!.rect.size.height);
	}

	func testExtendsRight()
	{
		let per: Layout = TestLayout.load(id: "testLayoutExtendsRight");
		
		let rectMap = CalculatorUIView.buildRectMap(per, layer: Layer.normal, screenSize: CGSize(width: 100, height: 100));
		
		XCTAssertEqual(1, rectMap.count);
		
		XCTAssertEqual(0, rectMap[0]!.rect.origin.x);
		XCTAssertEqual(0, rectMap[0]!.rect.origin.y);
		XCTAssertEqual(100.0, rectMap[0]!.rect.size.width);
		XCTAssertEqual(100.0, rectMap[0]!.rect.size.height);
	}

	func tesExtendsRightMany()
	{
		let per: Layout = TestLayout.load(id: "testLayoutExtendsRightMany");
		
		let rectMap = CalculatorUIView.buildRectMap(per, layer: Layer.normal, screenSize: CGSize(width: 100, height: 100));
		
		XCTAssertEqual(2, rectMap.count);

		XCTAssertEqual(0, rectMap[0]!.rect.origin.x);
		XCTAssertEqual(0, rectMap[0]!.rect.origin.y);
		XCTAssertEqual(50, rectMap[0]!.rect.size.width);
		XCTAssertEqual(100, rectMap[0]!.rect.size.height);
		
		XCTAssertEqual(50, rectMap[1]!.rect.origin.x);
		XCTAssertEqual(0, rectMap[1]!.rect.origin.y);
		XCTAssertEqual(50, rectMap[1]!.rect.size.width);
		XCTAssertEqual(100, rectMap[1]!.rect.size.height);
	}

	func tstLayoutUnequal()
	{
		let per: Layout = TestLayout.load(id: "scientific");

		let rectMap = CalculatorUIView.buildRectMap(per, layer: Layer.normal, screenSize: CGSize(width: 10, height: 10));

		var row = 0;
		
		// verify that all rows have the same origin y value
		for _ in 0...9
		{
			row += 1000;
			// grab the first rect in this row

			if let firstRect = rectMap[row]
			{
				let y = firstRect.rect.origin.y;

				// check first ten cells
				for cell in 1..<10
				{
					let cellId = row + cell;

					if let rectCell = rectMap[cellId]
					{
						//print("\(cellId) \(y) \(rectCell.rect)");
						XCTAssertEqual(y, rectCell.rect.origin.y);
					}
				}
			}
		}
	}
}
