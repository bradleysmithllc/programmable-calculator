//
//  ModulusTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import _RPN_

class CalculatorGridViewTests: TestSuperclass {
	func testEmpty() {
		let res = CalculatorCellOperationUIView.splitDecimal("");
		XCTAssertEqual("", res.integral);
		XCTAssertEqual("", res.decimal);
	}

	func testInteger() {
		let res = CalculatorCellOperationUIView.splitDecimal("12");
		XCTAssertEqual("12", res.integral);
		XCTAssertEqual("", res.decimal);
	}

	func testDecimal() {
		let res = CalculatorCellOperationUIView.splitDecimal("12.12");
		XCTAssertEqual("12", res.integral);
		XCTAssertEqual(".12", res.decimal);
	}

	func testBeginDecimal() {
		let res = CalculatorCellOperationUIView.splitDecimal(".12");
		XCTAssertEqual("", res.integral);
		XCTAssertEqual(".12", res.decimal);
	}
	
	func testEndDecimal() {
		let res = CalculatorCellOperationUIView.splitDecimal("12.");
		XCTAssertEqual("12", res.integral);
		XCTAssertEqual(".", res.decimal);
	}

	func testSeparatizeEmpty() {
		let res = CalculatorCellOperationUIView.separatize("", digitsPerSeparator: 2);

		XCTAssertEqual("", res);
	}

	func testSeparatizeLessThanCount() {
		let res = CalculatorCellOperationUIView.separatize("999", digitsPerSeparator: 4);
		
		XCTAssertEqual("999", res);
	}
	
	func testSeparatize() {
		let res = CalculatorCellOperationUIView.separatize("999999", digitsPerSeparator: 2);
		
		XCTAssertEqual("99,99,99", res);
	}
	
	func testSeparatize3() {
		let res = CalculatorCellOperationUIView.separatize("9999999", digitsPerSeparator: 3);
		
		XCTAssertEqual("9,999,999", res);
	}
	
	func testSeparatize4() {
		let res = CalculatorCellOperationUIView.separatize("9999999.09090909", digitsPerSeparator: 3);
		
		XCTAssertEqual("9,999,999,.09,090,909", res);
	}

	func testSeparatizeNegative() {
		let res = CalculatorCellOperationUIView.separatize("-9999999", digitsPerSeparator: 3);
		
		XCTAssertEqual("-9,999,999", res);
	}

	func testSeparatizeNegative2() {
		let res = CalculatorCellOperationUIView.separatize("-9999999", digitsPerSeparator: 2);
		
		XCTAssertEqual("-9,99,99,99", res);
	}
	
	func testSeparatizeNegative3() {
		let res = CalculatorCellOperationUIView.separatize("-99999999", digitsPerSeparator: 2);
		
		XCTAssertEqual("-99,99,99,99", res);
	}
}